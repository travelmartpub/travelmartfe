const withSass = require("@zeit/next-sass");
const withLess = require("@zeit/next-less");
const withCSS = require("@zeit/next-css");

const isProd = process.env.NODE_ENV === 'production';

// // fix: prevents error when .less files are required by node
if (typeof require !== "undefined") {
  require.extensions[".less"] = () => {
  };
  require.extensions[".css"] = (file) => {
  };
}

module.exports = withCSS({
  cssModules: true,
  cssLoaderOptions: {
    importLoaders: 1,
    localIdentName: "[local]___[hash:base64:5]",
  },
  ...withLess(
      withSass({
        lessLoaderOptions: {
          javascriptEnabled: true,
        },
      })
  ),
  env: {
    APP_API_URL: process.env.APP_API_URL,
    APP_API_URL_SERVER: process.env.APP_API_URL_SERVER,
    APP_SUB_DOMAIN: isProd ? process.env.APP_SUB_DOMAIN : '',
    APP_REPORT_STATIC_SERVER: process.env.APP_REPORT_STATIC_SERVER
  },
  assetPrefix: isProd ? process.env.APP_SUB_DOMAIN : '',
  async rewrites() {
    return [
      {
        source: '/seller',
        destination: 'http://seller.travelmart.store'
      },
    ]
  }
});