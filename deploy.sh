#!/bin/sh

CI_REGISTRY_USER=$1
CI_REGISTRY_PASSWORD=$2
CI_REGISTRY=$3
CONTAINER_IMAGE=$4
CONTAINER_NAME=$5
HOST=$6
PORT=$7

echo "-> STOP container $CONTAINER_NAME"
docker stop $CONTAINER_NAME || true && docker rm $CONTAINER_NAME || true

echo "-> PULL new container"
docker pull $CONTAINER_IMAGE

echo "-> START new container $CONTAINER_NAME at $HOST:$PORT"
docker run \
    -l "traefik.http.routers.$CONTAINER_NAME.rule=Host(\`$HOST\`)" \
    -l "traefik.http.routers.$CONTAINER_NAME.tls=true" \
    -l "traefik.http.routers.$CONTAINER_NAME.tls.certresolver=le" \
    -l "traefik.port=$PORT" \
    -l "traefik.enable=true" \
    -l "traefik.docker.network=web" \
    --net web \
    --name $CONTAINER_NAME \
    --restart always \
    -d \
    $CONTAINER_IMAGE

echo "-> CLEAN UP unused images"
docker image prune -a -f
