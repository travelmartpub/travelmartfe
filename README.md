# TRAVELMART-FRONTEND

First, run the development server:

```bash
yarn dev                            ## run with .env.development

yarn build && yarn start            ## run with .env.production

yarn build:sandbox && yarn start    ## run with .env.development.sandbox

yarn deploy:sandbox                 ## deploy to sandbox .39s
```

### How to run

Execute:

- Run via Docker Compose: `COMPOSE_DOCKER_CLI_BUILD=1 DOCKER_BUILDKIT=1 docker-compose up --build`
- Or, build with env=sandbox: `DOCKER_BUILDKIT=1 docker build --build-arg RELEASE=sandbox -t vnit-frontend .`, then run `docker-compose up`

### Learn More

To learn more about Next.js, take a look at the following resources:

- [Next.js Documentation](https://nextjs.org/docs) - learn about Next.js features and API.
- [Learn Next.js](https://nextjs.org/learn) - an interactive Next.js tutorial.
