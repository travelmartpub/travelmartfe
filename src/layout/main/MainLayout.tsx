import React from 'react';
import {Layout} from "antd";
import {FOOTER_TEXT} from "../../helper/config";
import {AppState} from "../../redux";
import {useSelector} from "react-redux";
import Sidebar from "../../component/Sidebar";
import {
  NAV_STYLE_ABOVE_HEADER,
  NAV_STYLE_BELOW_HEADER,
  NAV_STYLE_DARK_HORIZONTAL,
  NAV_STYLE_DEFAULT_HORIZONTAL, NAV_STYLE_FIXED,
  NAV_STYLE_INSIDE_HEADER_HORIZONTAL, NAV_STYLE_MINI_SIDEBAR, TAB_SIZE
} from "../../constant/ThemeSetting";
import Topbar from "../../component/Topbar";

const {Content, Footer} = Layout;

interface IProps {
  children: JSX.Element | JSX.Element[];
}

const getContainerClass = (navStyle: string) => {
  switch (navStyle) {
    case NAV_STYLE_DARK_HORIZONTAL:
      return "gx-container-wrap";
    case NAV_STYLE_DEFAULT_HORIZONTAL:
      return "gx-container-wrap";
    case NAV_STYLE_INSIDE_HEADER_HORIZONTAL:
      return "gx-container-wrap";
    case NAV_STYLE_BELOW_HEADER:
      return "gx-container-wrap";
    case NAV_STYLE_ABOVE_HEADER:
      return "gx-container-wrap";
    default :
      return ""
  }
};

const getNavStyles = (navStyle: string) => {
  switch (navStyle) {
      // case NAV_STYLE_DEFAULT_HORIZONTAL :
      //   return <HorizontalDefault/>;
      // case NAV_STYLE_DARK_HORIZONTAL :
      //   return <HorizontalDark/>;
      // case NAV_STYLE_INSIDE_HEADER_HORIZONTAL :
      //   return <InsideHeader/>;
      // case NAV_STYLE_ABOVE_HEADER :
      //   return <AboveHeader/>;
      // case NAV_STYLE_BELOW_HEADER :
      //   return <BelowHeader/>;
    case NAV_STYLE_FIXED :
      return <Topbar/>;
      // case NAV_STYLE_DRAWER :
      //   return <Topbar/>;
    case NAV_STYLE_MINI_SIDEBAR :
      return <Topbar/>;
      // case NAV_STYLE_NO_HEADER_MINI_SIDEBAR :
      //   return <NoHeaderNotification/>;
      // case NAV_STYLE_NO_HEADER_EXPANDED_SIDEBAR :
      //   return <NoHeaderNotification/>;
    default :
      return null;
  }
};

const getSidebar = (navStyle: string, width: number) => {
  return <Sidebar/>;
};

const MainLayout = (props: IProps) => {

  const {navStyle, width} = useSelector((state: AppState) => state.settings);

  return (
      <Layout className="gx-app-layout">
        {/*{this.getSidebar(navStyle, width)}*/}
        {getSidebar(navStyle, width)}
        <Layout>
          {/*{this.getNavStyles(navStyle)}*/}
          {/*{getNavStyles(navStyle)}*/}

          <Content className={`gx-layout-content ${getContainerClass(navStyle)} `}>
            {/*<App match={match}/>*/}
            <div className="gx-main-content-wrapper">
              {props.children}
            </div>
            <Footer>
              <div className="gx-layout-footer-content">
                {FOOTER_TEXT}
              </div>
            </Footer>
          </Content>
        </Layout>
      </Layout>
  );
};

export default MainLayout;