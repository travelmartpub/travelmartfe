import React, {useEffect} from 'react';
import {Layout} from "antd";
import {FOOTER_TEXT} from "../../../helper/config";
import BelowHeader from "../../../component/travelmart/Topbar/BelowHeader";
import BelowHeaderCheckout from "../../../component/travelmart/Topbar/BelowHeaderCheckout";

const {Content, Footer} = Layout;

interface IProps {
  children: JSX.Element | JSX.Element[];
}

const PaymentLayout = (props: IProps) => {

  useEffect(() => {
    // scroll top bar
    document.body.classList.add('full-scroll');
    document.body.classList.add('horizontal-layout');
  }, []);

  return (
      <Layout className="gx-app-layout">
        <Layout>
          <BelowHeaderCheckout/>
          <Content className={`gx-layout-content gx-container-wrap`}>
            <div className="gx-main-content-wrapper">
              {props.children}
            </div>

            <Footer>
              <div className="gx-layout-footer-content">
                {FOOTER_TEXT}
              </div>
            </Footer>

          </Content>
        </Layout>
      </Layout>
  );
};

export default PaymentLayout;