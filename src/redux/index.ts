import {Action} from '@reduxjs/toolkit';
import {createStore, applyMiddleware} from 'redux';
import thunk, {ThunkAction} from 'redux-thunk';
import {composeWithDevTools} from 'redux-devtools-extension';

import {createRootReducer} from './rootReducer';

export function initializeStore(initialState: any) {
  const createdStore = createStore(
      createRootReducer(),
      initialState,
      composeWithDevTools(applyMiddleware(thunk))
  );

  store = createdStore;
  return createdStore;
}

export type AppStore = ReturnType<typeof initializeStore>;
export type AppDispatch = AppStore['dispatch'];
export type AppThunk = ThunkAction<void, AppState, null, Action<string>>;
export type AppState = ReturnType<AppStore['getState']>;

export let store: AppStore = null;
