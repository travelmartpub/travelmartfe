import {IAddressState} from "../../model/state/IAddressState";
import {createSlice, PayloadAction} from "@reduxjs/toolkit";
import {IAddress} from "../../model/entity/IAddress";
import {AppThunk} from "../index";
import {AddressAPI} from "../../api/AddressAPI";

const initialState: IAddressState = {
  address: {
    userId: 0,
    phone: "",
    houseNumberStreet: "",
    address: ""
  }
};

const addressSlice = createSlice({
  name: 'address',
  initialState,
  reducers: {
    setAddress(state: IAddressState, action: PayloadAction<IAddress>) {
      state.address = action.payload;
    },
    removeAddress(state: IAddressState) {
      state.address = initialState.address
    }
  }
});

const {actions, reducer} = addressSlice;

export default reducer;

const getAddress = (): AppThunk => async (dispatch, getState) => {
  try {
    const {error, data} = await AddressAPI.getAddress();

    if (error.code === 1) {
      await dispatch(actions.setAddress(data));
    } else {
      console.log("Payload: error ", error.message);
    }
  } catch (error) {
    console.log("Payload: error ", error.message);
  } finally {
  }
};

const setAddress = (address: IAddress, successfulCallback: VoidFunction, failureCallback: VoidFunction): AppThunk => async (dispatch, getState) => {
  try {
    const {error, data} = await AddressAPI.setAddress(address);

    if (error.code === 1) {
      await dispatch(actions.setAddress(data));
      successfulCallback();
    } else {
      console.log("Payload: error ", error.message);
      failureCallback();
    }
  } catch (error) {
    console.log("Payload: error ", error.message);
    failureCallback();
  } finally {
  }
};

const removeAddress = (): AppThunk => async dispatch => {
  await dispatch(actions.removeAddress());
};

export const addressActions = {
  getAddress,
  setAddress,
  removeAddress
};

