import {createSlice, PayloadAction} from '@reduxjs/toolkit';
import {AppThunk} from "../index";
import {MeAPI} from "../../api/MeAPI";
import {authActions} from "./authSlice";
import {IUserState} from "../../model/state/IUserState";
import {IAuthority} from "../../model/entity/IAuthority";
import {IUser} from "../../model/entity/IUser";

const initialState: IUserState = {
  user: {
    id: 0,
    userName: "",
    email: "",
    activated: true,
    storeId: 0,
    avatar: "",
    authorities: [] as IAuthority[]
  }
};

/**
 * Simplifying Slice
 */
const meSlice = createSlice({
  name: 'me',
  initialState,
  reducers: {
    setUser(state: IUserState, action: PayloadAction<IUser>) {
      state.user = action.payload;
    },
    removeUser(state: IUserState) {
      state.user = initialState.user
    }
  }
});

/**
 *  Extract the action creators object and the reducer
 */
const {actions, reducer} = meSlice;

/**
 * Export the reducer, either as a default or named export
 */
export default reducer;

/**
 * ==========================================================================
 * Define a thunk that dispatches those action creators
 * More: https://redux-toolkit.js.org/usage/usage-guide#defining-async-logic-in-slices
 */
const getUserInfo = (token: string): AppThunk => async (dispatch, getState) => {
  try {
    const {error, data} = await MeAPI.getUser(token);

    if (error.code === 1) {
      await dispatch(authActions.updateAuthorities(data.authorities));
      await dispatch(actions.setUser(data));
    } else {
      console.log("Payload: error ", error.message);
    }
  } catch (error) {
    console.log("Payload: error ", error.message);
  } finally {
  }
};

const setGuestUser = (): AppThunk => async (dispatch, getState) => {
  await dispatch(actions.setUser(initialState.user));
};

const logOut = (): AppThunk => async (dispatch, getState) => {
  await dispatch(actions.removeUser());
};

/**
 * Export each action creator by name
 */
export const MeActions = {
  getUserInfo,
  setGuestUser,
  logOut
};
