import {createSlice} from '@reduxjs/toolkit';
import {
  LAYOUT_TYPE_FULL, NAV_STYLE_BELOW_HEADER, NAV_STYLE_FIXED,
  THEME_COLOR_SELECTION_PRESET,
  THEME_TYPE_LITE
} from "../../constant/ThemeSetting";

interface ISettingsState {
  navCollapsed: boolean,
  navStyle: string,
  layoutType: string,
  themeType: string,
  colorSelection: string,

  pathname: string,
  width: number,
  isDirectionRTL: boolean,
  locale: {
    languageId: string,
    locale: string,
    name: string,
    icon: string
  }
}

const initialState: ISettingsState = {
  navCollapsed: true,
  navStyle: NAV_STYLE_BELOW_HEADER,
  layoutType: LAYOUT_TYPE_FULL,
  themeType: THEME_TYPE_LITE,
  colorSelection: THEME_COLOR_SELECTION_PRESET,

  pathname: '',
  width: 1200,
  isDirectionRTL: false,
  locale: {
    languageId: 'english',
    locale: 'en',
    name: 'English',
    icon: 'us'
  }
};

/**
 * Simplifying Slice
 */
const settingsSlice = createSlice({
  name: 'settings',
  initialState,
  reducers: {

    // fetchStartReducer(state: ICommonState) {
    //   state.error = '';
    //   state.message = '';
    //   state.loading = true
    // },
    //
    // fetchSuccessReducer(state: ICommonState, action: PayloadAction<string>) {
    //   state.error = '';
    //   state.message = action.payload ? action.payload : '';
    //   state.loading = false
    // },
    //
    // fetchErrorReducer(state: ICommonState, action: PayloadAction<string>) {
    //   state.error = action.payload;
    //   state.message = '';
    //   state.loading = false
    // },
    //
    // showMessageReducer(state: ICommonState, action: PayloadAction<string>) {
    //   state.error = '';
    //   state.message = action.payload;
    //   state.loading = false
    // },
    //
    // hideMessageReducer(state: ICommonState) {
    //   state.error = '';
    //   state.message = '';
    //   state.loading = false
    // }
  }
});

/**
 *  Extract the action creators object and the reducer
 */
const {actions, reducer} = settingsSlice;

/**
 * Extract and export each action creator by name
 */
export const {} = actions;

/**
 * Export the reducer, either as a default or named export
 */
export default reducer;

/**
 * ==========================================================================
 * Define a thunk that dispatches those action creators
 * More: https://redux-toolkit.js.org/usage/usage-guide#defining-async-logic-in-slices
 */
// const fetchStart = (): AppThunk => async dispatch => {
//   dispatch(fetchStartReducer());
// };
//
// const hideMessage = (): AppThunk => async dispatch => {
//   dispatch(hideMessageReducer());
// };
//
// const fetchSuccess = (message): AppThunk => async dispatch => {
//   dispatch(fetchSuccessReducer(message));
//
//   setTimeout(() => {
//     dispatch(hideMessageReducer());
//   }, 3000);
// };
//
// const fetchError = (message): AppThunk => async dispatch => {
//   dispatch(fetchErrorReducer(message));
//
//   setTimeout(() => {
//     dispatch(hideMessageReducer());
//   }, 3000);
// };
//
// const showMessage = (message): AppThunk => async dispatch => {
//   dispatch(showMessageReducer(message));
//
//   setTimeout(() => {
//     dispatch(hideMessageReducer());
//   }, 3000);
// };

/**
 * Export each action creator by name
 */
export const settingsActions = {};