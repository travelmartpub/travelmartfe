import {createSlice, PayloadAction} from "@reduxjs/toolkit";
import {ICart, ICartItem} from "../../model/state/ICart";
import {StorageHelper} from "../../helper/Storage";
import {AppThunk} from "../index";
import {IStore} from "../../model/entity/IStore";
import {IProduct} from "../../model/entity/IProduct";

const initialState: ICart = {
  miniCartShow: false,
  items: [] as ICartItem[],
  stores: [] as IStore[],
  products: [] as IProduct[],
};

/**
 * Simplifying Slice
 */

const cartSlice = createSlice({
  name: "cart",
  initialState,
  reducers: {

    addItemToCart(state: ICart, action: PayloadAction<ICartItem>) {
      const item = state.items.find(cart => (
          (cart.productId === action.payload.productId) &&
          (cart.storeId === action.payload.storeId)
      ));

      // Add new
      if (item === undefined) {
        state.items = [...state.items, action.payload];
      }
      // Update quantity
      else {

        const itemIndex = state.items.findIndex((item) => (
            (item.productId === action.payload.productId) &&
            (item.storeId === action.payload.storeId)
        ));

        state.items[itemIndex].quantity = state.items[itemIndex].quantity + action.payload.quantity;
      }

      StorageHelper.setLocalStorage('cart', JSON.stringify(state));
    },

    addStoreToCart(state: ICart, action: PayloadAction<IStore>) {
      const isExist = state.stores.map(store => store.id).includes(action.payload.id);

      if (!isExist) {
        state.stores = [...state.stores, action.payload];
      }

      StorageHelper.setLocalStorage('cart', JSON.stringify(state));
    },

    addProductToCart(state: ICart, action: PayloadAction<IProduct>) {
      const isExist = state.products.map(product => product.id).includes(action.payload.id);

      if (!isExist) {
        state.products = [...state.products, action.payload];
      }

      StorageHelper.setLocalStorage('cart', JSON.stringify(state));
    },

    setCart(state: ICart, action: PayloadAction<ICart>) {
      state.items = action.payload.items;
      state.stores = action.payload.stores;
      state.products = action.payload.products;
    },

    toggleMiniCart(state: ICart, action: PayloadAction<boolean>) {
      state.miniCartShow = action.payload;
    },

    updateQuantity(state: ICart, action: PayloadAction<ICartItem>) {
      const itemIndex = state.items.findIndex((item) => (
          (item.productId === action.payload.productId) &&
          (item.storeId === action.payload.storeId)
      ));

      state.items[itemIndex].quantity = action.payload.quantity;
      StorageHelper.setLocalStorage('cart', JSON.stringify(state));
    }
  }
});

/**
 *  Extract the action creators object and the reducer
 */
const {actions, reducer} = cartSlice;

/**
 * Export the reducer, either as a default or named export
 */
export default reducer;

/**
 * ==========================================================================
 * Define a thunk that dispatches those action creators
 * More: https://redux-toolkit.js.org/usage/usage-guide#defining-async-logic-in-slices
 */
const addToCart = (item: ICartItem, store: IStore, product: IProduct) => async dispatch => {
  await dispatch(actions.addItemToCart(item));
  await dispatch(actions.addStoreToCart(store));
  await dispatch(actions.addProductToCart(product));

  // Toggle mini cart
  await dispatch(actions.toggleMiniCart(true));

  setTimeout(async () => {
    await dispatch(actions.toggleMiniCart(false));
  }, 2000);
};

const updateQuantity = (item: ICartItem) => async dispatch => {
  await dispatch(actions.updateQuantity(item));
};

const loadCart = (ctx): AppThunk => async (dispatch) => {
  const cart = StorageHelper.get2SideCookie(ctx, 'cart') as ICart;

  if (cart === undefined ||
      cart.items === undefined ||
      cart.stores === undefined ||
      cart.products === undefined)
    return;

  await dispatch(actions.setCart(cart));
}

const loadCartLocalStorage = (): AppThunk => async (dispatch) => {
  const cart = StorageHelper.getLocalStorage('cart') as ICart;

  if (cart === undefined || cart === null ||
      cart.items === undefined ||
      cart.stores === undefined ||
      cart.products === undefined)
    return;

  await dispatch(actions.setCart(cart));
}

const cleanCart = (): AppThunk => async (dispatch) => {
  StorageHelper.removeLocalStorage('cart');
  await dispatch(actions.setCart(initialState));
}

/**
 * Export each action creator by name
 */
export const CartActions = {
  addToCart,
  loadCart,
  updateQuantity,
  cleanCart,
  loadCartLocalStorage
};