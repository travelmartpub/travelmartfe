
import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { CategoryItemAPI } from '../../api/CategoryItemAPI';
import _ from 'lodash';
import { ICategoryItem, ICategoryItems } from '../../model/ICategoryItem';
import { commonActions } from './commonSlice';

const initialState: ICategoryItems = {
    isLoading: false,
    error: null,
    categoryItems: []
}

const categoryItemSlice = createSlice({
    name: 'categoryItems',
    initialState,
    reducers: {
        getAllMeCategoryItemsStart(state: ICategoryItems){
            state.isLoading = true;
        },
        getAllMeCategoryItemsSuccess(state: ICategoryItems, action: PayloadAction<Array<ICategoryItem>>){
            state.isLoading = false;
            state.categoryItems = action.payload;
        },
        getAllMeCategoryItemsFailed(state: ICategoryItems, action: PayloadAction<string>){
            state.isLoading = false;
            state.error = action.payload
        }
    }
})

const { actions, reducer } = categoryItemSlice;

export const {
    getAllMeCategoryItemsStart,
    getAllMeCategoryItemsSuccess,
    getAllMeCategoryItemsFailed
} = actions;

export default reducer;

const createCategoryItem = (name: string, description: string) => async dispatch => {
    const req = {
        name: name,
        description: description
    }

    console.log("req",req);

    try {
        dispatch(commonActions.fetchStart());
        const { error, data } = await CategoryItemAPI.createCategoryItem(req);
        console.log("respones: ", data);

        if ( error.code === 0){
            dispatch(commonActions.fetchSuccess("Inserted success"));
        } else {
            console.log("padload: data.error: ", error.message);
        } 
        
    } catch(error) {
        console.log("Error****:", error.message);
        dispatch(commonActions.fetchError(error.message));
    } finally {
    }
} 

const getAllCategoryItems = () => async dispatch => {
    try {
        dispatch(getAllMeCategoryItemsStart());
        const { error, data } = await CategoryItemAPI.getCategoryItems();
        if ( error.code ===0 ){
            await dispatch(getAllMeCategoryItemsSuccess(data));
        } else {
            console.log("payload: data.error,", error.message);
        }
    } catch (error) {
        console.log("Error: ", error.message);
        await dispatch(getAllMeCategoryItemsFailed(error.message));
       }
    finally {

    }
}

const updateCategoryItem = (id: number, name: string, description: string) => async dispatch => {
    const req = {
        id: id,
        name: name,
        description: description
    }

    console.log("req", req);
    
    try {
        dispatch(commonActions.fetchStart());
        const { error, data } = await CategoryItemAPI.updateCategoryItem(req);
        console.log("respones: ", data);
        
        if (error.code ===0 ){
            dispatch(commonActions.fetchSuccess("Edited"));
        } else {
            console.log("padload: data.error: ", error.message);
        } 
        
    } catch(error) {
        console.log("Error****:", error.message);
        dispatch(commonActions.fetchError(error.message));
    } finally {
    }
} 

const deleteCategoryItem = (id : number) => async dispatch => {
    try {
        dispatch(commonActions.fetchStart());
        const { error, data } = await CategoryItemAPI.deleteCategoryItem(id);
        console.log("respones: ", data);
        
        if (error.code ===0 ){
            dispatch(commonActions.fetchSuccess("Deleted: " + id));
        } else {
            console.log("padload: data.error: ", error.message);
        } 
        
    } catch(error) {
        console.log("Error****:", error.message);
        dispatch(commonActions.fetchError(error.message));
    } finally {
    }
}

export const categoryItemActions = {
    createCategoryItem,
    getAllCategoryItems,
    updateCategoryItem,
    deleteCategoryItem
}