import {createSlice, PayloadAction} from '@reduxjs/toolkit';
import {AppThunk} from "../index";
import {AuthAPI} from "../../api/AuthAPI";
import {ISignIn} from "../../model/Signin";
import {ISignUp} from "../../model/Signup";
import {isEmpty} from 'lodash';
import {MeActions} from "./meSlice";
import {StorageHelper} from "../../helper/Storage";
import {IAuthority} from "../../model/entity/IAuthority";
import {IAuthState} from "../../model/state/IAuthenState";
import {addressActions} from "./addressSlice";

const initialState: IAuthState = {
  token: '',
  authorities: [] as IAuthority[]
};

/**
 * Simplifying Slice
 */
const authSlice = createSlice({
  name: 'auth',
  initialState,
  reducers: {
    setToken(state: IAuthState, action: PayloadAction<string>) {
      state.token = action.payload;
    },
    setAuthorities(state: IAuthState, action: PayloadAction<Array<IAuthority>>) {
      state.authorities = action.payload;
    }
  }
});

/**
 *  Extract the action creators object and the reducer
 */
const {actions, reducer} = authSlice;

/**
 * Export the reducer, either as a default or named export
 */
export default reducer;

/**
 * ==========================================================================
 * Define a thunk that dispatches those action creators
 * More: https://redux-toolkit.js.org/usage/usage-guide#defining-async-logic-in-slices
 */
const login = (reqSignIn: ISignIn, successCallback: VoidFunction, failCallback: VoidFunction): AppThunk => async dispatch => {
  try {
    const {error, data} = await AuthAPI.signIn(reqSignIn);

    if (error.code === 1) {
      await StorageHelper.setCookie('token', data.token, 3650);
      await dispatch(actions.setToken(data.token));
      await dispatch(addressActions.getAddress());
      successCallback();
    } else {
      console.log("Payload: error ", error.message);
      failCallback();
    }

  } catch (error) {
    console.log("Payload: error ", error.message);
    failCallback();
  } finally {
  }
};

const register = (reqSignUp: ISignUp, successCallback: VoidFunction, failCallback: (error: string) => void): AppThunk => async dispatch => {
  try {
    const {error, data} = await AuthAPI.signUp(reqSignUp);

    if (error.code === 1) {
      successCallback();
    } else {
      console.log("Payload: error ", error.message);
      failCallback(error.details[0]);
    }

  } catch (error) {
    console.log("Payload: error ", error);
    failCallback(error.message);
  }
};

const logout = (successCallback: VoidFunction): AppThunk => async dispatch => {
  dispatch(MeActions.logOut());
  dispatch(addressActions.removeAddress());
  dispatch(actions.setToken(""));
  StorageHelper.removeCookie('token');
  successCallback();
};

const validateToken = (ctx): AppThunk => async (dispatch) => {
  const token = StorageHelper.get2SideCookie(ctx, "token");

  if (isEmpty(token)) {
    await dispatch(MeActions.setGuestUser());
    return;
  }

  await dispatch(MeActions.getUserInfo(token));
  await dispatch(addressActions.getAddress());
};

const updateAuthorities = (authorities: Array<IAuthority>): AppThunk => async (dispatch) => {
  await dispatch(actions.setAuthorities(authorities));
};

/**
 * Export each action creator by name
 */
export const authActions = {
  login,
  register,
  validateToken,
  logout,
  updateAuthorities
};
