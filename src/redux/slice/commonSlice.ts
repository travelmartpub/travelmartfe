import {createSlice, PayloadAction} from '@reduxjs/toolkit';
import {AppThunk} from "../index";
import {ICommon} from "../../model/ICommon";


const initialState: ICommon = {
  isLoading: false,
  error: null,
  message: ""
};

/**
 * Simplifying Slice
 */
const commonSlice = createSlice({
  name: 'common',
  initialState,
  reducers: {
    fetchStartReducer(state: ICommon) {
      state.error = '';
      state.message = '';
      state.isLoading = true
    },

    fetchSuccessReducer(state: ICommon, action: PayloadAction<string>) {
      state.error = '';
      state.message = action.payload ? action.payload : '';
      state.isLoading = false
    },

    fetchErrorReducer(state: ICommon, action: PayloadAction<string>) {
      state.error = action.payload;
      state.message = '';
      state.isLoading = false
    },

    showMessageReducer(state: ICommon, action: PayloadAction<string>) {
      state.error = '';
      state.message = action.payload;
      state.isLoading = false
    },

    hideMessageReducer(state: ICommon) {
      state.error = '';
      state.message = '';
      state.isLoading = false
    }
  }
});

/**
 *  Extract the action creators object and the reducer
 */
const {actions, reducer} = commonSlice;

/**
 * Extract and export each action creator by name
 */
export const {
  fetchStartReducer,
  fetchSuccessReducer,
  fetchErrorReducer,
  showMessageReducer,
  hideMessageReducer
} = actions;

/**
 * Export the reducer, either as a default or named export
 */
export default reducer;

/**
 * ==========================================================================
 * Define a thunk that dispatches those action creators
 * More: https://redux-toolkit.js.org/usage/usage-guide#defining-async-logic-in-slices
 */
const fetchStart = (): AppThunk => async dispatch => {
  dispatch(fetchStartReducer());
};

const hideMessage = (): AppThunk => async dispatch => {
  dispatch(hideMessageReducer());
};

const fetchSuccess = (message?: string): AppThunk => async dispatch => {
  dispatch(fetchSuccessReducer(message));

  setTimeout(() => {
    dispatch(hideMessageReducer());
  }, 3000);
};

const fetchError = (message?: string): AppThunk => async dispatch => {
  dispatch(fetchErrorReducer(message));

  setTimeout(() => {
    dispatch(hideMessageReducer());
  }, 3000);
};

const showMessage = (message?: string): AppThunk => async dispatch => {
  dispatch(showMessageReducer(message));

  setTimeout(() => {
    dispatch(hideMessageReducer());
  }, 3000);
};

/**
 * Export each action creator by name
 */
export const commonActions = {
  fetchStart,
  fetchSuccess,
  fetchError,
  showMessage,
  hideMessage
};