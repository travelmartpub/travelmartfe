import {createSlice, PayloadAction} from '@reduxjs/toolkit';
import {IService, IServiceReq, IServices} from "../../model/IService";
import {ServiceAPI} from "../../api/ServiceAPI";

const initialState: IServices = {
  isLoading: false,
  error: null,
  services: [],
  isLoadingCreate: false
};

/**
 * Simplifying Slice
 */
const serviceSlice = createSlice({
  name: 'service',
  initialState,
  reducers: {
    getAllServicesStart(state: IServices) {
      state.isLoading = true;
    },
    getAllServicesSuccess(state: IServices, action: PayloadAction<Array<IService>>) {
      state.isLoading = false;
      state.services = action.payload;
    },
    getAllServicesFailed(state: IServices, action: PayloadAction<string>) {
      state.isLoading = false;
      state.error = action.payload;
    },
    createServiceStart(state: IServices) {
      state.isLoadingCreate = true;
    },
    createServiceSuccess(state: IServices) {
      state.isLoadingCreate = false;
    },
    createServiceFailed(state: IServices, action: PayloadAction<string>) {
      state.isLoadingCreate = false;
      state.error = action.payload;
    },
  }
});

/**
 *  Extract the action creators object and the reducer
 */
const {actions, reducer} = serviceSlice;

/**
 * Extract and export each action creator by name
 */
export const {
  getAllServicesStart,
  getAllServicesSuccess,
  getAllServicesFailed,
  createServiceStart,
  createServiceSuccess,
  createServiceFailed
} = actions;

/**
 * Export the reducer, either as a default or named export
 */
export default reducer;

/**
 * ==========================================================================
 * Define a thunk that dispatches those action creators
 * More: https://redux-toolkit.js.org/usage/usage-guide#defining-async-logic-in-slices
 */

const create = (req: IServiceReq) => async dispatch => {
  console.log("req", req);

  try {
    dispatch(createServiceStart());

    const {error, data} = await ServiceAPI.createService(req);
    console.log("response: ", data);

    if (error.code === 0) {
      await dispatch(createServiceSuccess());

    } else {
      console.log("payload: data.error", error.message);
      dispatch(createServiceFailed(error.message));
    }
  } catch (error) {
    console.log("Error****:", error.message);
    dispatch(createServiceFailed(error.message));
  } finally {
  }
};

/**
 * Get all services
 */

const getAllServices = () => async dispatch => {
  try {
    dispatch(getAllServicesStart());

    const {error, data} = await ServiceAPI.getAllServices();

    if (error.code === 0) {
      await dispatch(getAllServicesSuccess(data));
    } else {
      console.log("payload: data.error", error.message);
      await dispatch(getAllServicesFailed(error.message));
    }
  } catch (error) {
    console.log("Error****:", error.message);
    await dispatch(getAllServicesFailed(error.message));
  } finally {
  }
};

/**
 * Export each action creator by name
 */export const serviceActions = {
  create,
  getAllServices
};
