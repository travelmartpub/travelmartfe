import {createSlice, PayloadAction} from '@reduxjs/toolkit';
import {ProductAPI} from "../../api/ProductAPI";
import {IProductState} from "../../model/state/IProductState";
import {IProduct} from "../../model/entity/IProduct";
import {IErrorState} from "../../model/state/IErrorState";
import {CommonHelper} from "../../helper/CommonHelper";
import {IProductRequest} from "../../model/IProductRequest";
import {StoreAPI} from "../../api/StoreAPI";
import {AppThunk} from "../index";

const initialState: IProductState = {
  isLoading: false,
  error: {
    isError: false,
    codeError: -1,
    msgError: ""
  },
  product: {
    id: 0,
    name: "",
    description: "",
    price: 0,
    activated: false,
    stock: 0,
    sold: 0,
    createdAt: "",
    images: "",
    storeId: 0
  }
};

/**
 * Simplifying Slice
 */
const productSlice = createSlice({
  name: 'product',
  initialState,
  reducers: {
    setProduct(state: IProductState, action: PayloadAction<IProduct>) {
      state.product = action.payload;
      state.isLoading = false;
    },
    fireError(state: IProductState, action: PayloadAction<IErrorState>) {
      state.error = {...action.payload, isError: true};
      state.isLoading = false;
    },
    startFetch(state: IProductState) {
      state.isLoading = true;
    }
  }
});

/**
 *  Extract the action creators object and the reducer
 */
const {actions, reducer} = productSlice;

/**
 * Export the reducer, either as a default or named export
 */
export default reducer;

/**
 * ==========================================================================
 * Define a thunk that dispatches those action creators
 * More: https://redux-toolkit.js.org/usage/usage-guide#defining-async-logic-in-slices
 */

/**
 * Fetch product by id
 */

const fetchProductById = (productId: string | string[]) => async dispatch => {
  try {
    await dispatch(actions.startFetch());
    const {error, data} = await ProductAPI.fetchProductById(Number(productId));

    if (error.code === 1) {
      console.log(data);
      await dispatch(actions.setProduct(data));
      return;
    }

    console.log("Payload: error", error);
    await dispatch(actions.fireError(CommonHelper.getError(error)));

  } catch (error) {
    console.log("Payload: error", error);
    await dispatch(actions.fireError(CommonHelper.getError(error)));
  } finally {
  }
};

const fetchProductByIdLocalState = (productId: string | string[], successfulCallback: (product) => void, failureCallback: VoidFunction) => async dispatch => {
  try {
    const {error, data} = await ProductAPI.fetchProductById(Number(productId));

    if (error.code === 1) {
      successfulCallback(data);
      return;
    }

    console.log("Payload: error", error);
    failureCallback();

  } catch (error) {
    console.log("Payload: error", error);
    failureCallback();
  } finally {
  }
};

const createProduct = (product: IProductRequest, successCallback: (data: IProduct) => void, failCallback: (error: string) => void): AppThunk => async dispatch => {
  try {
    const {error, data} = await StoreAPI.createProduct(product);

    if (error.code === 1) {
      successCallback(data);
      return;
    } else {
      await dispatch(actions.fireError(CommonHelper.getError(error)));
      failCallback(error.message);
    }
  } catch (error) {
    await dispatch(actions.fireError(CommonHelper.getError(error)));
    failCallback(error.message);
  } finally {
  }
}

const updateProduct = (product: IProductRequest, successCallback: (data: IProduct) => void, failCallback: (error: string) => void): AppThunk => async dispatch => {
  try {
    const {error, data} = await StoreAPI.updateProduct(product);

    if (error.code === 1) {
      successCallback(data);
      return;
    } else {
      await dispatch(actions.fireError(CommonHelper.getError(error)));
      failCallback(error.message);
    }
  } catch (error) {
    await dispatch(actions.fireError(CommonHelper.getError(error)));
    failCallback(error.message);
  } finally {
  }
}

const setActivateProduct = (productId: string, activate: boolean, successCallback: (data: IProduct) => void, failCallback: (error: string) => void): AppThunk => async dispatch => {
  try {
    const {error, data} = await StoreAPI.deactivateProduct(productId, activate);

    if (error.code === 1) {
      successCallback(data);
      return;
    } else {
      await dispatch(actions.fireError(CommonHelper.getError(error)));
      failCallback(error.message);
    }
  } catch (error) {
    await dispatch(actions.fireError(CommonHelper.getError(error)));
    failCallback(error.message);
  } finally {
  }
}

/**
 * Export each action creator by name
 */
export const ProductActions = {
  fetchProductById,
  fetchProductByIdLocalState,
  createProduct,
  updateProduct,
  setActivateProduct
};
