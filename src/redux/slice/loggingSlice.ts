import {createSlice, PayloadAction} from '@reduxjs/toolkit';
import {ILogging} from "../../model/ILogging";
import {LogViewAPI} from "../../api/LogViewAPI";

const initialState: ILogging = {
  isLoading: false,
  error: null,
  logs: []
};

/**
 * Simplifying Slice
 */
const loggingSlice = createSlice({
  name: 'logging',
  initialState,
  reducers: {
    getLogsStart(state: ILogging) {
      state.isLoading = true;
    },
    getLogsSuccess(state: ILogging, action: PayloadAction<Array<string>>) {
      state.isLoading = false;
      state.logs = action.payload;
    },
    getLogsFailed(state: ILogging, action: PayloadAction<string>) {
      state.isLoading = false;
      state.error = action.payload;
    }
  }
});

// Extract the action creators object and the reducer
const {actions, reducer} = loggingSlice;

// Extract and export each action creator by name
export const {
  getLogsStart,
  getLogsSuccess,
  getLogsFailed
} = actions;

// Export the reducer, either as a default or named export
export default reducer;

/**
 * Define a thunk that dispatches those action creators
 * More: https://redux-toolkit.js.org/usage/usage-guide#defining-async-logic-in-slices
 */

/**
 * Get all log view
 */
const getLogs = (logName: string) => async dispatch => {

  try {
    dispatch(getLogsStart());

    const {error, data} = await LogViewAPI.getAllLogs(logName);

    if (error.code === 0) {
      await dispatch(getLogsSuccess(data.logs));
    } else {
      console.log(error.message);
      dispatch(getLogsFailed(error.message));
    }
  } catch (error) {
    console.log("Error****:", error.message);
    dispatch(getLogsFailed(error.message));
  } finally {
  }
};

// Export each action creator by name
export const loggingActions = {
  getLogs
};
