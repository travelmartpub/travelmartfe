import {createSlice, PayloadAction} from '@reduxjs/toolkit';
import {IErrorState} from "../../../model/state/IErrorState";
import {CommonHelper} from "../../../helper/CommonHelper";
import {IOrderAdminState} from "../../../model/state/admin/IOrderAdminState";
import {IOrder, IOrderProductDetail, IOrderStoreDetail} from "../../../model/entity/IOrder";
import {OrderAPI} from "../../../api/OrderAPI";
import {StoreAPI} from "../../../api/StoreAPI";

const initialState: IOrderAdminState = {
  isLoading: false,
  error: {
    isError: false,
    codeError: -1,
    msgError: ""
  },
  orders: [] as IOrder[],
  orderStoreDetails: [] as IOrderStoreDetail[],
  orderProductDetails: [] as IOrderProductDetail[]
};

/**
 * Simplifying Slice
 */
const slice = createSlice({
  name: 'orderAdmin',
  initialState,
  reducers: {
    setOrders(state: IOrderAdminState, action: PayloadAction<IOrder>) {
      state.orders.push(action.payload);
      state.isLoading = false;
    },
    setOrderStores(state: IOrderAdminState, action: PayloadAction<Array<IOrderStoreDetail>>) {
      state.orderStoreDetails = action.payload;
      state.isLoading = false;
    },
    setOrderProducts(state: IOrderAdminState, action: PayloadAction<Array<IOrderProductDetail>>) {
      for (const value of action.payload) {
        state.orderProductDetails.push(value);
      }
      state.isLoading = false;
    },
    fireError(state: IOrderAdminState, action: PayloadAction<IErrorState>) {
      state.error = {...action.payload, isError: true};
      state.isLoading = false;
    },
    startFetch(state: IOrderAdminState) {
      state.isLoading = true;
    }
  }
});

/**
 *  Extract the action creators object and the reducer
 */
const {actions, reducer} = slice;

/**
 * Export the reducer, either as a default or named export
 */
export default reducer;

/**
 * ==========================================================================
 * Define a thunk that dispatches those action creators
 * More: https://redux-toolkit.js.org/usage/usage-guide#defining-async-logic-in-slices
 */

const fetchOrderStoreDetails = () => async dispatch => {
  try {

    dispatch(actions.startFetch());
    const {error, data, paging} =
        await OrderAPI.fetchOrderStoreDetails();

    if (error.code === 1) {

      dispatch(actions.setOrderStores(data));
    }

  } catch (error) {
    console.log("Payload: error", error);
    dispatch(actions.fireError(CommonHelper.getError(error)));
  }
};

const fetchOrderStoreDetailsLocalState = (successfulCallback: (orderStoreDetails, paging) => void) => async dispatch => {
  try {
    const {error, data, paging} =
        await OrderAPI.fetchOrderStoreDetails();

    if (error.code === 1) {
      successfulCallback(data, paging);

    }
    console.log("Payload: error", error);
  } catch (error) {
    console.log("Payload: error", error);
  }
};

const fetchOrders = (orderStoreIds: Array<string>) => async dispatch => {
  try {

    dispatch(actions.startFetch());
    for (const value of orderStoreIds) {
      const {error, data} = await OrderAPI.fetchOrder(value);
      if (error.code == 1) {
        dispatch(actions.setOrders(data));
      }
    }

  } catch (error) {
    console.log("Payload: error", error);
    dispatch(actions.fireError(CommonHelper.getError(error)));
  }
}

const fetchProducts = (orderStoreIds: Array<string>) => async dispatch => {

  try {

    dispatch(actions.startFetch());
    for (const value of orderStoreIds) {
      const {error, data} = await OrderAPI.fetchOrderProductDetails(value);
      if (error.code == 1) {
        dispatch(actions.setOrderProducts(data));
      }
    }

  } catch (error) {
    console.log("Payload: error", error);
    dispatch(actions.fireError(CommonHelper.getError(error)));
  }


}

const acceptOrder = (orderStoreId: number) => async dispatch => {
  try {
    const {error, data, paging} =
        await StoreAPI.acceptOrder(orderStoreId);

    if (error.code === 1) {
      return;
    } else {
      console.log("Payload: error", error);
      dispatch(actions.fireError(CommonHelper.getError(error)));
    }

  } catch (error) {
    console.log("Payload: error", error);
    dispatch(actions.fireError(CommonHelper.getError(error)));
  }
}

const declineOrder = (orderStoreId: number) => async dispatch => {
  try {
    const {error, data, paging} =
        await StoreAPI.declineOrder(orderStoreId);

    if (error.code === 1) {
      return;
    } else {
      console.log("Payload: error", error);
      dispatch(actions.fireError(CommonHelper.getError(error)));
    }

  } catch (error) {
    console.log("Payload: error", error);
    dispatch(actions.fireError(CommonHelper.getError(error)));
  }
}

const shipOrder = (orderStoreId: number) => async dispatch => {
  try {
    const {error, data, paging} =
        await StoreAPI.shipOrder(orderStoreId);

    if (error.code === 1) {
      return;
    } else {
      console.log("Payload: error", error);
      dispatch(actions.fireError(CommonHelper.getError(error)));
    }

  } catch (error) {
    console.log("Payload: error", error);
    dispatch(actions.fireError(CommonHelper.getError(error)));
  }
}

/**
 * Export each action creator by name
 */
export const OrderAdminActions = {
  fetchOrderStoreDetails,
  fetchOrders,
  fetchProducts,
  fetchOrderStoreDetailsLocalState,
  acceptOrder,
  declineOrder,
  shipOrder
};
