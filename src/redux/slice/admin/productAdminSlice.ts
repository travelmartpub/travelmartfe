import {createSlice, PayloadAction} from '@reduxjs/toolkit';
import {IProduct} from "../../../model/entity/IProduct";
import {IErrorState} from "../../../model/state/IErrorState";
import {CommonHelper} from "../../../helper/CommonHelper";
import {IProductAdminState} from "../../../model/state/admin/IProductAdminState";
import {StoreAPI} from "../../../api/StoreAPI";
import {IPaging} from "../../../model/entity/IPaging";

const initialState: IProductAdminState = {
  isLoading: false,
  error: {
    isError: false,
    codeError: -1,
    msgError: ""
  },
  products: [] as IProduct[],
  paging: {
    limit: 0,
    offset: 0,
    orderBy: null,
    total: 0
  }
};

/**
 * Simplifying Slice
 */
const productSlice = createSlice({
  name: 'productAdmin',
  initialState,
  reducers: {
    setProducts(state: IProductAdminState, action: PayloadAction<Array<IProduct>>) {
      state.products = action.payload;
      state.isLoading = false;
    },
    setPaging(state: IProductAdminState, action: PayloadAction<IPaging>) {
      state.paging = action.payload;
      state.isLoading = false;
    },
    fireError(state: IProductAdminState, action: PayloadAction<IErrorState>) {
      state.error = {...action.payload, isError: true};
      state.isLoading = false;
    },
    startFetch(state: IProductAdminState) {
      state.isLoading = true;
    }
  }
});

/**
 *  Extract the action creators object and the reducer
 */
const {actions, reducer} = productSlice;

/**
 * Export the reducer, either as a default or named export
 */
export default reducer;

/**
 * ==========================================================================
 * Define a thunk that dispatches those action creators
 * More: https://redux-toolkit.js.org/usage/usage-guide#defining-async-logic-in-slices
 */

const fetchProductsByStoreId = (storeId: number, limit: number, offset: number, orderBy?: string) => async dispatch => {
  try {

    dispatch(actions.startFetch());
    const {error, data, paging} =
        await StoreAPI.fetchStoreProduct(Number(limit), Number(offset), String(orderBy));

    if (error.code === 1) {
      dispatch(actions.setProducts(data));
      dispatch(actions.setPaging(paging))
      return;
    }

    console.log("Payload: error", error);
    dispatch(actions.fireError(CommonHelper.getError(error)));

  } catch (error) {
    console.log("Payload: error", error);
    dispatch(actions.fireError(CommonHelper.getError(error)));
  }
};

const fetchProductsByStoreIdLocalState = (storeId: number, limit: number, offset: number,
                                          successfulCallback: (products, paging) => void) => async dispatch => {
  try {
    const {error, data, paging} =
        await StoreAPI.fetchStoreProduct(Number(limit), Number(offset), null);

    if (error.code === 1) {
      successfulCallback(data, paging);
      return;
    }

    console.log("Payload: error", error);
  } catch (error) {
    console.log("Payload: error", error);
  }
};

const resetData = () => async dispatch => {
  dispatch(actions.setProducts(initialState.products));
  dispatch(actions.setPaging(initialState.paging));
  dispatch(actions.startFetch);
};

/**
 * Export each action creator by name
 */
export const ProductAdminActions = {
  fetchProductsByStoreId,
  resetData,
  fetchProductsByStoreIdLocalState
};
