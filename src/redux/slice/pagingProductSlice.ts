import {createSlice, PayloadAction} from "@reduxjs/toolkit";
import {IPagingProductsState} from "../../model/state/IPagingProducts";
import {IErrorState} from "../../model/state/IErrorState";
import {IProduct} from "../../model/entity/IProduct";
import {CommonHelper} from "../../helper/CommonHelper";
import {ProductAPI} from "../../api/ProductAPI";
import {IPaging} from "../../model/entity/IPaging";

const initialState: IPagingProductsState = {
  isLoading: false,
  error: {
    isError: false,
    codeError: -1,
    msgError: ""
  },
  products: [],
  paging: null
};

/**
 * Simplifying Slice
 */
const pagingProductsSlice = createSlice({
  name: 'pagingProduct',
  initialState,
  reducers: {
    fireError(state: IPagingProductsState, action: PayloadAction<IErrorState>) {
      state.error = {...action.payload, isError: true};
      state.isLoading = false;
    },
    startFetch(state: IPagingProductsState) {
      state.isLoading = true;
    },
    setProducts(state: IPagingProductsState, action: PayloadAction<Array<IProduct>>) {
      state.products = action.payload;
      state.isLoading = false;
    },
    setPaging(state: IPagingProductsState, action: PayloadAction<IPaging>) {
      state.paging = action.payload;
      state.isLoading = false;
    }
  }
});

/**
 *  Extract the action creators object and the reducer
 */
const {actions, reducer} = pagingProductsSlice;

/**
 * Export the reducer, either as a default or named export
 */
export default reducer;

/**
 * ==========================================================================
 * Define a thunk that dispatches those action creators
 * More: https://redux-toolkit.js.org/usage/usage-guide#defining-async-logic-in-slices
 */

/**
 * Fetch products
 */

const fetchPagingProducts = (limit: number, offset: number, orderBy?: string) => async dispatch => {
  try {
    await dispatch(actions.startFetch());
    const {
      error,
      data,
      paging
    } = await ProductAPI.fetchPagingProducts(Number(limit), Number(offset), String(orderBy));

    if (error.code === 1) {
      await dispatch(actions.setProducts(data));
      await dispatch(actions.setPaging(paging));
      return;
    }

    console.log("Payload: error", error);
    await dispatch(actions.fireError(CommonHelper.getError(error)));

  } catch (error) {
    console.log("Payload: error", error);
    await dispatch(actions.fireError(CommonHelper.getError(error)));
  }
};

const fetchPagingProductsLocalState = (limit: number, offset: number, successfulCallback: (data, paging) => void) => async dispatch => {
  try {
    const {
      error,
      data,
      paging
    } = await ProductAPI.fetchPagingProducts(Number(limit), Number(offset), null);

    if (error.code === 1) {
      successfulCallback(data, paging);
      return;
    }

    console.log("Payload: error", error);
  } catch (error) {
    console.log("Payload: error", error);
  }
};

const fetchStorePagingProducts = (storeId: number, limit: number, offset: number) => async dispatch => {
  try {
    await dispatch(actions.startFetch());
    const {
      error,
      data,
      paging
    } = await ProductAPI.fetchStorePagingProducts(storeId, Number(limit), Number(offset));

    if (error.code === 1) {
      await dispatch(actions.setProducts(data));
      await dispatch(actions.setPaging(paging));
    }

    console.log("Payload: error", error);
    await dispatch(actions.fireError(CommonHelper.getError(error)));

  } catch (error) {
    console.log("Payload: error", error);
    await dispatch(actions.fireError(CommonHelper.getError(error)));
  }
};

const fetchStorePagingProductsLocalState = (storeId: number, limit: number, offset: number, successfulCallback: (data, paging) => void) => async dispatch => {
  try {
    const {
      error,
      data,
      paging
    } = await ProductAPI.fetchStorePagingProducts(storeId, Number(limit), Number(offset));

    if (error.code === 1) {
      successfulCallback(data, paging);
    }

    console.log("Payload: error", error);
  } catch (error) {
    console.log("Payload: error", error);
  }
};

/**
 * Export each action creator by name
 */
export const PagingProductsActions = {
  fetchPagingProducts,
  fetchStorePagingProducts,
  fetchPagingProductsLocalState,
  fetchStorePagingProductsLocalState
};