import {createSlice, PayloadAction} from '@reduxjs/toolkit';
import {IErrorState} from "../../model/state/IErrorState";
import {CommonHelper} from "../../helper/CommonHelper";
import {IStoreState} from "../../model/state/IStoreState";
import {IStore} from "../../model/entity/IStore";
import {StoreAPI} from "../../api/StoreAPI";

const initialState: IStoreState = {
  isLoading: false,
  error: {
    isError: false,
    codeError: -1,
    msgError: ""
  },
  store: {
    id: 0,
    name: "",
    active: true
  }
};

/**
 * Simplifying Slice
 */
const storeSlice = createSlice({
  name: 'store',
  initialState,
  reducers: {
    setStore(state: IStoreState, action: PayloadAction<IStore>) {
      state.store = action.payload;
      state.isLoading = false;
    },
    fireError(state: IStoreState, action: PayloadAction<IErrorState>) {
      state.error = {...action.payload, isError: true};
      state.isLoading = false;
    },
    startFetch(state: IStoreState) {
      state.isLoading = true;
    }
  }
});

/**
 *  Extract the action creators object and the reducer
 */
const {actions, reducer} = storeSlice;

/**
 * Export the reducer, either as a default or named export
 */
export default reducer;

/**
 * ==========================================================================
 * Define a thunk that dispatches those action creators
 * More: https://redux-toolkit.js.org/usage/usage-guide#defining-async-logic-in-slices
 */

/**
 * Fetch product by id
 */

const fetchStoreById = (storeId: string | string[]) => async dispatch => {
  try {
    await dispatch(actions.startFetch());
    const {error, data} = await StoreAPI.fetchStoreById(Number(storeId));

    if (error.code === 1) {
      console.log(data);
      await dispatch(actions.setStore(data));
      return;
    }

    console.log("Payload: error", error);
    await dispatch(actions.fireError(CommonHelper.getError(error)));

  } catch (error) {
    console.log("Payload: error", error);
    await dispatch(actions.fireError(CommonHelper.getError(error)));
  } finally {
  }
};

const fetchStoreByIdLocalState = (storeId: number, successfulCallback: (store) => void, failCallBack: () => void) => async dispatch => {
  try {
    const {error, data} = await StoreAPI.fetchStoreById(Number(storeId));

    if (error.code === 1) {
      successfulCallback(data);
      return;
    }

    failCallBack();

  } catch (error) {
    failCallBack();
  }
};

const createUserStore = (storeName: string, successfulCallback: VoidFunction, failureCallback: VoidFunction) => async dispatch => {
  try {
    await dispatch(actions.startFetch());
    const {error, data} = await StoreAPI.createUserStore(storeName);

    if (error.code === 1) {
      await dispatch(actions.setStore(data));
      successfulCallback();
    } else {
      await dispatch(actions.fireError(CommonHelper.getError(error)));
      failureCallback();
    }
  } catch (error) {
    await dispatch(actions.fireError(CommonHelper.getError(error)));
    failureCallback();
  }
}

/**
 * Export each action creator by name
 */
export const StoreActions = {
  fetchStoreById,
  createUserStore,
  fetchStoreByIdLocalState
};
