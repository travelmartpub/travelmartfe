import {createSlice, PayloadAction} from '@reduxjs/toolkit';
import {BenchmarkAPI} from "../../api/BenchmarkAPI";
import {commonActions} from "./commonSlice";
import _ from 'lodash';
import {IExecution, IExecutions} from "../../model/IBenchmark";

const initialState: IExecutions = {
  isLoading: false,
  error: null,
  executions: []
};

/**
 * Simplifying Slice
 */
const executionSlice = createSlice({
  name: 'benchmark',
  initialState,
  reducers: {
    getAllMeExecutionsStart(state: IExecutions) {
      state.isLoading = true;
    },
    getAllMeExecutionsSuccess(state: IExecutions, action: PayloadAction<Array<IExecution>>) {
      state.isLoading = false;
      state.executions = action.payload;
    },
    getAllMeExecutionsFailed(state: IExecutions, action: PayloadAction<string>) {
      state.isLoading = false;
      state.error = action.payload;
    }
  }
});

/**
 *  Extract the action creators object and the reducer
 */
const {actions, reducer} = executionSlice;

/**
 * Extract and export each action creator by name
 */
export const {
  getAllMeExecutionsStart,
  getAllMeExecutionsSuccess,
  getAllMeExecutionsFailed
} = actions;

/**
 * Export the reducer, either as a default or named export
 */
export default reducer;

/**
 * ==========================================================================
 * Define a thunk that dispatches those action creators
 * More: https://redux-toolkit.js.org/usage/usage-guide#defining-async-logic-in-slices
 */
const execute = (script: string[], ccu: number, duration: number) => async dispatch => {
  const reqId = Date.now();

  const req = {
    ccu: ccu,
    duration: duration,
    reqId: reqId,
    scriptPath: _.join(script, "/")
  };

  console.log("req", req);

  try {
    dispatch(commonActions.fetchStart());
    const {error, data} = await BenchmarkAPI.execute(req);
    console.log("response: ", data);

    if (error.code === 0) {
      dispatch(commonActions.fetchSuccess("The test script is executing"));
    } else {
      console.log("payload: data.error", error.message);
      dispatch(commonActions.fetchError(error.message));
    }
  } catch (error) {
    console.log("Error****:", error.message);
    dispatch(commonActions.fetchError(error.message));
  } finally {
  }
};

/**
 * Get all executions
 */

const getExecutions = (username: string) => async dispatch => {
  try {
    dispatch(getAllMeExecutionsStart());

    const {error, data} = await BenchmarkAPI.getExecutions(username);

    if (error.code === 0) {
      await dispatch(getAllMeExecutionsSuccess(data));
    } else {
      console.log("payload: data.error", error.message);
      await dispatch(getAllMeExecutionsFailed(error.message));
    }
  } catch (error) {
    console.log("Error****:", error.message);
    await dispatch(getAllMeExecutionsFailed(error.message));
  } finally {
  }
};

/**
 * Export each action creator by name
 */export const benchmarkActions = {
  execute,
  getExecutions
};
