import {createSlice, PayloadAction} from "@reduxjs/toolkit";
import {OrderAPI} from "../../api/OrderAPI";
import {IOrderState} from "../../model/state/IOrderState";
import {IOrderRequest} from "../../model/common/IOrderRequest";
import {IOrder} from "../../model/entity/IOrder";
import {IPaging} from "../../model/entity/IPaging";

const initialState: IOrderState = {
  orders: []
};

/**
 * Simplifying Slice
 */

const orderSlice = createSlice({
  name: "order",
  initialState,
  reducers: {
    setOrders(state: IOrderState, action: PayloadAction<Array<IOrder>>) {
      state.orders = action.payload;
    },
    removeOrders(state: IOrderState) {
      state.orders = initialState.orders;
    }
  }
});

/**
 *  Extract the action creators object and the reducer
 */
const {actions, reducer} = orderSlice;

/**
 * Export the reducer, either as a default or named export
 */
export default reducer;

/**
 * ==========================================================================
 * Define a thunk that dispatches those action creators
 * More: https://redux-toolkit.js.org/usage/usage-guide#defining-async-logic-in-slices
 */

const createOrder = (req: IOrderRequest, successCallback: (orderId) => void, failCallback: VoidFunction) => async dispatch => {

  try {
    const {error, data} = await OrderAPI.createOrder(req);

    if (error.code === 1) {
      successCallback(data.orderId);
    } else {
      failCallback();
      console.log("Payload: error ", error.message);
    }
  } catch (error) {
    failCallback();
    console.log("Payload: error ", error.message);
  } finally {

  }
};

const fetchOrders = (limit: number, offset: number, successCallback: (orders: Array<IOrder>, paging: IPaging) => void, failureCallBack: VoidFunction) => async dispatch => {
  try {
    const {error, data, paging} = await OrderAPI.fetchOrders(limit, offset);

    if (error.code === 1) {
      successCallback(data, paging);
    } else {
      failureCallBack();
    }
  } catch (error) {
    failureCallBack();
  } finally {

  }
}

const fetchOrdersByStore = (limit: number, offset: number, successCallback: (orders: Array<IOrder>, paging: IPaging) => void, failureCallBack: VoidFunction) => async dispatch => {
  try {
    const {error, data, paging} = await OrderAPI.fetchOrdersByStore(limit, offset);

    if (error.code === 1) {
      successCallback(data, paging);
    } else {
      failureCallBack();
    }
  } catch (error) {
    failureCallBack();
  } finally {

  }
}

/**
 * Export each action creator by name
 */
export const OrderActions = {
  createOrder,
  fetchOrders,
  fetchOrdersByStore
};