import {combineReducers} from '@reduxjs/toolkit';
import commonSlice from "./slice/commonSlice";
import settingsSlice from './slice/settingsSlice';
import benchmarkSlice from './slice/benchmarkSlice';
import authSlice from './slice/authSlice';
import meSlice from './slice/meSlice';
import categoryItemSlice from './slice/categoryItemSlice';
import orderSlice from './slice/orderSlice';
import loggingSlice from './slice/loggingSlice';
import serviceSlice from './slice/serviceSlice';
import productSlice from './slice/productSlice';
import cartSlice from "./slice/cartSlice";
import storeSlice from "./slice/storeSlice";
import pagingProductSlice from "./slice/pagingProductSlice";
import productAdminSlice from "./slice/admin/productAdminSlice";
import orderAdminSlice from "./slice/admin/orderAdminSlice";
import addressSlice from "./slice/addressSlice";

export function createRootReducer() {
  return combineReducers({
    common: commonSlice,
    settings: settingsSlice,
    benchmark: benchmarkSlice,
    categoryItem: categoryItemSlice,
    order: orderSlice,
    cart: cartSlice,
    auth: authSlice,
    me: meSlice,
    logging: loggingSlice,
    service: serviceSlice,
    product: productSlice,
    pagingProducts: pagingProductSlice,
    store: storeSlice,
    productAdmin: productAdminSlice,
    orderAdmin: orderAdminSlice,
    address: addressSlice
  });
}
