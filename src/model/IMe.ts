import {IRootState} from "./IRootState";

export interface IMe extends IRootState {
  profile: IProfile;
}

export interface IProfile {
  id: number,
  login: string,
  firstName: string,
  lastName: string,
  email: string,
  imageUrl: string,
  activated: boolean,
  langKey: string,
  createdBy: string,
  createdDate: Date,
  lastModifiedBy: string,
  lastModifiedDate: Date,
  authorities: Array<string>
}