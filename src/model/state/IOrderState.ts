import {IOrder} from "../entity/IOrder";

export interface IOrderState {
  orders: Array<IOrder>
}
