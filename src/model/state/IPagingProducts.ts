import {IErrorState} from "./IErrorState";
import {IProduct} from "../entity/IProduct";
import {IPaging} from "../entity/IPaging";

export interface IPagingProductsState {
    isLoading: boolean,
    error: IErrorState,
    products: Array<IProduct>
    paging: IPaging
}