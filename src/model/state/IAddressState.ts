import {IAddress} from "../entity/IAddress";

export interface IAddressState {
  address: IAddress
}