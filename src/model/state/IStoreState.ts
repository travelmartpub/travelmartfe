import {IErrorState} from "./IErrorState";
import {IStore} from "../entity/IStore";

export interface IStoreState {
  isLoading: boolean,
  error: IErrorState,
  store: IStore
}
