import {IStore} from "../entity/IStore";
import {IProduct} from "../entity/IProduct";

export interface ICart {
  miniCartShow: boolean,
  items: Array<ICartItem>,
  stores: Array<IStore>,
  products: Array<IProduct>
}

export interface ICartItem {
  productId: number,
  storeId: number,
  quantity: number
}
