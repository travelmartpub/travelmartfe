import {IAuthority} from "../entity/IAuthority";

export interface IAuthState {
  token: string,
  authorities: Array<IAuthority>
}