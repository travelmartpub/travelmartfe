import {IProduct} from "../entity/IProduct";
import {IErrorState} from "./IErrorState";

export interface IProductState {
  isLoading: boolean,
  error: IErrorState,
  product: IProduct
}
