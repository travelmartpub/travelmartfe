import {IProduct} from "../../entity/IProduct";
import {IErrorState} from "../IErrorState";
import {IPaging} from "../../entity/IPaging";

export interface IProductAdminState {
  isLoading: boolean,
  error: IErrorState,
  products: Array<IProduct>,
  paging: IPaging
}
