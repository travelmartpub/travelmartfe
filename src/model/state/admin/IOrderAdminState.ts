import {IErrorState} from "../IErrorState";
import {IOrder, IOrderProductDetail, IOrderStoreDetail} from "../../entity/IOrder";

export interface IOrderAdminState {
  isLoading: boolean,
  error: IErrorState,
  orders: Array<IOrder>,
  orderStoreDetails: Array<IOrderStoreDetail>
  orderProductDetails: Array<IOrderProductDetail>
}
