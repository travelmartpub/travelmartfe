export interface IErrorState {
  isError: boolean,
  codeError: number,
  msgError: string
}
