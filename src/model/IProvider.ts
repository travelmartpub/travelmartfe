export interface IProvider {
  id: number,
  name: string,
  phone: string,
  address: string,
  description: string,
  active: boolean
}