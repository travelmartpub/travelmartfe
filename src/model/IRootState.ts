export interface IRootState {
  isLoading: boolean,
  error: string
}