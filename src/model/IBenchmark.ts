import {IRootState} from "./IRootState";
import {IUser} from "./User";

export interface IExecutions extends IRootState {
  executions: Array<IExecution>
}

export interface IExecution {
  id: number,
  reqId: number,
  ccu: number,
  duration: number,
  reportPath: string,
  scriptPath: string,
  status: string,
  user: IUser
}