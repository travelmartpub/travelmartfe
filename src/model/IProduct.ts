import {IRootState} from "./IRootState";
import {IItem} from "./IItem";

export interface IProducts extends IRootState {
  products: Array<IProduct>,
  isLoadingCreate: boolean
}

export interface IProduct extends IItem {
}

export interface IProductReq {
  active: true,
  brandId: number,
  categoryId: number,
  code: string,
  cost: 0,
  description: string,
  imageIds: Array<number>,
  inventory: number,
  name: string,
  price: number,
  providerId: number,
  unitId: number,
  warrantyPeriod: number
}