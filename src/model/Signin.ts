export interface ISignIn {
  username: string,
  password: string,
  rememberMe: boolean
}