import {ICategoryProduct} from "./ICategoryProduct";
import {IImage} from "./IImage";
import {IUnit} from "./IUnit";
import {IBrand} from "./IBrand";
import {IProvider} from "./IProvider";

export interface IItem {
    id: number,
    name: string,
    active: boolean,
    code: string,
    cost: number,
    price: number,
    description: string,
    inventory: number,
    warrantyPeriod: number,
    category: ICategoryProduct,
    images: Array<IImage>,
    unit: IUnit,
    brand: IBrand,
    provider: IProvider
}