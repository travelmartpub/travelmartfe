import {IRootState} from "./IRootState";
import {IItem} from "./IItem";

export interface IServices extends IRootState {
  services: Array<IItem>,
  isLoadingCreate: boolean
}

export interface IService extends IItem {
}

export interface IServiceReq {
  "active": boolean,
  "categoryId": number,
  "cost": number,
  "description": string,
  "imageIds": Array<number>,
  "name": string,
  "price": number,
  "unitId": number,
  "warrantyPeriod": number
}