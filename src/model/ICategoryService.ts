export interface ICategoryService {
  id: number,
  name: string,
  active: boolean
}