import {IPaging} from "./entity/IPaging";

export interface IResponse<T = any> {
  error: IErrorResponse
  data: T
  paging?: IPaging
}

export interface IErrorResponse {
  code: number,
  message: string,
  details: Array<string>
}