import {IRootState} from "./IRootState";

export interface ILogging extends IRootState {
  logs: Array<string>;
}