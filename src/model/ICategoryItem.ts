import {IRootState} from './IRootState';

export interface ICategoryItems extends IRootState {
    categoryItems: Array<ICategoryItem>
}

export interface ICategoryItem {
    id: number,
    name: string,
    description: string
}