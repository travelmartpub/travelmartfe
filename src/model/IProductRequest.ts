export interface IProductRequest {
  id: number,
  storeId: number,
  name: string,
  description: string,
  price: number,
  stock: number,
  images: string
}