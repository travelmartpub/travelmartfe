export interface IAuthority {
  userId: number,
  name: string
}