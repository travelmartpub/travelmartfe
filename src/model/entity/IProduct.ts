export interface IProduct {
  id: number,
  storeId: number,
  name: string,
  description: string,
  price: number,
  activated: boolean,
  stock: number,
  sold: number,
  createdAt: string,
  images: string,
  quantity?: number
}
