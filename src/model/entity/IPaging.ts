export interface IPaging {
  limit: number,
  offset: number,
  orderBy: string,
  total: number
}