export interface IAddress {
  userId: number,
  phone: string,
  houseNumberStreet: string,
  address: string
}