export interface IOrder {
  id?: number
  userId: number,
  totalPrice: number,
  receiverName: string,
  email?: string,
  phone: string,
  houseNumberStreet: string,
  address: string,
  shipmentMethod: string,
  paymentMethod: string,
  status?: string,
  createdAt?: any,
  orderStoreDetails: Array<IOrderStoreDetail>
}

export interface IOrderStoreDetail {
  id?: number,
  orderId?: number,
  storeId: number,
  storeName?: string,
  totalPrice: number,
  status?: string,
  orderProductDetails: Array<IOrderProductDetail>
}

export interface IOrderProductDetail {
  id?: number
  orderStoreDetailId?: number,
  productId: number,
  productName?: string,
  quantity: number,
  price: number,
  totalPrice: number
}