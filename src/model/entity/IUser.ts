import {IAuthority} from "./IAuthority";

export interface IUser {
  id: number,
  userName: string,
  email: string,
  activated: boolean,
  storeId: number,
  avatar: string,
  authorities: Array<IAuthority>
}