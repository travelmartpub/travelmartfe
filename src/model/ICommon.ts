import {IRootState} from "./IRootState";

export interface ICommon extends IRootState {
  message: string;
}