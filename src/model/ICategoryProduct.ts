export interface ICategoryProduct {
  id: number,
  name: string,
  active: boolean
}