import {IRootState} from "./IRootState";
import {ItemType} from "../constant/ItemType";

export interface IOrders extends IRootState {
  orders: Array<IOrder>,
  isLoadingCreate: boolean
}

export interface IOrder {
  itemId: number,
  quantity: number
}

export interface IOrderReq {
  orderDetails: Array<IOrderDetailReq>,
  discount: number,
  description: string,
  createdOrderTime: number,
}

export interface IOrderDetailReq {
  type: ItemType,
  id: number,
  quantity: number,
  cost: number,
  price: number,
  description: string
}