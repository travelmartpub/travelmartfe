export interface IImage {
  id: number,
  title: string,
  content: string
}