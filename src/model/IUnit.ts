export interface IUnit {
  id: number,
  name: string,
  active: boolean,
}