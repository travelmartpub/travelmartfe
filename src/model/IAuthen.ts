import {IRootState} from "./IRootState";

export interface IAuth extends IRootState {
  token: string,
  authorities: Array<string>
}

export interface IToken {
  token: string;
}