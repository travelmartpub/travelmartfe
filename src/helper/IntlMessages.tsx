import React from "react";
import {FormattedMessage, injectIntl} from "react-intl";

interface IProps {
  id: string
}

const InjectMassage = (props: IProps) => {
  return (
      <FormattedMessage {...props} />
  )
};

export default injectIntl(InjectMassage, {
  withRef: false
});
