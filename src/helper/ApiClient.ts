import axios from 'axios';
import {StorageHelper} from "./Storage";

const TIMEOUT = 1 * 60 * 1000;

/**
 * Detail for custom configurations
 */
const onRequestSuccess = config => {
  let token = StorageHelper.getCookie('token');
  config.headers.Authorization = `Bearer ${token}`;
  return config;
};

const onResponseSuccess = response => response;
const onResponseError = err => {
  // const status = err.status || (err.response ? err.response.status : 0);
  // if (status === 403 || status === 401) {
  //   onUnauthenticated();
  // }
  return Promise.reject(err);
};

/**
 * Configuration with custom values
 */
const ins = axios.create();

ins.defaults.timeout = TIMEOUT;
ins.defaults.baseURL = process.env.APP_API_URL;
ins.interceptors.request.use(onRequestSuccess);
ins.interceptors.response.use(onResponseSuccess, onResponseError);

export {ins as axios};