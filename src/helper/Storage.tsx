import nextCookie from 'next-cookies';
import cookie from 'js-cookie';

const setCookie = (key: string, value: string, expires = 1) => {
  cookie.set(key, value, {expires});
};

const getCookie = (key: string) => {
  return cookie.get(key);
};

const removeCookie = (key: string) => {
  cookie.remove(key);
};

const getServerCookie = (
    ctx: { req?: { headers: { cookie?: string } } },
    key: string | number
) => {
  return nextCookie(ctx)[key];
};

const get2SideCookie = (ctx: any, key: string): any => {
  return process.browser ? getCookie(key) : getServerCookie(ctx, key);
}

const setLocalStorage = (key: string, value: string) => {
  localStorage.setItem(key, value);
}

const getLocalStorage = (key: string): any => {
  const item = localStorage.getItem(key);

  if (item) {
    return JSON.parse(item);
  }

  return null;
}

const removeLocalStorage = (key: string) => {
  localStorage.removeItem(key);
};

export const StorageHelper = {
  setCookie,
  getCookie,
  removeCookie,
  getServerCookie,
  get2SideCookie,
  setLocalStorage,
  getLocalStorage,
  removeLocalStorage
};