import {ColumnProps} from "antd/es/table";
import {IErrorState} from "../model/state/IErrorState";
import {ErrorMessage} from "../constant/ErrorMessage";
import {IErrorResponse} from "../model/IResponse";
import {ICartItem} from "../model/state/ICart";
import {IProduct} from "../model/entity/IProduct";
import {IStore} from "../model/entity/IStore";
import {IOrderProductDetail, IOrderStoreDetail} from "../model/entity/IOrder";
import {IOrderRequest} from "../model/common/IOrderRequest";
import {IUser} from "../model/entity/IUser";

const getNameOfColumn = <T>(columns: ColumnProps<T>[]): Array<string> => {
  return columns.filter((value => {
        if (value.dataIndex) {
          return value.dataIndex;
        }
      })
  ).map((value => value.dataIndex));
};

const getTitleOfColumn = <T>(columns: ColumnProps<T>[]): Array<string> => {
  return columns.filter((value => {
        if (value.title) {
          return value.title;
        }
      })
  ).map((value => value.title.toString()));
};

const getError = (error: IErrorResponse): IErrorState => {
  return {
    codeError: error.code,
    isError: true,
    msgError: ErrorMessage[error.code]
  }
}

const getProducts = (items: Array<ICartItem>, products: Array<IProduct>, storeId: number): Array<IProduct> => {
  const itemsByStore = items.filter((item) => item.storeId === storeId);

  const resultProducts = [] as IProduct[];
  products.forEach(product => {
    const item = itemsByStore.find(item => item.productId === product.id);
    if (item !== undefined) {
      resultProducts.push({...product, quantity: item.quantity})
    }
  })

  return resultProducts;
}

const countItems = (items: Array<ICartItem>): number => {
  if (items.length === 0)
    return 0;
  return items.map(items => items.quantity).reduce((a, b) => a + b);
}

const calcTotalPrice = (items: Array<ICartItem>, products: Array<IProduct>): number => {
  if (items.length === 0)
    return 0;

  let totalPrice = 0
  items.forEach(item => {
    const product = products.find(product => product.id == item.productId);

    totalPrice += item.quantity * product.price;
  })

  return totalPrice;
}

const buildOrderRequest = (userId: number, receiverName: string, email: string, phone: string, houseNumberStreet: string,
                           address: string, shipmentMethod: string, paymentMethod: string,
                           stores: Array<IStore>, items: Array<ICartItem>, products: Array<IProduct>): IOrderRequest => {

  const orderStoreDetails = [] as Array<IOrderStoreDetail>;

  let orderPrice = 0;

  stores.forEach(store => {

    let orderStorePrice = 0;

    const orderProductDetails = [] as Array<IOrderProductDetail>;

    items.filter(item => item.storeId === store.id).forEach(item => {
      const product = products.find(product => product.id === item.productId);

      const orderProductDetail: IOrderProductDetail = {
        productId: product.id,
        productName: product.name,
        price: product.price,
        quantity: item.quantity,
        totalPrice: product.price * item.quantity
      };
      orderProductDetails.push(orderProductDetail);

      orderStorePrice += orderProductDetail.totalPrice;
    })

    const orderStoreDetail: IOrderStoreDetail = {
      storeId: store.id,
      storeName: store.name,
      totalPrice: orderStorePrice,
      orderProductDetails: orderProductDetails
    }
    orderStoreDetails.push(orderStoreDetail);

    orderPrice += orderStorePrice;
  })

  return {
    address: address,
    houseNumberStreet: houseNumberStreet,
    paymentMethod: paymentMethod,
    phone: phone,
    receiverName: receiverName,
    shipmentMethod: shipmentMethod,
    userId: userId,
    totalPrice: orderPrice,
    orderStoreDetails: orderStoreDetails
  };
}

const productParams = (spId: string): { storeId: number, productId: number } => {
  const result = spId.split(".");
  return {
    storeId: Number(result[0]),
    productId: Number(result[1])
  }
}

const getAuthorities = (user: IUser): Array<string> => {
  if (user.authorities === undefined)
    return [] as string[]

  return user.authorities.map(authority => authority.name);
}

export const CommonHelper = {
  getNameOfColumn,
  getError,
  getProducts,
  countItems,
  calcTotalPrice,
  buildOrderRequest,
  productParams,
  getAuthorities,
  getTitleOfColumn
};