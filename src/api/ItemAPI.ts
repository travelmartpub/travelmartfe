import { axios } from '../helper/ApiClient';
import { IResponse } from '../model/IResponse';
import { AxiosResponse } from "axios";
import { IItem } from '../model/IItem';

const DOMAIN_URL = "items";

const execute = async(data): Promise<IResponse<string>> => {
    const resp: AxiosResponse<IResponse> = await axios.post(`${DOMAIN_URL}`, data);
    return resp.data;
}

const getItems = async() : Promise<IResponse<Array<IItem>>> => {
    const resp: AxiosResponse<IResponse> = await axios.get(`${DOMAIN_URL}`);
    return resp.data;
}

const updateItem = async(data: IItem) : Promise<IResponse<IItem>> => {
    const resp: AxiosResponse<IResponse> = await axios.put(`${DOMAIN_URL}/${data.id}`, data);
    return resp.data;
}

const deleteItem = async(id : number) : Promise<IResponse<number>> => {
    const resp: AxiosResponse<IResponse> = await axios.delete(`${DOMAIN_URL}/${id}`);
    return resp.data;
}

export const ItemAPI = {
    execute,
    getItems,
    updateItem,
    deleteItem
};
