import {IResponse} from "../model/IResponse";
import {IAddress} from "../model/entity/IAddress";
import {axios} from "../helper/ApiClient";
import {AxiosResponse} from "axios";

const ADDRESS_URL = "/api/account/address"

const getAddress = async (): Promise<IResponse<IAddress>> => {
  const resp: AxiosResponse<IResponse> = await axios.get(`${ADDRESS_URL}`)
  return resp.data;
};

const setAddress = async (address: IAddress): Promise<IResponse<IAddress>> => {
  const resp: AxiosResponse<IResponse> = await axios.post(`${ADDRESS_URL}`, address)

  return resp.data;
};

export const AddressAPI = {
  getAddress,
  setAddress
};