import {axios} from "../helper/ApiClient";
import {IResponse} from "../model/IResponse";
import {ILogging} from "../model/ILogging";

const DOMAIN_URL = "logs";

const getAllLogs = async (logName): Promise<IResponse<ILogging>> => {
  const resp = await axios.get(`${DOMAIN_URL}`, {
    params: {
      logName: logName,
      reqId: Date.now()
    }
  });

  return resp.data;
};

export const LogViewAPI = {
  getAllLogs
};
