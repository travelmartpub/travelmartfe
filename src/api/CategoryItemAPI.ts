import { axios } from '../helper/ApiClient';
import { IResponse } from '../model/IResponse';
import { AxiosResponse } from "axios"
import { ICategoryItem } from '../model/ICategoryItem';

const DOMAIN_URL = "category-item";

const createCategoryItem = async(data): Promise<IResponse<string>> => {
    const resp: AxiosResponse<IResponse> = await axios.post(`${DOMAIN_URL}`, data);
    return resp.data;
}

const getCategoryItems = async(): Promise<IResponse<Array<ICategoryItem>>> => {
    const resp: AxiosResponse<IResponse> = await axios.get(`${DOMAIN_URL}`);
    return resp.data;
}

const updateCategoryItem = async(data: ICategoryItem): Promise<IResponse<ICategoryItem>> => {
    const resp: AxiosResponse<IResponse> = await axios.put(`${DOMAIN_URL}/${data.id}`, data);
    return resp.data;
} 

const deleteCategoryItem = async(id : number) : Promise<IResponse<number>> => {
    const resp: AxiosResponse<IResponse> = await axios.delete(`${DOMAIN_URL}/${id}`);
    return resp.data;
}

export const CategoryItemAPI = {
    createCategoryItem,
    getCategoryItems,
    updateCategoryItem,
    deleteCategoryItem
};