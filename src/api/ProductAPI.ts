import {axios} from "../helper/ApiClient";
import {IResponse} from "../model/IResponse";
import {AxiosResponse} from "axios";
import {IProduct} from "../model/entity/IProduct";

const PRODUCT_ID = "/api/v1/products";

const PAGING_PRODUCTS = "api/v1/products/paging"

const STORE_PAGING_PRODUCT = "/api/v1/products/store"

const fetchProductById = async (productId: number): Promise<IResponse<IProduct>> => {
  const resp: AxiosResponse<IResponse<IProduct>> = await axios.get(`${PRODUCT_ID}/${productId}`, {});
  return resp.data;
};

const fetchPagingProducts = async (limit: number, offset: number, orderBy?: string): Promise<IResponse<Array<IProduct>>> => {
  let resp: AxiosResponse<IResponse<Array<IProduct>>>;

  if (null == orderBy) {
    resp = await axios.get(
        `${PAGING_PRODUCTS}?limit=${limit}&offset=${offset}&orderBy=${orderBy}`, {});
  } else {
    resp = await axios.get(
        `${PAGING_PRODUCTS}?limit=${limit}&offset=${offset}`, {});
  }
  return resp.data;
};

const fetchStorePagingProducts = async (storeId: number, limit: number, offset: number): Promise<IResponse<Array<IProduct>>> => {
  const resp: AxiosResponse<IResponse<Array<IProduct>>> = await axios.get(`${STORE_PAGING_PRODUCT}/${storeId}/products?limit=${limit}&offset=${offset}`, {});
  return resp.data;
};

export const ProductAPI = {
  fetchProductById,
  fetchPagingProducts,
  fetchStorePagingProducts
};