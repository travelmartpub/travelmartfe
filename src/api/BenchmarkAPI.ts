import {axios} from "../helper/ApiClient";
import {IResponse} from "../model/IResponse";
import {AxiosResponse} from "axios";
import {IExecution, IExecutions} from "../model/IBenchmark";

const DOMAIN_URL = "executions";

const execute = async (data): Promise<IResponse<string>> => {
  const resp: AxiosResponse<IResponse> = await axios.post(`${DOMAIN_URL}`, data);
  return resp.data;
};

const getExecutions = async (username: string): Promise<IResponse<Array<IExecution>>> => {
  const resp: AxiosResponse<IResponse> = await axios.get(`${DOMAIN_URL}`, {
    params: {
      user: username,
      reqId: Date.now()
    }
  });

  return resp.data;
};

export const BenchmarkAPI = {
  execute,
  getExecutions
};
