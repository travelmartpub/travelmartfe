import {axios} from "../helper/ApiClient";
import {IResponse} from "../model/IResponse";
import {AxiosResponse} from "axios";
import {IService, IServiceReq} from "../model/IService";

const DOMAIN_URL = "items";
const ITEM_TYPE = "service";

const createService = async (data: IServiceReq): Promise<IResponse<IService>> => {
  const resp: AxiosResponse<IResponse<IService>> = await axios.post(`${DOMAIN_URL}\\${ITEM_TYPE}`, data);
  return resp.data;
};

const getAllServices = async (): Promise<IResponse<Array<IService>>> => {
  const resp: AxiosResponse<IResponse<Array<IService>>> = await axios.get(`${DOMAIN_URL}\\${ITEM_TYPE}`, {});
  return resp.data;
};

export const ServiceAPI = {
  createService,
  getAllServices
};