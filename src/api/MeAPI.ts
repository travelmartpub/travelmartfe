import axios, {AxiosResponse} from "axios";
import {IResponse} from "../model/IResponse";
import {IUser} from "../model/entity/IUser";

const DOMAIN_URL = "/api/account";

const getUser = async (token): Promise<IResponse<IUser>> => {
  const resp: AxiosResponse<IResponse> = await axios.get(`${DOMAIN_URL}`,
      {
        baseURL: process.env.APP_API_URL_SERVER,
        headers: {
          authorization: "Bearer " + token
        }
      });

  return resp.data;
};

export const MeAPI = {
  getUser
};
