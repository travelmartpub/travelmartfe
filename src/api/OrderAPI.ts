import {axios} from "../helper/ApiClient";
import {IResponse} from "../model/IResponse";
import {AxiosResponse} from "axios";
import {IOrderRequest} from "../model/common/IOrderRequest";
import {IOrderResponse} from "../model/common/IOrderResponse";
import {IOrder, IOrderProductDetail, IOrderStoreDetail} from "../model/entity/IOrder";

const DOMAIN_URL = "/api/v1/orders/create";
const GET_ORDER_STORE_DETAILS = "/api/v1/stores/orders"
const GET_ORDER = "/api/v1/orders"
const GET_ORDER_STORE_DETAIL = "/api/v1/orders/store"
const GET_ORDER_PRODUCT_DETAIL = "/api/v1/orders/store/product"
const GET_USER_ORDERS = "/api/v1/orders/user/orders"
const GET_STORE_ORDERS = "/api/v1/orders/store/orders"

const createOrder = async (data: IOrderRequest): Promise<IResponse<IOrderResponse>> => {
  const resp: AxiosResponse<IResponse<IOrderResponse>> = await axios.post(`${DOMAIN_URL}`, data);
  return resp.data;
};

const fetchOrderStoreDetails = async (): Promise<IResponse<Array<IOrderStoreDetail>>> => {
  const resp: AxiosResponse<IResponse<Array<IOrderStoreDetail>>> = await axios.get(
      `${GET_ORDER_STORE_DETAILS}`, {});
  return resp.data;
};

const fetchOrder = async (orderId: string): Promise<IResponse<IOrder>> => {
  const resp: AxiosResponse<IResponse<IOrder>> = await axios.get(
      `${GET_ORDER}/${orderId}`, {});
  return resp.data;
}

const fetchOrders = async (limit: number, offset: number): Promise<IResponse<Array<IOrder>>> => {
  const resp: AxiosResponse<IResponse<Array<IOrder>>> = await axios.get(
      `${GET_USER_ORDERS}?limit=${limit}&offset=${offset}`, {});
  return resp.data;
}

const fetchOrdersByStore = async (limit: number, offset: number): Promise<IResponse<Array<IOrder>>> => {
  const resp: AxiosResponse<IResponse<Array<IOrder>>> = await axios.get(
      `${GET_STORE_ORDERS}?limit=${limit}&offset=${offset}`, {});
  return resp.data;
}

const fetchOrderStoreDetail = async (orderStoreDetailId: string): Promise<IResponse<IOrderStoreDetail>> => {
  const resp: AxiosResponse<IResponse<IOrderStoreDetail>> = await axios.get(
      `${GET_ORDER_STORE_DETAIL}/${orderStoreDetailId}`, {});
  return resp.data;
}

const fetchOrderProductDetail = async (orderProductDetailId: string): Promise<IResponse<IOrderProductDetail>> => {
  const resp: AxiosResponse<IResponse<IOrderProductDetail>> = await axios.get(
      `${GET_ORDER_PRODUCT_DETAIL}/${orderProductDetailId}`, {});
  return resp.data;
}

const fetchOrderProductDetails = async (orderStoreDetailId: string): Promise<IResponse<Array<IOrderProductDetail>>> => {
  const resp: AxiosResponse<IResponse<Array<IOrderProductDetail>>> = await axios.get(
      `${GET_ORDER_STORE_DETAIL}/${orderStoreDetailId}/products`, {});
  return resp.data;
}

export const OrderAPI = {
  createOrder,
  fetchOrderStoreDetails,
  fetchOrder,
  fetchOrders,
  fetchOrderStoreDetail,
  fetchOrderProductDetail,
  fetchOrderProductDetails,
  fetchOrdersByStore
};