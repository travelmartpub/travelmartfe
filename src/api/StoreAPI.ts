import {axios} from "../helper/ApiClient";
import {IResponse} from "../model/IResponse";
import {AxiosResponse} from "axios";
import {IStore} from "../model/entity/IStore";
import {IProduct} from "../model/entity/IProduct";
import {IProductRequest} from "../model/IProductRequest";

const PRODUCT_ID = "/api/v1/stores";
const STORE_PAGING_PRODUCT = "/api/v1/stores/products";
const CREATE_PRODUCT = "/api/v1/stores/product/create";
const UPDATE_PRODUCT = "/api/v1/stores/product/update";
const DEACTIVATE_PRODUCT = "/api/v1/stores/product/deactivate"
const HANDLE_ORDER = "/api/v1/stores/order"
const CREATE_USER_STORE = "/api/v1/stores/user/create"

const fetchStoreById = async (storeId: number): Promise<IResponse<IStore>> => {
  const resp: AxiosResponse<IResponse<IStore>> = await axios.get(`${PRODUCT_ID}/${storeId}`, {});
  return resp.data;
};

const fetchStoreProduct = async (limit: number, offset: number, orderBy?: string): Promise<IResponse<Array<IProduct>>> => {
  let resp: AxiosResponse<IResponse<Array<IProduct>>>;

  if (null == orderBy) {
    resp = await axios.get(
        `${STORE_PAGING_PRODUCT}?limit=${limit}&offset=${offset}&orderBy=${orderBy}`, {});
  } else {
    resp = await axios.get(
        `${STORE_PAGING_PRODUCT}?limit=${limit}&offset=${offset}`, {});
  }
  return resp.data;
}

const createProduct = async (product: IProductRequest): Promise<IResponse<IProduct>> => {
  const resp = await axios.post(`${CREATE_PRODUCT}`, product);

  return resp.data;
}

const createUserStore = async (name: string): Promise<IResponse<IStore>> => {
  const resp = await axios.post(`${CREATE_USER_STORE}`, new Object({name})
  );

  return resp.data;
}

const updateProduct = async (product: IProductRequest): Promise<IResponse<IProduct>> => {
  const resp = await axios.post(`${UPDATE_PRODUCT}`, product);

  return resp.data;
}

const deactivateProduct = async (product: string, activate: boolean): Promise<IResponse<IProduct>> => {
  const resp = await axios.post(`${DEACTIVATE_PRODUCT}/${product}?activate=${activate}`);

  return resp.data;
}

const acceptOrder = async (orderStoreId: number): Promise<IResponse<number>> => {
  const resp = await axios.post(`${HANDLE_ORDER}/${orderStoreId}/accept`);

  return resp.data;
}

const declineOrder = async (orderStoreId: number): Promise<IResponse<number>> => {
  const resp = await axios.post(`${HANDLE_ORDER}/${orderStoreId}/decline`);

  return resp.data;
}

const shipOrder = async (orderStoreId: number): Promise<IResponse<number>> => {
  const resp = await axios.post(`${HANDLE_ORDER}/${orderStoreId}/ship`);

  return resp.data;
}

export const StoreAPI = {
  fetchStoreById,
  fetchStoreProduct,
  createProduct,
  updateProduct,
  deactivateProduct,
  acceptOrder,
  declineOrder,
  createUserStore,
  shipOrder
};