import {ISignIn} from "../model/Signin";
import {axios} from "../helper/ApiClient";
import {IResponse} from "../model/IResponse";
import {IToken} from "../model/IAuthen";
import {AxiosResponse} from "axios";
import {ISignUp} from "../model/Signup";
import {IUser} from "../model/entity/IUser";

const DOMAIN_SIGN_IN_URL = "/auth/login";
const DOMAIN_SIGN_UP_URL = "/auth/register";

const signIn = async (data: ISignIn): Promise<IResponse<IToken>> => {
  const resp: AxiosResponse<IResponse> = await axios.post(`${DOMAIN_SIGN_IN_URL}`, data);
  return resp.data;
};

const signUp = async (data: ISignUp): Promise<IResponse<IUser>> => {
  const resp: AxiosResponse<IResponse> = await axios.post(`${DOMAIN_SIGN_UP_URL}`, data);
  return resp.data;
};

export const AuthAPI = {
  signIn,
  signUp
};
