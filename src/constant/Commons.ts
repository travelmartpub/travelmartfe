import {NextRouter} from "next/router";

export const SigninPage = (router: NextRouter) => {
  return {
    pathname: "/signin",
    query: {from: router.pathname}
  }
}