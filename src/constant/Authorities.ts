const ROLE_USER = 'ROLE_USER';
const ROLE_STORE = 'ROLE_STORE';
const ROLE_ADMIN = 'ROLE_ADMIN';

export const Authorities = {
  ROLE_USER,
  ROLE_STORE,
  ROLE_ADMIN,
};