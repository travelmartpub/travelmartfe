import React, {useEffect, useState} from "react";
import {NextPage} from "next";
import {Col, Pagination, Row, Spin} from "antd";
import {useDispatch} from "react-redux";
import MainLayout from "../layout/travelmart/main/MainLayout";
import ProductItemSimple from "../component/ECommerce/ProductItemSimple";
import {IPagingProductsState} from "../model/state/IPagingProducts";
import {PagingProductsActions} from "../redux/slice/pagingProductSlice";
import {useRouter} from "next/router";

const initialState: IPagingProductsState = {
  isLoading: true,
  error: {
    isError: false,
    codeError: -1,
    msgError: ""
  },
  products: [],
  paging: null
};

// TODO: Change Sidebar to Topbar
const Home: NextPage = () => {

  const dispatch = useDispatch();
  const router = useRouter()

  const [pagingProducts, setPagingProducts] = useState<IPagingProductsState>(initialState);
  const limit = 20;
  const {page} = router.query

  useEffect(() => {
    dispatch(PagingProductsActions.fetchPagingProductsLocalState(limit, (page != null ? Number(page) - 1 : 0) * limit, (products, paging) => {
      setPagingProducts({isLoading: false, error: null, products: products, paging: paging});
    }));

  }, [page])

  const changePage = async (page) => {
    router.push(process.env.APP_SUB_DOMAIN + `?page=${page}`).then(() => {
      setPagingProducts(initialState);
    })
  }

  return (
      <MainLayout>
        {pagingProducts.isLoading ?
            <Spin tip="Loading..." style={{
              margin: '0',
              position: 'absolute',
              top: '50%',
              left: '50%',
              transform: 'translate(-50%, -50%)'
            }}/>
            :
            <>
              <Row>
                {pagingProducts.products.map((product, index) => (
                    <Col key={index} xl={6} md={8} sm={12} xs={24}>
                      <ProductItemSimple key={index} grid product={product}/>
                    </Col>
                ))}
              </Row>
              <Row style={{textAlign: 'center', marginBottom: '10px', bottom: '10px'}}>
                <div className="gx-layout-footer-content" style={{alignSelf: "center", alignContent: "center"}}>
                  <Pagination current={page != null ? Number(page) : 1} pageSize={limit} onChange={changePage}
                              total={pagingProducts.paging.total}/>
                </div>
              </Row></>}
      </MainLayout>
  )
}
export default Home