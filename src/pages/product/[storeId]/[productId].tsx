import React, {useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {NextPage} from "next";
import {Button, Card, Col, Collapse, InputNumber, Modal, Row} from "antd";
import MainLayout from "../../../layout/travelmart/main/MainLayout";
import NumberFormat from "react-number-format";
import {AppState} from "../../../redux";
import {CartActions} from "../../../redux/slice/cartSlice";
import {ICartItem} from "../../../model/state/ICart";
import parse from 'html-react-parser'
import {useRouter} from 'next/router'
import {ProductActions} from "../../../redux/slice/productSlice";
import Error404 from "../../../component/ErrorPages/Error404";
import {StoreActions} from "../../../redux/slice/storeSlice";
import {IProductState} from "../../../model/state/IProductState";
import {IStoreState} from "../../../model/state/IStoreState";
import {IPagingProductsState} from "../../../model/state/IPagingProducts";
import {PagingProductsActions} from "../../../redux/slice/pagingProductSlice";
import ProductItemSimple from "../../../component/ECommerce/ProductItemSimple";
import Link from "next/link";

const Panel = Collapse.Panel;

const ship = `
  Dịch vụ giao hàng bởi Travel Mart hoặc lấy hàng trực tiếp tại cửa hàng`;

const warranty = `
  Chấp nhận thanh toán qua MOMO, ZALO PAY hoặc COD`;

const customPanelStyle = {
  borderRadius: 4,
  border: 0,
  overflow: 'hidden',
};

const Index: NextPage = () => {
  const dispatch = useDispatch();
  const router = useRouter();
  const {storeId, productId} = router.query;

  const [quantity, setQuantity] = useState<number>(1);
  const productState: IProductState = useSelector((state: AppState) => state.product);
  const storeState: IStoreState = useSelector((state: AppState) => state.store);
  const relevantProductsState: IPagingProductsState = useSelector((state: AppState) => state.pagingProducts)

  const {product} = productState;
  const {store} = storeState;

  const {products} = relevantProductsState;


  /**
   * Hook useEffect call at top-level, before return
   **/
  useEffect(() => {
    dispatch(ProductActions.fetchProductById(productId));
    dispatch(StoreActions.fetchStoreById(storeId));

    dispatch(PagingProductsActions.fetchPagingProducts(20, 0));
  }, [storeId, productId])

  /**
   * This is data page, must validate data valid
   **/
  if (productState.error.isError || storeState.error.isError)
    return <Error404/>;
  // if (productState.error.isError)
  //   return <Error404/>;

  const btnAddCart = async () => {

    if (quantity > product.stock) {
      Modal.error({
        title: 'Không còn đủ số lượng hàng trong kho!'
      });
      return;
    }

    const item: ICartItem = {
      productId: product.id,
      storeId: store.id,
      quantity: quantity
    }

    await dispatch(CartActions.addToCart(item, store, product));
  };

  const updateQuantity = (value: number) => {

    console.log("value: ", value);
    console.log("stock: ", product.stock);

    if (value > product.stock) {
      console.log("true ");
      setQuantity(product.stock);
    } else {
      setQuantity(value);
    }
  }

  return (
      (productState.isLoading || storeState.isLoading) ? (<></>) :

          <MainLayout>
            <Row className="gx-mb-5">
              {/* product image */}
              <Col xl={12} lg={24} md={12} sm={24} xs={24}>
                <Card className="gx-card align-center">
                  {/*<h3>{JSON.stringify(cart, null, 2)}</h3>*/}
                  {/*<h3>{productId}</h3>*/}
                  <img className="gx-w-75"
                       src={product.images != undefined ? product.images.split(',')[0] : "https://product.hstatic.net/200000360759/product/2290201002073_b1edb636ac6f41dca717e7e6cb8d89bb_master.jpg"}
                       alt='Neature'/>
                </Card>
              </Col>

              {/* product detail */}
              <Col xl={12} lg={24} md={12} sm={24} xs={24}>
                <Row>

                  <Col lg={24} md={24} sm={24} xs={24}>
                    <Card className="gx-card">
                      <div className="ant-row-flex gx-mb-1">
                        <h2 className="gx-mr-2 gx-mb-0 gx-fs-xxxl gx-font-weight-medium">{product.name}</h2>
                        {/*<h4 className="gx-pt-2 gx-chart-up">32% <i*/}
                        {/*    className="icon icon-menu-down gx-fs-sm"/></h4>*/}
                      </div>

                      <p className="gx-text-grey gx-font-weight-light gx-mb-4">
                        Mã sản phẩm: <span
                          className="gx-text-primary gx-font-weight-normal gx-fs-sm">{product.id}</span> |
                        Cửa hàng: <span
                          className="gx-text-primary gx-font-weight-normal gx-fs-sm">
                          <Link href={process.env.APP_SUB_DOMAIN + `/stores/${storeId}`}>
                            {store.name}
                          </Link>
                        </span> |
                        Số lượng trong kho: <span
                          className="gx-text-primary gx-font-weight-normal gx-fs-sm">{product.stock}</span>
                      </p>

                      <h1 className="gx-mb-4 gx-text-primary gx-font-weight-medium gx-fs-xxxl">
                        <NumberFormat
                            value={product.price}
                            displayType={"text"}
                            thousandSeparator={true}
                            suffix={""}
                        />₫
                      </h1>

                      <div className="gx-product-description-panel gx-mb-4">
                        <div className="gx-timeline-panel-header">
                          <div className="gx-timeline-heading">
                            <h4 className="gx-timeline-title">Đặc điểm nổi bật</h4>
                          </div>
                        </div>
                        <div className="gx-timeline-body">
                          {parse(product.description)}
                        </div>
                      </div>

                      <Row>
                        <Col lg={5} md={5} sm={5} xs={5}>
                          <InputNumber size="large" className="gx-mr-2 gx-mb-1" min={1} max={50}
                                       defaultValue={1}
                                       value={quantity}
                                       onChange={updateQuantity}/>
                        </Col>
                        <Col lg={19} md={19} sm={19} xs={19} style={{paddingLeft: '35px'}}>
                          <Button className="gx-btn-secondary gx-mb-3 ant-btn-lg ant-btn-block"
                                  onClick={btnAddCart}>
                            Thêm vào giỏ</Button>
                        </Col>
                      </Row>

                    </Card>
                  </Col>

                </Row>

                <Row>
                  <Col lg={24} md={24} sm={24} xs={24}>
                    <Card className="gx-card" title="Thông tin sản phẩm">
                      <Collapse className="gx-collapse-custom" bordered={false}
                          // defaultActiveKey={['1']}
                      >
                        <Panel header="Dịch vụ giao hàng" key="1" style={customPanelStyle}>
                          <p>{ship}</p>
                        </Panel>
                        <Panel header="Phương thức thanh toán" key="2" style={customPanelStyle}>
                          <p>{warranty}</p>
                        </Panel>
                      </Collapse>
                    </Card>
                  </Col>
                </Row>

              </Col>
            </Row>

            <Row className="gx-mb-5">
              <Col lg={24} md={24} sm={24} xs={24}>
                <h2
                    className="gx-text-uppercase gx-mb-5 align-center gx-text-black gx-font-weight-bold gx-fnd-title gx-border-top gx-pt-xxl-5">
                  Sản phẩm liên quan</h2>

                <Row type="flex" gutter={16}>
                  {products.slice(0, 5).map((product, index) => (
                      <Col key={index} className="gx-w-20">
                        <ProductItemSimple key={index} grid product={product}/>
                      </Col>
                  ))}
                </Row>
              </Col>
            </Row>

          </MainLayout>
  )
}
export default Index