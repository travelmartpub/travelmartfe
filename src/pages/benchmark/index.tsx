import {NextPage} from "next"
import {useDispatch, useSelector} from "react-redux";
import {Button, Card, Col, Icon, Modal, Popover, Row, Spin, Table} from "antd";
import MainLayout from "../../layout/main/MainLayout";
import InfoView from "../../component/InfoView";
import {AppState} from "../../redux";
import React, {useEffect, useState} from "react";
import {benchmarkActions} from "../../redux/slice/benchmarkSlice";
import Cascader from "../../component/DataEntry/Cascader";
import Slider from "../../component/DataEntry/Slider";
import {CascaderOptionType} from "antd/lib/cascader";
import _ from 'lodash';
import {Authorities} from "../../constant/Authorities";
import Error403 from "../../component/ErrorPages/Error403";
import {
  marksCCU,
  marksDuration,
  optionsScripts,
  moreExecutionsCard,
  moreFieldsCard
} from "../../component/CustomForPage/BenchmarkComponents";
import {ColumnProps} from "antd/es/table";
import {IExecution} from "../../model/IBenchmark";
import Link from "next/link";
import {
  REACT_APP_REPORT_STATIC_SERVER,
  REACT_APP_REPORT_STATIC_SERVER_SB
} from "../../helper/config";
import {loggingActions} from "../../redux/slice/loggingSlice";
import {isEmpty} from "lodash";

interface IProps {
}

const Index: NextPage<IProps> = () => {
  const dispatch = useDispatch();

  const {executions, isLoading} = useSelector((state: AppState) => state.benchmark);
  const {authorities}: { authorities: Array<string> } = useSelector((state: AppState) => state.auth);
  const {logs, isLoading: isLogLoading} = useSelector((state: AppState) => state.logging);

  const [ccu, setCCU] = useState<number>(1);
  const [duration, setDuration] = useState<number>(30);
  const [script, setScript] = useState<string[]>([]);
  const [isVisible, setIsVisible] = useState(false);
  const [executionId, setExecutionId] = useState('');

  /**
   * This is private page, must validate role before access
   **/
  const privilege: boolean = _.includes(authorities, Authorities.ROLE_USER);

  const fetchExecutions = () => {
    dispatch(benchmarkActions.getExecutions('admin'));
  };

  /**
   * Hook useEffect call at top-level, before return
   **/
  useEffect(() => {
    if (privilege) {

      fetchExecutions();
    }
  }, []);

  /**
   * This is private page, must validate role before access
   **/
  if (!privilege)
    return <Error403/>;

  /**
   * Business logic code
   **/
  const onClickExecute = async () => {
    console.log("script, ccu, duration", script, ccu, duration);
    await dispatch(benchmarkActions.execute(script, ccu, duration));
    fetchExecutions();
  };

  const onClickViewLog = async (executionId) => {
    // Get log view from server
    await setExecutionId(executionId);
    await dispatch(loggingActions.getLogs("mylog.fluentd.log.GET_BALANCE_" + executionId + ".jmeter.log"));
    await setIsVisible(true);
  };

  /**
   *  Column structure for table
   */
  const columns: ColumnProps<IExecution>[] = [
    {
      title: 'ID',
      dataIndex: 'id',
      key: 'id',
      align: 'left'
    },
    {
      title: 'UserName',
      dataIndex: 'user',
      key: 'user',
      align: 'left',
      render: text => <span className="gx-link">{text.login}</span>
    },
    {
      title: 'Script',
      dataIndex: 'scriptPath',
      key: 'scriptPath',
      align: 'left',
      render: text => <span className="gx-link">{text}</span>
    },
    {
      title: 'CCU',
      dataIndex: 'ccu',
      key: 'ccu',
      align: 'left'
    },
    {
      title: 'Duration',
      dataIndex: 'duration',
      key: 'duration',
      align: 'left'
    },
    {
      title: 'Status',
      dataIndex: 'status',
      key: 'status',
    },
    {
      title: 'Report',
      key: 'reportPath',
      dataIndex: 'reportPath',
      align: 'left',
      render: (text, record) => (
          <Link href={{pathname: `${REACT_APP_REPORT_STATIC_SERVER_SB}/${text}`}}>
            <a>
              <Icon type="link" style={{fontSize: '23px'}}/>
            </a>
          </Link>
      ),
    },
    {
      title: 'Action',
      key: 'action',
      dataIndex: 'id',
      align: 'left',
      render: (text, record) => (
          <Button type="primary" size="small"
                  onClick={async () => {
                    await onClickViewLog(text)
                  }}>View Log</Button>
      )
    }
  ];

  return (
      <MainLayout>
        <Card className="gx-card" title="Running Field" extra={moreFieldsCard()}>
          <Row>
            {/*================================*/}
            {/*====== SELECT TEST SCRIPT ======*/}
            {/*================================*/}
            <Col lg={9} md={12} sm={24} xs={24}>

              <Row type="flex" justify="start" align={"middle"}>
                <Col lg={7} md={24} sm={24} xs={24}>
                  <br/>
                  Choose script:
                </Col>
                <Col lg={17} md={24} sm={24} xs={24}>
                  <br/>
                  <Cascader
                      onChange={(value: string[], selectedOptions?: CascaderOptionType[]) => {
                        setScript(value);
                      }}
                      options={optionsScripts}/>
                </Col>
              </Row>

              <br/>

              <Row type="flex" justify="start" align={"middle"}>
                <Col lg={24} md={24} sm={24} xs={24}>
                  <div>
                    <i className="icon icon-affix"/>{' '}
                    Lastest: {' '}
                    {/*<span> Pin: </span>*/}
                    <a className="page-benchmark">wwomens_3_mix_langpage.jmx</a>, {' '}
                    <a className="page-benchmark">wwomens_7_referral</a>
                  </div>
                </Col>
              </Row>
            </Col>

            {/*================================*/}
            {/*===== SELECT CONFIGURATION =====*/}
            {/*================================*/}
            <Col lg={12} md={12} sm={24} xs={24}>
              <Row type="flex" justify="start" align={"middle"}>
                <Col lg={6} md={24} sm={24} xs={24}>
                  Concurrent Users:
                </Col>
                <Col lg={18} md={24} sm={24} xs={24}>
                  <Slider marks={marksCCU} inputValue={ccu} max={125} min={1}
                          onChange={(value) => {
                            setCCU(value)
                          }}/>
                </Col>
              </Row>
              <Row type="flex" justify="start" align={"middle"}>
                <Col lg={6} md={24} sm={24} xs={24}>
                  Duration:
                </Col>
                <Col lg={18} md={24} sm={24} xs={24}>
                  <Slider marks={marksDuration} inputValue={duration} max={900} min={30}
                          onChange={(value) => {
                            setDuration(value)
                          }}/>
                </Col>
              </Row>
              <Row>
                <Col lg={24} md={24} sm={24} xs={24}>

                </Col>
              </Row>

            </Col>

            {/*================================*/}
            {/*====== BUTTON EXECUTE TEST =====*/}
            {/*================================*/}
            <Col lg={2} md={12} sm={24} xs={24}>
              <br/>
              <Button onClick={onClickExecute} type="primary">Execute Test</Button>
            </Col>

          </Row>
        </Card>


        {/*================================*/}
        {/*=========== TABLE ==============*/}
        {/*================================*/}
        <Card
            className="gx-card"
            title="Executions"
            extra={moreExecutionsCard(() => {
              fetchExecutions();
            })}
        >
          <Row>
            <Col span={24}>

              <Table className="gx-table-responsive"
                     loading={isLoading}
                     columns={columns}
                     dataSource={executions}
                     rowKey={record => String(record.id)}
              />
            </Col>
          </Row>
        </Card>

        <InfoView/>

        {/*================================*/}
        {/*========= SHOW LOG VIEW ========*/}
        {/*================================*/}
        <Modal
            title={`Log View #${executionId}`}
            visible={isVisible}
            width="60%"
            onOk={() => {
              setIsVisible(false)
            }}
            onCancel={() => {
              setIsVisible(false)
            }}
            footer={[
              <Button key="back"
                      loading={isLogLoading}
                      onClick={async () => {
                        await onClickViewLog(executionId)
                      }}>
                Reload
              </Button>,
              <Button key="submit"
                      type="primary"
                      onClick={() => {
                        setIsVisible(false)
                      }}>
                Close
              </Button>,
            ]}
        >
          <Spin tip="Loading..." spinning={isLogLoading}>

            {
              isEmpty(logs) ?
                  (<p>No log content.</p>) :
                  logs.map((log, i) => {
                    return (<p key={i}>{log.message}</p>)
                  })
            }

          </Spin>
        </Modal>

      </MainLayout>
  );
};

export default Index;