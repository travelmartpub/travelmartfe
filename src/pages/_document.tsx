import Document, {Html, Head, Main, NextScript, DocumentContext} from 'next/document'

class MyDocument extends Document {
  static async getInitialProps(ctx: DocumentContext) {
    const initialProps = await Document.getInitialProps(ctx);

    return initialProps
  }

  render() {
    return (
        <Html>
          <Head>
            <link href={process.env.APP_SUB_DOMAIN + "/favicon.ico"} rel="shortcut icon"/>

            <link href={process.env.APP_SUB_DOMAIN + "/assets/vendors/noir-pro/styles.css"}
                  rel="stylesheet"/>
            <link href={process.env.APP_SUB_DOMAIN + "/assets/vendors/gaxon/styles.css"}
                  rel="stylesheet"/>
            <link href={process.env.APP_SUB_DOMAIN + "/assets/vendors/flag/sprite-flags-24x24.css"}
                  rel="stylesheet"/>
            <link
                href={process.env.APP_SUB_DOMAIN + "/assets/vendors/react-notification/react-notifications.css"}
                rel="stylesheet"/>
            <link href={process.env.APP_SUB_DOMAIN + "/assets/vendors/loader/loader.css"}
                  rel="stylesheet"/>
          </Head>

          <body>
          <Main/>
          <NextScript/>
          </body>
        </Html>
    )
  }
}

export default MyDocument