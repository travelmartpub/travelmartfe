import {NextPage} from "next"
import {useSelector} from "react-redux";
import {AppState} from "../../redux";
import React, {useEffect} from "react";
import _ from 'lodash';
import {Authorities} from "../../constant/Authorities";
import {useRouter} from "next/router";
import {IUser} from "../../model/entity/IUser";
import {CommonHelper} from "../../helper/CommonHelper";
import Error403 from "../../component/ErrorPages/Error403";

interface IProps {
}

const Index: NextPage<IProps> = () => {
  const router = useRouter();

  const {user}: { user: IUser } = useSelector((state: AppState) => state.me);
  const authorities = CommonHelper.getAuthorities(user);

  /**
   * This is private page, must validate role before access
   **/
  const privilege: boolean = _.includes(authorities, Authorities.ROLE_STORE);

  /**
   * Hook useEffect call at top-level, before return
   **/
  useEffect(() => {
    if (!privilege) {
      return
    }

    // Direct to default page
    router.push("/store/product")
  }, []);

  /**
   * This is private page, must validate role before access
   **/
  if (!privilege)
    return <Error403></Error403>;

  /**
   * Business logic code
   **/
  return (
      <></>
  );
};

export default Index;
