import {NextPage} from "next"
import {useDispatch, useSelector} from "react-redux";
import MainLayout from "../../../layout/main/MainLayout";
import {AppState} from "../../../redux";
import React, {useEffect, useState} from "react";
import _ from 'lodash';
import {Authorities} from "../../../constant/Authorities";
import {Breadcrumb, Button, Card, Dropdown, Menu, Modal} from "antd";
import ModalFormProduct from "../../../component/CustomForPage/product/ModalFormProduct";
import TableOption from "../../../component/Table/TableOption";
import {ColumnProductSimple} from "../../../component/CustomForPage/product/ColumnProductSimple";
import {CommonHelper} from "../../../helper/CommonHelper";
import {IUser} from "../../../model/entity/IUser";
import {useRouter} from "next/router";
import {ProductAdminActions} from "../../../redux/slice/admin/productAdminSlice";
import {ColumnProps} from "antd/es/table";
import {IProduct} from "../../../model/entity/IProduct";
import NumberFormat from "react-number-format";
import {ProductActions} from "../../../redux/slice/productSlice";
import Error403 from "../../../component/ErrorPages/Error403";

interface IProps {
}

const initPagination = {
  current: 1,
  pageSize: 10,
  total: 10,
  onChange: null
}

const initProducts = [{
  activated: false, createdAt: "", quantity: 0, sold: 0,
  description: "", id: 0, images: "", name: "", price: 0, stock: 0, storeId: 0
}]

const initProduct = {
  activated: false, createdAt: "", quantity: 0, sold: 0,
  description: "", id: 0, images: "", name: "", price: 0, stock: 0, storeId: 0
}

const Index: NextPage<IProps> = () => {
  const dispatch = useDispatch();
  const router = useRouter();

  const {user}: { user: IUser } = useSelector((state: AppState) => state.me);
  const authorities = CommonHelper.getAuthorities(user);
  const [showModal, setShowModal] = useState<boolean>(false);
  const [product, setProduct] = useState<IProduct>(initProduct);

  const [products, setProducts] = useState<Array<IProduct>>(initProducts);
  const [pagination, setPagination] = useState(initPagination);
  const [loading, setLoading] = useState(true);

  const limit = 10;
  const {page} = router.query

  /**
   * This is private page, must validate role before access
   **/
  const privilege: boolean = _.includes(authorities, Authorities.ROLE_STORE);

  const fetchProductsByStoreId = () => {
    setLoading(true);
    dispatch(ProductAdminActions.fetchProductsByStoreIdLocalState(user.storeId, limit, (page != null ? Number(page) - 1 : 0) * limit, (products, paging) => {
      setProducts(products);
      setPagination({
        current: page != null ? Number(page) : 1,
        pageSize: limit,
        total: paging.total,
        onChange: changePage
      });
      setLoading(false);
    }));
  }
  /**
   * Hook useEffect call at top-level, before return
   **/
  useEffect(() => {
    if (!privilege) {
      return
    }

    fetchProductsByStoreId();
  }, [page]);

  /**
   * This is private page, must validate role before access
   **/
  if (!privilege)
    return <Error403></Error403>;

  const changePage = (page) => {
    router.push(process.env.APP_SUB_DOMAIN + `/store/product?page=${page}`).then(() => {
      setProducts(initProducts);
      setPagination(initPagination);
      setLoading(true);
    })
  }

  const defaultCheckedList = CommonHelper.getNameOfColumn(ColumnProductSimple.columns);

  const handleOk = async () => {
    setShowModal(false);
    await setProduct({
      activated: false, createdAt: "", quantity: 0, sold: 0,
      description: "", id: 0, images: "", name: "", price: 0, stock: 0, storeId: 0
    })
    fetchProductsByStoreId();
  };

  const handleCancel = async () => {
    setShowModal(false);
    await setProduct({
      activated: false, createdAt: "", quantity: 0, sold: 0,
      description: "", id: 0, images: "", name: "", price: 0, stock: 0, storeId: 0
    })
    fetchProductsByStoreId();
  };

  const insertBox = () => {
    return (
        <Button onClick={() => {
          setShowModal(true)
        }}>
          Thêm sản phẩm
        </Button>
    );
  };


  const handleEdit = (record) => {
    setProduct(record);
    setShowModal(true);
  }

  const handleActive = async (record) => {
    await dispatch(ProductActions.setActivateProduct(record.id, !record.activated, () => {
      Modal.success({
        title: 'Cập nhật thành công!',
      })
      fetchProductsByStoreId();
    }, () => {
      Modal.warning({
        title: 'Cập nhật không thành công!'
      })
      fetchProductsByStoreId();
    }));
  }

  const menu = (record) => (
      <Menu>
        <Menu.Item key="0">
          <a rel="noopener noreferrer" href="#" onClick={() => {
            handleEdit(record);
          }}>
            Edit
          </a>
        </Menu.Item>

        <Menu.Item key="1">
          <a rel="noopener noreferrer" href="#" onClick={() => {
            handleActive(record);
          }}>{record.activated == true ? <p>Ẩn sản phẩm</p> : <p>Hiện sản phẩm</p>}</a>
        </Menu.Item>
      </Menu>
  );

  /**
   *  Column structure for table
   */

  const columns: ColumnProps<IProduct>[] = [
    {
      dataIndex: 'images',
      key: 'images',
      align: 'left',
      width: 90,
      fixed: 'left',
      render: (text, record) => {
        return <div>
          <img className="gx-rounded-circle gx-size-50" src={record.images.split(",")[0]}
               alt={record.name}/>
        </div>
      },
    },
    {
      title: 'ID',
      dataIndex: 'id',
      key: 'id',
      align: 'left',
      width: 130,
      sorter: (a, b) => a.id - b.id,
    },
    {
      title: 'Tên',
      dataIndex: 'name',
      key: 'name',
      align: 'left',
      width: 250,
      ellipsis: true,
      sorter: (a, b) => a.name.length - b.name.length
    },
    {
      title: 'Đơn giá',
      dataIndex: 'price',
      key: 'price',
      align: 'left',
      width: 130,
      sorter: (a, b) => a.price - b.price,
      render: (text, record) => (
          <>
            <NumberFormat
                value={record.price}
                displayType={"text"}
                thousandSeparator={true}
                suffix={""}
            /> ₫
          </>
      )
    },
    {
      title: 'Số lượng',
      dataIndex: 'stock',
      key: 'stock',
      width: 130,
      sorter: (a, b) => a.stock - b.stock
    },
    {
      title: 'Mô tả',
      dataIndex: 'description',
      key: 'description',
      align: 'left',
      ellipsis: true,
      width: 400
    },
    {
      title: 'Trạng thái',
      dataIndex: 'activated',
      key: 'activated',
      align: 'left',
      width: 100,
      ellipsis: true,
      sorter: (a, b) => Number(a.activated) - Number(b.activated),
      render: (text, record) => (
          <span>{record.activated == true ? <div>Hoạt động</div> : <div>Ẩn</div>}</span>
      )
    },
    {
      title: 'Action',
      key: 'action',
      dataIndex: 'action',
      fixed: 'right',
      render: (text, record) => (
          <Dropdown overlay={menu(record)} placement="bottomLeft" trigger={['click']}>
            <span className="gx-link ant-dropdown-link">
              <i className="gx-icon-btn icon icon-ellipse-v"/>
            </span>
          </Dropdown>
      ),
    }
  ];

  return (
      <MainLayout>

        <Card className="gx-card">
          <Breadcrumb>
            <Breadcrumb.Item>Thiết lập</Breadcrumb.Item>
            <Breadcrumb.Item><span className="gx-link">Sản phẩm</span></Breadcrumb.Item>
          </Breadcrumb>
        </Card>

        <Card className="gx-card">
          {/*<Table className="gx-table-no-bordered" columns={ColumnProductSimple.columns}*/}
          {/*       dataSource={products2} pagination={false}*/}
          {/*       size="small"/>*/}

          <TableOption
              isLoading={loading}
              columns={columns}
              onReload={fetchProductsByStoreId}
              data={products}
              insertFunction={insertBox()}
              defaultCheckedList={defaultCheckedList}
              pagination={pagination}
          />

          <ModalFormProduct
              modalTitle="Thêm sản phẩm"
              showModal={showModal}
              handleOk={handleOk}
              isLoadingOk={false}
              handleCancel={handleCancel}
              data={product}
          />

        </Card>
      </MainLayout>
  );
};

export default Index;
