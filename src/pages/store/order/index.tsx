import {NextPage} from "next"
import {useDispatch, useSelector} from "react-redux";
import MainLayout from "../../../layout/main/MainLayout";
import {AppState} from "../../../redux";
import React, {useEffect, useState} from "react";
import _ from 'lodash';
import {Authorities} from "../../../constant/Authorities";
import {Breadcrumb, Card, Dropdown, Menu, Table} from "antd";
import TableOption from "../../../component/Table/TableOption";
import {CommonHelper} from "../../../helper/CommonHelper";
import {IUser} from "../../../model/entity/IUser";
import {useRouter} from "next/router";
import {OrderAdminActions} from "../../../redux/slice/admin/orderAdminSlice";
import {ColumnProps} from "antd/es/table";
import {IOrder} from "../../../model/entity/IOrder";
import Error403 from "../../../component/ErrorPages/Error403";
import {OrderActions} from "../../../redux/slice/orderSlice";

interface IProps {
}

const initOrders: IOrder[] = [{
  id: 0,
  userId: 0,
  totalPrice: 0,
  receiverName: "",
  email: "",
  phone: "",
  houseNumberStreet: "",
  address: "",
  shipmentMethod: "",
  paymentMethod: "",
  status: "",
  createdAt: "",
  orderStoreDetails: [{
    storeId: 0,
    totalPrice: 0,
    status: "",
    orderProductDetails: null
  }]
}]

const initPagination = {
  current: 1,
  pageSize: 10,
  total: 10,
  onChange: null
}

const Index: NextPage<IProps> = () => {
      const dispatch = useDispatch();
      const router = useRouter();

      const {user}: { user: IUser } = useSelector((state: AppState) => state.me);
      const authorities = CommonHelper.getAuthorities(user);

      const [orders, setOrders] = useState<IOrder[]>(initOrders);
      const [loading, setLoading] = useState<boolean>(true);
      const [pagination, setPagination] = useState(initPagination);

      const limit = 10;
      const {page} = router.query;

      /**
       * This is private page, must validate role before access
       **/
      const privilege: boolean = _.includes(authorities, Authorities.ROLE_STORE);

      const fetchOrderByStore = () => {
        setLoading(true);
        dispatch(OrderActions.fetchOrdersByStore(limit, (page != null ? Number(page) - 1 : 0) * limit, (orders, paging) => {
          setOrders(orders);
          setPagination({
            current: page != null ? Number(page) : 1,
            pageSize: limit,
            total: paging.total,
            onChange: changePage
          });
          setLoading(false);
        }, () => {
          setOrders(initOrders);
          setPagination(initPagination);
        }))
      }

      const changePage = (page) => {
        router.push(process.env.APP_SUB_DOMAIN + `/store/order?page=${page}`).then(() => {
          setLoading(true);
          setOrders(initOrders);
        })
      }

      /**
       * This is private page, must validate role before access
       **/
      if (!privilege)
        return <Error403/>;

      useEffect(() => {
        fetchOrderByStore();
      }, [page]);

      const insertBox = () => {
        return (<></>);
      };

      const renderStatus = (record) => {
        switch (record) {
          case 'CREATED':
            return 'Chờ xác nhận'
          case 'ACCEPTED':
            return 'Đã đồng ý'
          case 'CANCELLED':
            return 'Đã hủy'
          case 'COMPLETED':
            return 'Đã hoàn thành'
        }
      }

      const handleAccept = async (record) => {
        await dispatch(OrderAdminActions.acceptOrder(record.id));
      }

      const handleCancel = async (record) => {
        await dispatch(OrderAdminActions.declineOrder(record.id));
      }

      const handleShip = async (record) => {
        await dispatch(OrderAdminActions.shipOrder(record.id));
      }

      const menu = (record) => (
          <Menu>
            {record.status != 'ACCEPTED' && record.status != 'CANCELLED' && record.status != 'COMPLETED' ?
                <Menu.Item key="0">
                  <a rel="noopener noreferrer" href="#" onClick={(e) => {
                    e.preventDefault();
                    handleAccept(record).then(() => fetchOrderByStore());
                  }}>Xác nhận</a>
                </Menu.Item> : <div></div>}

            {record.status == 'ACCEPTED' && record.status != 'CANCELLED' && record.status != 'COMPLETED' ?
                <Menu.Item key="0">
                  <a rel="noopener noreferrer" href="#" onClick={(e) => {
                    e.preventDefault();
                    handleShip(record).then(() => fetchOrderByStore());
                  }}>Đã giao hàng</a>
                </Menu.Item> : <div></div>}

            {record.status != 'CANCELLED' && record.status != 'COMPLETED' ?
                <Menu.Item key="1">
                  <a rel="noopener noreferrer" href="#" onClick={(e) => {
                    e.preventDefault();
                    handleCancel(record).then(() => fetchOrderByStore());
                  }}>Hủy đơn hàng</a>
                </Menu.Item> : <div></div>}

          </Menu>
      );

      const expandedOrderStoreDetailRender = (record) => {

        console.log("record: ", record);

        const data = record.orderStoreDetails[0].orderProductDetails;
        const storeId = record.orderStoreDetails[0].storeId;
        const columns = [
          {
            title: 'Sản phẩm',
            dataIndex: 'productName',
            key: 'productName',
            width: '35%',
            sorter: (a, b) => Number(a.productName.length) - Number(b.productName.length),
            render: (text, record) =>
                <a href={process.env.APP_SUB_DOMAIN + `/product/${storeId}/${record.productId}`}>{record.productName}</a>
          },
          {
            title: 'Đơn giá',
            dataIndex: 'price',
            key: 'price',
            sorter: (a, b) => Number(a.price) - Number(b.price),
            width: '15%'
          },
          {
            title: 'Số lượng',
            dataIndex: 'quantity',
            key: 'quantity',
            sorter: (a, b) => Number(a.quantity) - Number(b.quantity),
            width: '30%'
          },
          {
            title: 'Thành tiền',
            dataIndex: 'totalPrice',
            key: 'totalPrice',
            sorter: (a, b) => Number(a.totalPrice) - Number(b.totalPrice),
            width: '20%',
          }
        ];
        return <Table columns={columns} dataSource={data} pagination={false}/>;
      };

      /**
       *  Column structure for table
       */
      const columns: ColumnProps<IOrder>[] = [
        {
          title: 'ID',
          dataIndex: 'id',
          key: 'id',
          width: '10%',
          sorter: (a, b) => Number(a.id) - Number(b.id),
        }, {
          title: 'Tên người nhận',
          dataIndex: 'receiverName',
          key: 'receiverName',
          width: '15%',
          sorter: (a, b) => Number(a.receiverName.length) - Number(b.receiverName.length),
        }, {
          title: 'Số điện thoại',
          dataIndex: 'phone',
          key: 'phone',
          width: '10%',
          sorter: (a, b) => Number(a.phone) - Number(b.phone),
        }, {
          title: 'Số nhà, tên đường',
          dataIndex: 'houseNumberStreet',
          key: 'houseNumberStreet',
          width: '15%',
          sorter: (a, b) => Number(a.houseNumberStreet.length) - Number(b.houseNumberStreet.length),
        }, {
          title: 'Địa chỉ',
          dataIndex: 'address',
          key: 'address',
          width: '30%',
          sorter: (a, b) => Number(a.address.length) - Number(b.address.length),
        }, {
          title: 'Thành tiền',
          dataIndex: 'totalPrice',
          key: 'totalPrice',
          width: '10%',
          sorter: (a, b) => Number(a.totalPrice) - Number(b.totalPrice),
        }, {
          title: 'Trạng thái',
          dataIndex: 'status',
          key: 'status',
          width: '10%',
          sorter: (a, b) => Number(a.status.length) - Number(b.status.length),
          render: (text, record) =>
              <div>{renderStatus(record.orderStoreDetails[0].status)}</div>
        },
        {
          title: 'Hành động',
          key: 'action',
          dataIndex: 'action',
          render: (text, record) => (
              <Dropdown overlay={menu(record.orderStoreDetails[0])} placement="bottomLeft" trigger={['click']}>
            <span className="gx-link ant-dropdown-link">
              <i className="gx-icon-btn icon icon-ellipse-v"/>
            </span>
              </Dropdown>
          ),
        }
      ];

      /**
       * Business logic code
       **/
      const defaultCheckedList = CommonHelper.getNameOfColumn(columns);

      return (
          <MainLayout>

            <Card className="gx-card">
              <Breadcrumb>
                <Breadcrumb.Item>Danh sách</Breadcrumb.Item>
                <Breadcrumb.Item><span className="gx-link">Đơn hàng</span></Breadcrumb.Item>
              </Breadcrumb>
            </Card>

            <Card className="gx-card">
              <TableOption
                  isLoading={loading}
                  columns={columns}
                  onReload={fetchOrderByStore}
                  data={orders}
                  insertFunction={insertBox()}
                  defaultCheckedList={defaultCheckedList}
                  expandedRowRender={expandedOrderStoreDetailRender}
                  pagination={pagination}
              />
            </Card>

          </MainLayout>
      );
    }
;

export default Index;
