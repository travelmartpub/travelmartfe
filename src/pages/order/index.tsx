import {NextPage} from "next"
import {useDispatch, useSelector} from "react-redux";
import MainLayout from "../../layout/travelmart/main/MainLayout";
import {AppState} from "../../redux";
import React, {useEffect, useState} from "react";
import {Authorities} from "../../constant/Authorities";
import {Breadcrumb, Card, Table} from "antd";
import {CommonHelper} from "../../helper/CommonHelper";
import {IUser} from "../../model/entity/IUser";
import {useRouter} from "next/router";
import Error403 from "../../component/ErrorPages/Error403";
import _ from 'lodash';
import {IOrder, IOrderStoreDetail} from "../../model/entity/IOrder";
import {OrderActions} from "../../redux/slice/orderSlice";

interface IProps {
}

const initOrders: IOrder[] = [{
  id: 0,
  userId: 0,
  totalPrice: 0,
  receiverName: "",
  email: "",
  phone: "",
  houseNumberStreet: "",
  address: "",
  shipmentMethod: "",
  paymentMethod: "",
  status: "",
  createdAt: "",
  orderStoreDetails: new Array<IOrderStoreDetail>()
}]

const initPagination = {
  current: 1,
  pageSize: 10,
  total: 10,
  onChange: null
}

const Index: NextPage<IProps> = () => {
      const dispatch = useDispatch();
      const router = useRouter();

      const {user}: { user: IUser } = useSelector((state: AppState) => state.me);
      const authorities = CommonHelper.getAuthorities(user);

      const [orders, setOrders] = useState<IOrder[]>(initOrders);
      const [loading, setLoading] = useState<boolean>(true);
      const [pagination, setPagination] = useState(initPagination);

      const limit = 10;
      const {page} = router.query;

      /**
       * This is private page, must validate role before access
       **/
      const privilege: boolean = _.includes(authorities, Authorities.ROLE_USER);

      /**
       * This is private page, must validate role before access
       **/
      if (!privilege)
        return <Error403/>;

      useEffect(() => {
        dispatch(OrderActions.fetchOrders(limit, (page != null ? Number(page) - 1 : 0) * limit, (data, paging) => {
          setOrders(data);
          setPagination({
            current: page != null ? Number(page) : 1,
            pageSize: limit,
            total: paging.total,
            onChange: changePage
          });
          setLoading(false);
        }, () => {
          setOrders(initOrders);
          setPagination(initPagination);
        }));

      }, [page]);

      const changePage = (page) => {
        router.push(process.env.APP_SUB_DOMAIN + `/order?page=${page}`).then(() => {
          setLoading(true);
          setOrders(initOrders);
        })
      }

      const renderStatus = (status) => {
        switch (status) {
          case 'CREATED':
            return 'Đã đặt hàng'
          case 'ACCEPTED':
            return 'Chờ vận chuyển'
          case 'CANCELLED':
            return 'Đã hủy'
          case 'COMPLETED':
            return 'Đã hoàn thành'
        }
      }

      const expandedOrderRender = (record) => {

        console.log("record: ", record);
        const data = record.orderStoreDetails;

        const columns = [
          {
            title: 'Cửa hàng',
            dataIndex: 'storeName',
            key: 'storeName',
            width: '50%',
            sorter: (a, b) => Number(a.storeName.length) - Number(b.storeName.length),
            render: (text, record) =>
                <a href={process.env.APP_SUB_DOMAIN + `/stores/${record.storeId}`}>{record.storeName}</a>
          },
          {
            title: 'Thành tiền',
            dataIndex: 'totalPrice',
            width: '25%',
            key: 'totalPrice',
            sorter: (a, b) => Number(a.totalPrice) - Number(b.totalPrice),
          },
          {
            title: 'Trạng thái',
            dataIndex: 'status',
            key: 'status',
            width: '25%',
            sorter: (a, b) => Number(a.status.length) - Number(b.status.length),
            render: (text, record) =>
                <div>{renderStatus(record.status)}</div>
          },
        ];

        return <Table columns={columns} dataSource={data} expandedRowRender={expandedOrderStoreDetailRender}
                      pagination={false}/>;
      };

      const expandedOrderStoreDetailRender = (record) => {

        console.log("record: ", record);

        const data = record.orderProductDetails;
        const storeId = record.storeId;
        const columns = [
          {
            title: 'Sản phẩm',
            dataIndex: 'productName',
            key: 'productName',
            width: '25%',
            sorter: (a, b) => Number(a.productName.length) - Number(b.productName.length),
            render: (text, record) =>
                <a href={process.env.APP_SUB_DOMAIN + `/product/${storeId}/${record.productId}`}>{record.productName}</a>
          },
          {
            title: 'Đơn giá',
            dataIndex: 'price',
            key: 'price',
            width: '25%',
            sorter: (a, b) => Number(a.price) - Number(b.price),
          },
          {
            title: 'Số lượng',
            dataIndex: 'quantity',
            key: 'quantity',
            width: '25%',
            sorter: (a, b) => Number(a.quantity) - Number(b.quantity),
          },
          {
            title: 'Thành tiền',
            dataIndex: 'totalPrice',
            key: 'totalPrice',
            width: '25%',
            sorter: (a, b) => Number(a.totalPrice) - Number(b.totalPrice),
          }
        ];
        return <Table columns={columns} dataSource={data} pagination={false}/>;
      };

      const columns = [{
        title: 'ID',
        dataIndex: 'id',
        key: 'id',
        width: '10%',
        sorter: (a, b) => Number(a.id) - Number(b.id),
      }, {
        title: 'Tên người nhận',
        dataIndex: 'receiverName',
        key: 'receiverName',
        width: '15%',
        sorter: (a, b) => Number(a.receiverName.length) - Number(b.receiverName.length),
      }, {
        title: 'Số điện thoại',
        dataIndex: 'phone',
        key: 'phone',
        width: '10%',
        sorter: (a, b) => Number(a.phone) - Number(b.phone),
      }, {
        title: 'Số nhà, tên đường',
        dataIndex: 'houseNumberStreet',
        key: 'houseNumberStreet',
        width: '15%',
        sorter: (a, b) => Number(a.houseNumberStreet.length) - Number(b.houseNumberStreet.length),
      }, {
        title: 'Địa chỉ',
        dataIndex: 'address',
        key: 'address',
        width: '30%',
        sorter: (a, b) => Number(a.address.length) - Number(b.address.length),
      }, {
        title: 'Thành tiền',
        dataIndex: 'totalPrice',
        key: 'totalPrice',
        width: '10%',
        sorter: (a, b) => Number(a.totalPrice) - Number(b.totalPrice),
      }, {
        title: 'Trạng thái',
        dataIndex: 'status',
        key: 'status',
        width: '10%',
        sorter: (a, b) => Number(a.status.length) - Number(b.status.length),
        render: (text, record) =>
            <div>{renderStatus(record.status)}</div>
      }];

      return (
          <MainLayout>
            <div>
              <Card className="gx-card">
                <Breadcrumb>
                  <Breadcrumb.Item>Đơn hàng</Breadcrumb.Item>
                </Breadcrumb>
              </Card>
              <Card className="gx-card">
                <Table className="gx-table-responsive"
                       loading={loading}
                       columns={columns}
                       dataSource={orders}
                       expandedRowRender={expandedOrderRender}
                       pagination={pagination}/>
              </Card>
            </div>

          </MainLayout>
      );
    }
;

export default Index;
