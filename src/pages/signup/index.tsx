import React, {SyntheticEvent, useEffect} from "react";
import {Button, Checkbox, Form, Icon, Input, Modal} from "antd";
import {useDispatch, useSelector} from 'react-redux';
import {FormComponentProps} from "antd/lib/form";
import {NextPage} from "next";
import InfoView from "../../component/InfoView";
import {useRouter} from "next/router";
import {ISignUp} from "../../model/Signup";
import {SigninPage} from "../../constant/Commons";
import {authActions} from "../../redux/slice/authSlice";
import Link from "next/link";
import {IUser} from "../../model/entity/IUser";
import {AppState} from "../../redux";

interface IProfile {
  username: string;
  email: string;
  password: string;
}

type IProps = FormComponentProps

const SignUp: NextPage<IProps> = (props: IProps) => {

      const dispatch = useDispatch();
      const router = useRouter();

      const {getFieldDecorator, validateFields, resetFields} = props.form;

      const {user}: { user: IUser } = useSelector((state: AppState) => state.me);

      useEffect(() => {
        if (user != undefined && user.id != 0) {
          router.push(process.env.APP_SUB_DOMAIN + "/");
        }
      }, []);

      const handleSubmit = (e: SyntheticEvent) => {
        e.preventDefault();
        validateFields(async (err, values: ISignUp) => {
          if (!err) {

            if (values.password != values.confirmPassword) {
              Modal.warn({
                title: 'Mật khẩu không khớp'
              });
              return;
            }
            await dispatch(authActions.register(values, () => {
                  Modal.confirm({
                    title: 'Đăng ký thành công, chuyển đến trang đăng nhập?',
                    // content: 'Some descriptions',
                    okText: "Đăng nhập",
                    cancelText: "Chưa muốn",
                    onOk() {
                      router.push(SigninPage(router))
                    },
                    onCancel() {
                      resetFields();
                    },
                  });
                }, (error) => {
                  Modal.warning({
                    title: 'Đăng ký không thành công!',
                    content: error
                  })
                }
            ));

          }
        });
      };

      return (
          <div className="gx-app-login-wrap">
            <div className="gx-app-login-container">
              <div className="gx-app-login-main-content">
                <div className="gx-app-logo-content">
                  <div className="gx-app-logo-content-bg">
                    <img src={process.env.APP_SUB_DOMAIN + "/assets/images/neature.e7f17afb.jpg"}
                         alt='Neature'/>
                  </div>
                  <div className="gx-app-logo-wid">
                    {/*<h1><IntlMessages id="app.userAuth.signIn"/></h1>*/}
                    {/*<p><IntlMessages id="app.userAuth.bySigning"/></p>*/}
                    {/*<p><IntlMessages id="app.userAuth.getAccount"/></p>*/}
                    <h1>Đăng ký</h1>
                    <p></p>
                    <p></p>
                  </div>
                  <div className="gx-app-logo">
                    {/*<img alt="example" src={"assets/images/logo.png"}/>*/}
                    <b>TRAVEL MART ©</b>
                  </div>
                </div>
                <div className="gx-app-login-content">
                  <Form onSubmit={e => {
                    handleSubmit(e)
                  }} className="gx-signin-form gx-form-row0">

                    <Form.Item>
                      {getFieldDecorator('username', {
                        initialValue: "",
                        rules: [{required: true, message: 'PVui lòng nhập tên đăng nhập',}],
                      })(
                          <Input placeholder="Tên đăng nhập"/>
                      )}

                    </Form.Item>
                    <Form.Item>
                      {getFieldDecorator('email', {
                        initialValue: "",
                        rules: [{
                          type: 'email', message: 'E-mail không hợp lệ',
                        }, {
                          required: true, message: 'Vui lòng nhập E-mail',
                        }],
                      })(
                          <Input placeholder="E-mail"/>
                      )}

                    </Form.Item>
                    <Form.Item>
                      {getFieldDecorator('password', {
                        // initialValue: "demo#123",
                        rules: [{required: true, message: 'Vui lòng nhập mật khẩu'}],
                      })(
                          <Input type="password" placeholder="Mật khẩu"/>
                      )}

                    </Form.Item>
                    <Form.Item>
                      {getFieldDecorator('confirmPassword', {
                        // initialValue: "demo#123",
                        rules: [{required: true, message: 'Vui lòng nhập mật khẩu'}],
                      })(
                          <Input type="password" placeholder="Xác nhận mật khẩu"/>
                      )}

                    </Form.Item>
                    <Form.Item>
                      {getFieldDecorator('toc', {
                        valuePropName: 'checked',
                        initialValue: true,
                      })(
                          <Checkbox>
                            {/*<IntlMessages id="appModule.iAccept"/>*/}
                            Đồng ý
                          </Checkbox>
                      )}
                      <span className="gx-signup-form-forgot gx-link">
                    {/*<IntlMessages  id="appModule.termAndCondition"/>*/}
                        Điều khoản dịch vụ
                  </span>
                    </Form.Item>
                    <Form.Item>
                      <Button type="primary" className="gx-mb-0" htmlType="submit">
                        {/*<IntlMessages id="app.userAuth.signIn"/>*/}
                        Đăng ký
                      </Button>
                      <span>
                    {/*<IntlMessages id="app.userAuth.or"/>*/}
                        <span>hoặc </span>
                  </span>
                      {/*<Link href="/signup" as="/signup">*/}
                      {/*  /!*<IntlMessages id="app.userAuth.signUp"/>*!/*/}
                      {/*  Sign Up*/}
                      {/*</Link>*/}
                      <span className="gx-link">
                        <Link href={process.env.APP_SUB_DOMAIN + '/signin'}>
                          Đăng nhập
                        </Link>
                      </span>
                    </Form.Item>

                    <div className="gx-flex-row gx-justify-content-between">
                      <span></span>
                      <ul className="gx-social-link">
                        <li>
                          {/*<GoogleOutlined/>*/}
                          <Icon type="google" onClick={() => {
                            // this.props.showAuthLoader();
                            // this.props.userGoogleSignIn();
                          }}/>
                        </li>
                        <li>
                          {/*<FacebookOutlined/>*/}
                          <Icon type="facebook" onClick={() => {
                            // this.props.showAuthLoader();
                            // this.props.userFacebookSignIn();
                          }}/>
                        </li>
                        <li>
                          {/*<GithubOutlined/>*/}
                          <Icon type="github" onClick={() => {
                            // this.props.showAuthLoader();
                            // this.props.userGithubSignIn();
                          }}/>
                        </li>
                      </ul>
                    </div>

                    {/*<span*/}
                    {/*  className="gx-text-light gx-fs-sm"> demo user email: 'demo@example.com' and password: 'demo#123'</span>*/}
                  </Form>
                </div>

                <InfoView/>

              </div>
            </div>
          </div>
      );
    }
;

const WrappedNormalRegisterForm = Form.create<IProps>()(SignUp);
export default WrappedNormalRegisterForm;
