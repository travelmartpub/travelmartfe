import {NextPage} from "next"
import {useDispatch, useSelector} from "react-redux";
import {Card, Col, Modal, Row, Table, Form, Popconfirm, Menu, Dropdown} from "antd";
import MainLayout from "../../layout/main/MainLayout";
import {ColumnProps} from "antd/es/table";
import { ICategoryItem } from "../../model/ICategoryItem";
import { AppState } from "../../redux";
import { categoryItemActions } from "../../redux/slice/categoryItemSlice";
import React, {useEffect, useState, useRef, SyntheticEvent} from "react";
import { ServiceForm } from '../../component/DataEntry/Form';
interface IProps {
  form
}
const Index: NextPage<IProps> = (props: IProps) => {
    const dispatch = useDispatch();
    const { categoryItems, isLoading} = useSelector((state: AppState) => state.categoryItem)
    const [isModalVisible, setIsModalVisible] = useState(false);

    const fetchCategoryItems = () => {
        dispatch(categoryItemActions.getAllCategoryItems());
    }

    useEffect(() => {
        fetchCategoryItems();
    }, [])

    const columns: ColumnProps<ICategoryItem>[] = [
        {
          title: 'ID',
          dataIndex: 'id',
          key: 'id',
          align: 'left'
        },
        {
          title: 'CategoryItem Name',
          dataIndex: 'name',
          key: 'name',
          align: 'left',
          render: text => <span className="gx-link">{text}</span>
        },
        {
          title: 'Description',
          dataIndex: 'description',
          key: 'description',
          align: 'left',
          render: text => <span className="gx-link">{text}</span>
        },
        {
          title: 'Action',
          key: 'action',
          dataIndex: 'id',
          align: 'left',
          render: (text) => (
            <Dropdown overlay={ () => showMenu(text)} placement="bottomRight">
              {/*<MoreOutlined style={{ fontSize: '32px', color: '#08c' }}/>*/}
            </Dropdown>
          )
        }
      ];
    const { form } = props;
    const { setFieldsValue, resetFields} = form;

    const deleteService = async (id: number) => {
      await dispatch(categoryItemActions.deleteCategoryItem(id));
          fetchCategoryItems();
    }

  const showMenu = (id: number) => ( 
    <Menu>
      <Menu.Item key="Edit" onClick={() => showModalUpdate(id)}>Edit</Menu.Item>
      <Menu.Item key="Delete">
        <Popconfirm onConfirm={() =>deleteService(id)} title="Are you sure？">
          Delete
        </Popconfirm>
      </Menu.Item>
    </Menu>
  )

  const showModalUpdate = (id: number) => {
    const data = categoryItems.find( (service)=> service.id === id)
    setFieldsValue({
      id: id,
      name: data.name,
      description: data.description
    })
    setIsModalVisible(true);
  }

    const handleUpdate = (e: SyntheticEvent) => {
      e.preventDefault();
      props.form.validateFields( async(err, values) => {
        if (!err) {
          await dispatch(categoryItemActions.updateCategoryItem(values.id, values.name, values.description));
          fetchCategoryItems();
          setIsModalVisible(false);
        }
      });
      resetFields();
    }
    const handleCancel = () => {
      setIsModalVisible(false);
      form.resetFields();
    };

    const handleSubmit = (e: SyntheticEvent)  =>{
      e.preventDefault();
      props.form.validateFields( async(err, values) => {
        if (!err) {
          await dispatch(categoryItemActions.createCategoryItem(values.name, values.description));
          fetchCategoryItems();
        }
      })
      resetFields();
    }
    const nothing = () => {
      console.log("form edit")
    }
    return (
          <MainLayout>
            <Modal title="Basic Modal" visible={isModalVisible} onOk={handleUpdate} onCancel={handleCancel} width={800} okText="Edit">
              <ServiceForm form={form} name="frmEdit" onSubmit={nothing} />
            </Modal>
            <Card className="gx-card" title="Insert Service">
              <ServiceForm form={form} name="frmInsert" onSubmit={handleSubmit}/>
            </Card>
            {/*================================*/}
            {/*=========== TABLE ==============*/}
            {/*================================*/}
            <Card
                className="gx-card"
                title="categoryItems"
            >
              <Row>
                <Col span={24}>
    
                  <Table className="gx-table-responsive"
                         loading={isLoading}
                         columns={columns}
                         dataSource={categoryItems}
                         rowKey={record => String(record.id)}
                  />
                </Col>
              </Row>
            </Card>
          </MainLayout>
      );
};

export default Form.create()(Index);