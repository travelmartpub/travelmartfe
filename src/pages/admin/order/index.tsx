import {NextPage} from "next"
import {useDispatch, useSelector} from "react-redux";
import MainLayout from "../../../layout/main/MainLayout";
import {AppState} from "../../../redux";
import React, {useEffect} from "react";
import _ from 'lodash';
import {Authorities} from "../../../constant/Authorities";
import {Breadcrumb, Card} from "antd";
import TableOption from "../../../component/Table/TableOption";
import {CommonHelper} from "../../../helper/CommonHelper";
import {IUser} from "../../../model/entity/IUser";
import {SigninPage} from "../../../constant/Commons";
import {useRouter} from "next/router";
import {ColumnOrderSimple} from "../../../component/CustomForPage/order/ColumnOrderSimple";
import {IOrderAdminState} from "../../../model/state/admin/IOrderAdminState";
import {OrderAdminActions} from "../../../redux/slice/admin/orderAdminSlice";

interface IProps {
}

const Index: NextPage<IProps> = () => {
  const dispatch = useDispatch();
  const router = useRouter();

  const {user}: { user: IUser } = useSelector((state: AppState) => state.me);
  const authorities = CommonHelper.getAuthorities(user);

  const orderAdminState: IOrderAdminState = useSelector((state: AppState) => state.orderAdmin)
  // const {isLoading, error, orderStoreDetails} = orderAdminState;
  const {isLoading, error} = orderAdminState;

  const orderStoreDetails = [{
    id: 1640407557246,
    storeId: 1,
    totalPrice: 200000,
    orderProductDetails: []
  }, {
    id: 1640407568250,
    storeId: 1,
    totalPrice: 162000,
    orderProductDetails: []
  }, {
    id: 1640407571132,
    storeId: 1,
    totalPrice: 31000,
    orderProductDetails: []
  },]

  /**
   * This is private page, must validate role before access
   **/
  const privilege: boolean = _.includes(authorities, Authorities.ROLE_STORE);

  const fetchOrderStoreDetailsByStoreId = () => {
    dispatch(OrderAdminActions.fetchOrderStoreDetailsByStoreId(1, 20, 0));
  }
  /**
   * Hook useEffect call at top-level, before return
   **/
  useEffect(() => {
    if (!privilege) {
      router.push(SigninPage(router))
      return
    }

    fetchOrderStoreDetailsByStoreId()
  }, []);

  /**
   * This is private page, must validate role before access
   **/
  if (!privilege)
    return <></>;

  /**
   * Business logic code
   **/
  const defaultCheckedList = CommonHelper.getNameOfColumn(ColumnOrderSimple.columns);

  const insertBox = () => {
    return (<></>);
  };

  return (
      <MainLayout>

        <Card className="gx-card">
          <Breadcrumb>
            <Breadcrumb.Item>Home</Breadcrumb.Item>
            <Breadcrumb.Item><span className="gx-link">Orders</span></Breadcrumb.Item>
          </Breadcrumb>
        </Card>

        <Card className="gx-card">
          <TableOption
              isLoading={isLoading}
              columns={ColumnOrderSimple.columns}
              onReload={fetchOrderStoreDetailsByStoreId}
              data={orderStoreDetails}
              insertFunction={insertBox()}
              defaultCheckedList={defaultCheckedList}
          />
        </Card>

      </MainLayout>
  );
};

export default Index;
