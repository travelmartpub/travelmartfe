import {NextPage} from "next"
import {useDispatch, useSelector} from "react-redux";
import MainLayout from "../../../layout/main/MainLayout";
import {AppState} from "../../../redux";
import React, {useEffect, useState} from "react";
import _ from 'lodash';
import {Authorities} from "../../../constant/Authorities";
import Error403 from "../../../component/ErrorPages/Error403";
import {Breadcrumb, Button, Card, Table} from "antd";
import {ProductActions} from "../../../redux/slice/productSlice";
import ModalFormProduct from "../../../component/CustomForPage/product/ModalFormProduct";
import {ColumnProduct} from "../../../component/CustomForPage/product/ColumnProduct";
import TableOption from "../../../component/Table/TableOption";
import {ColumnProductSimple} from "../../../component/CustomForPage/product/ColumnProductSimple";
import {IPagingProductsState} from "../../../model/state/IPagingProducts";
import {PagingProductsActions} from "../../../redux/slice/pagingProductSlice";
import {IProductReq} from "../../../model/IProduct";
import {IProduct} from "../../../model/entity/IProduct";
import {CommonHelper} from "../../../helper/CommonHelper";
import {IUser} from "../../../model/entity/IUser";
import {SigninPage} from "../../../constant/Commons";
import {useRouter} from "next/router";
import {ProductAdminActions} from "../../../redux/slice/admin/productAdminSlice";
import {IProductAdminState} from "../../../model/state/admin/IProductAdminState";

interface IProps {
}

const Index: NextPage<IProps> = () => {
  const dispatch = useDispatch();
  const router = useRouter();

  const {user}: { user: IUser } = useSelector((state: AppState) => state.me);
  const authorities = CommonHelper.getAuthorities(user);
  const [showModal, setShowModal] = useState<boolean>(false);
  // const {products, isLoading, isLoadingCreate}:
  //     { products: Array<IProduct>, isLoading: boolean, isLoadingCreate: boolean } = useSelector((state: AppState) => state.product);

  const productAdminState: IProductAdminState = useSelector((state: AppState) => state.productAdmin)
  const {isLoading, error, products} = productAdminState;


  /**
   * This is private page, must validate role before access
   **/
  const privilege: boolean = _.includes(authorities, Authorities.ROLE_STORE);

  const fetchProductsByStoreId = () => {
    dispatch(ProductAdminActions.fetchProductsByStoreId(1, 20, 0));
  }
  /**
   * Hook useEffect call at top-level, before return
   **/
  useEffect(() => {
    if (!privilege) {
      router.push(SigninPage(router))
      return
    }

    fetchProductsByStoreId()
  }, []);

  /**
   * This is private page, must validate role before access
   **/
  if (!privilege)
    return <></>;

  /**
   * Business logic code
   **/
  const defaultCheckedList = CommonHelper.getNameOfColumn(ColumnProductSimple.columns);

  const handleOk = async (product: IProductReq): Promise<VoidFunction> => {
    setShowModal(false);
    // await dispatch(productActions.create(product));
    // await fetchAllProducts();
    return;
  };

  const handleCancel = () => {
    setShowModal(false);
  };

  const insertBox = () => {
    return (
        <Button onClick={() => {
          setShowModal(true)
        }}>
          Create Product
        </Button>
    );
  };

  return (
      <MainLayout>

        <Card className="gx-card">
          <Breadcrumb>
            <Breadcrumb.Item>Home</Breadcrumb.Item>
            <Breadcrumb.Item><span className="gx-link">Products</span></Breadcrumb.Item>
          </Breadcrumb>
        </Card>

        <Card className="gx-card">
          {/*<Table className="gx-table-no-bordered" columns={ColumnProductSimple.columns}*/}
          {/*       dataSource={products2} pagination={false}*/}
          {/*       size="small"/>*/}

          <TableOption
              isLoading={isLoading}
              columns={ColumnProductSimple.columns}
              onReload={fetchProductsByStoreId}
              data={products}
              insertFunction={insertBox()}
              defaultCheckedList={defaultCheckedList}
          />

          <ModalFormProduct
              modalTitle="Create New Product"
              showModal={showModal}
              handleOk={handleOk}
              isLoadingOk={false}
              handleCancel={handleCancel}
          />

        </Card>
      </MainLayout>
  );
};

export default Index;
