import {NextPage} from "next"
import {useDispatch, useSelector} from "react-redux";
import {AppState} from "../../redux";
import React, {SyntheticEvent, useEffect, useState} from "react";
import _ from 'lodash';
import {Authorities} from "../../constant/Authorities";
import {useRouter} from "next/router";
import {IUser} from "../../model/entity/IUser";
import {CommonHelper} from "../../helper/CommonHelper";
import Error403 from "../../component/ErrorPages/Error403";
import MainLayout from "../../layout/travelmart/main/MainLayout";
import {Button, Card, Col, Form, Input, Modal, Row} from "antd";
import ContainerHeader from "../../component/ContainerHeader";
import {FormComponentProps} from "antd/lib/form";
import {IAddress} from "../../model/entity/IAddress";
import {addressActions} from "../../redux/slice/addressSlice";
import {IStore} from "../../model/entity/IStore";
import {StoreActions} from "../../redux/slice/storeSlice";
import {StorageHelper} from "../../helper/Storage";
import {MeActions} from "../../redux/slice/meSlice";

interface IProps extends FormComponentProps {
  showModal: boolean,
  handleCancel: VoidFunction,
  handleOk: VoidFunction,
}

const Index: NextPage<IProps> = (props: IProps) => {
  const router = useRouter();
  const dispatch = useDispatch();
  const {getFieldDecorator, validateFields} = props.form;

  const {user}: { user: IUser } = useSelector((state: AppState) => state.me);
  const {address}: { address: IAddress } = useSelector((state: AppState) => state.address);
  const authorities = CommonHelper.getAuthorities(user);

  const [showModal, setShowModal] = useState<boolean>(false);

  /**
   * This is private page, must validate role before access
   **/
  const storePrivilege = (_.includes(authorities, Authorities.ROLE_STORE));
  const userPrivilege: boolean = _.includes(authorities, Authorities.ROLE_USER);

  /**
   * Hook useEffect call at top-level, before return
   **/
  useEffect(() => {
    if (!userPrivilege) {
      return
    }
    dispatch(addressActions.getAddress());
  }, []);

  /**
   * This is private page, must validate role before access
   **/
  if (!userPrivilege)
    return <Error403/>;

  const handleSubmit = (e: SyntheticEvent) => {
    e.preventDefault();

    validateFields(async (err, values: IAddress) => {
      if (!err) {

        if (values.phone == '' || values.houseNumberStreet == '' || values.address == '') {
          Modal.warning({
            title: 'Cập nhật không thành công!',
            content: 'Vui lòng nhập đầy đủ thông tin địa chỉ hợp lệ'
          })
        }

        await dispatch(addressActions.setAddress(values, () => {
              Modal.success({
                title: 'Cập nhật thành công!'
              });
            }, () => {
              Modal.warning({
                title: 'Cập nhật không thành công!',
                content: 'Vui lòng kiểm tra lại thông tin địa chỉ'
              })
            }
        ));
      }
    });
    dispatch(addressActions.getAddress());
    return;
  }

  const ModalFormCreateStore = (props: IProps) => {

    const {getFieldDecorator, validateFields, resetFields} = props.form;
    const {showModal, handleCancel, handleOk} = props;

    const handleSubmit = (e: SyntheticEvent) => {
      e.preventDefault();
      validateFields(async (err, values: IStore) => {
        if (values.name == '') {
          Modal.warning({
            title: 'Đăng ký bán hàng không thành công!',
            content: 'Vui lòng nhập tên cửa hàng'
          })
          return;
        }

        if (!err) {
          dispatch(StoreActions.createUserStore(values.name, () => {
            Modal.success({
              title: 'Đăng ký bán hàng thành công, vui lòng đăng nhập lại!'
            });
          }, () => {
            Modal.warning({
              title: 'Đăng ký bán hàng không thành công!'
            })
          }));
        }
        const token = StorageHelper.getCookie('token');
        await dispatch(MeActions.getUserInfo(token));
        handleOk();
        resetFields();
      })
    }

    return (
        <Modal title="Đăng ký bán hàng (Yêu cầu đăng nhập lại)"
               visible={showModal}
               onCancel={handleCancel}
               width={1000}
               footer={[
                 <Button key="back" onClick={async () => {
                   await handleCancel();
                   resetFields();
                 }}>
                   Hủy
                 </Button>,
                 <Button key="submit" type="primary" onClick={handleSubmit}>
                   Xác nhận
                 </Button>
               ]}>
          <Form className="gx-app-login-container gx-app-login-content">
            <Form.Item>
              {getFieldDecorator('name', {
                initialValue: ""
              })(
                  <Input placeholder="Tên cửa hàng"/>
              )}
            </Form.Item>
          </Form>
        </Modal>
    )
  }

  const ModalCreateStoreForm = Form.create<IProps>()(ModalFormCreateStore)

  const handleCreateUserStore = () => {
    setShowModal(true);
  }

  /**
   * Business logic code
   **/
  return (
      <MainLayout>
        <Row className="gx-mb-5">
          <Col span={24}>
            <ContainerHeader title="HỒ SƠ"/>
          </Col>

          {/* product image */}
          <Col xl={24} lg={24} md={24} sm={24} xs={24}>

            <Card>
              <Form onSubmit={handleSubmit} className="gx-app-login-container gx-app-login-content">

                <Form.Item>
                  {getFieldDecorator('phone', {
                    initialValue: address.phone
                  })(
                      <Input type="number" placeholder="Số điện thoại"/>
                  )}
                </Form.Item>
                <Form.Item>
                  {getFieldDecorator('houseNumberStreet', {
                    initialValue: address.houseNumberStreet
                  })(
                      <Input placeholder="Số nhà, tên đường"/>
                  )}
                </Form.Item>
                <Form.Item>
                  {getFieldDecorator('address', {
                    initialValue: address.address
                  })(
                      <Input placeholder="Địa chỉ"/>
                  )}
                </Form.Item>
                <Form.Item>
                  <Button type="primary" className="gx-mb-0" htmlType="submit">
                    Lưu thông tin
                  </Button>

                  {!storePrivilege &&
                      <Button className="gx-mb-0" onClick={handleCreateUserStore}>
                        Đăng ký bán hàng
                      </Button>}
                </Form.Item>
              </Form>
            </Card>
          </Col>

        </Row>

        <ModalCreateStoreForm
            showModal={showModal}
            handleCancel={() => {
              setShowModal(false)
            }}
            handleOk={() => {
              setShowModal(false)
            }}/>
      </MainLayout>
  );
};

const Account = Form.create<IProps>()(Index);
export default Account;
