import App from "next/app";
import {Provider} from 'react-redux';
import {initializeStore} from "../redux";
import withRedux from 'next-redux-wrapper';
import "../../public/assets/vendors/style";
import "../../styles/wieldy.less";
import {authActions} from "../redux/slice/authSlice";
import {CartActions} from "../redux/slice/cartSlice";
import {useDispatch} from "react-redux";
import {useEffect} from "react";


const AppWrapper = ({children}) => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(CartActions.loadCartLocalStorage());
  }, [false]);

  return <>{children}</>;
};


class MainApp extends App<any> {

  static async getInitialProps({Component, ctx}: any) {
    const {store} = ctx;
    await store.dispatch(authActions.validateToken(ctx));

    const pageProps = {
      ...(Component.getInitialProps
          ? await Component.getInitialProps(ctx)
          : {})
    };

    return {
      pageProps
    };
  }

  constructor(props: any) {
    super(props);
  }

  async componentDidMount(): Promise<void> {
  }

  render() {
    const {Component, pageProps, store} = this.props;

    return (
        <Provider store={store}>
          <AppWrapper>
            <Component {...pageProps} />
          </AppWrapper>
        </Provider>
    );
  }
}

export default withRedux(initializeStore)(MainApp);
