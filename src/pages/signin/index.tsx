import React, {SyntheticEvent, useEffect, useState} from "react";
import {Button, Checkbox, Form, Icon, Input, Modal} from "antd";
import {useDispatch, useSelector} from 'react-redux';
import {FormComponentProps} from "antd/lib/form";
import {NextPage} from "next";
import InfoView from "../../component/InfoView";
import {authActions} from "../../redux/slice/authSlice";
import {ISignIn} from "../../model/Signin";
import {useRouter} from "next/router";
import Link from "next/link";
import {IUser} from "../../model/entity/IUser";
import {AppState} from "../../redux";

interface IProfile {
  username: string;
  password: string;

  [index: string]: any;
}

type IProps = FormComponentProps

const SignIn: NextPage<IProps> = (props: IProps) => {

      const dispatch = useDispatch();
      const router = useRouter();

      const [profile, setProfile] = useState<IProfile>({
        username: '',
        password: ''
      });

      const {user}: { user: IUser } = useSelector((state: AppState) => state.me);

      const {getFieldDecorator, validateFields} = props.form;

      // componentDidUpdate()
      // {
      //   if (this.props.token !== null) {
      //     this.props.history.push('/');
      //   }
      // }

      useEffect(() => {
        if (user != undefined && user.id != 0) {
          router.push(process.env.APP_SUB_DOMAIN + "/");
        }
      }, []);
      const handleSubmit = (e: SyntheticEvent) => {
        e.preventDefault();
        validateFields(async (err, values: ISignIn) => {
          if (!err) {

            // JUST DEMO REDUX
            // START =================
            // dispatch(commonActions.fetchSuccess("Just Test"));
            // END =================

            await dispatch(authActions.login(values, () => {
                  // Back to previous page (from url)
                  if (router.query && router.query.from) {
                    const from = router.query.from as string;
                    router.push(from);
                  } else {
                    router.push(process.env.APP_SUB_DOMAIN + "/");
                  }
                }, () => {
                  Modal.warning({
                    title: 'Đăng nhập không thành công!',
                    content: 'Vui lòng kiểm tra lại thông tin đăng nhập'
                  })
                }
            ));

          }
        });
      };

      return (
          <div className="gx-app-login-wrap">
            <div className="gx-app-login-container">
              <div className="gx-app-login-main-content">
                <div className="gx-app-logo-content">
                  <div className="gx-app-logo-content-bg">
                    <img src={process.env.APP_SUB_DOMAIN + "/assets/images/neature.e7f17afb.jpg"}
                         alt='Neature'/>
                  </div>
                  <div className="gx-app-logo-wid">
                    {/*<h1><IntlMessages id="app.userAuth.signIn"/></h1>*/}
                    {/*<p><IntlMessages id="app.userAuth.bySigning"/></p>*/}
                    {/*<p><IntlMessages id="app.userAuth.getAccount"/></p>*/}
                    <h1>Đăng nhập</h1>
                    <p></p>
                    <p></p>
                  </div>
                  <div className="gx-app-logo">
                    {/*<img alt="example" src={"assets/images/logo.png"}/>*/}
                    <b>TRAVEL MART ©</b>
                  </div>
                </div>
                <div className="gx-app-login-content">
                  <Form onSubmit={e => {
                    handleSubmit(e)
                  }} className="gx-signin-form gx-form-row0">

                    <Form.Item
                    >
                      {getFieldDecorator('username', {
                        initialValue: "admin",
                        rules: [{
                          required: true, message: 'Vui lòng nhập tên đăng nhập!',
                        }],
                      })(
                          <Input placeholder="Tên đăng nhập"/>
                      )}

                    </Form.Item>
                    <Form.Item>
                      {getFieldDecorator('password', {
                        // initialValue: "demo#123",
                        rules: [{required: true, message: 'Vui lòng nhập mật khẩu!'}],
                      })(
                          <Input type="password" placeholder="Mật khẩu"/>
                      )}

                    </Form.Item>
                    <Form.Item>
                      {getFieldDecorator('rememberMe', {
                        valuePropName: 'checked',
                        initialValue: true,
                      })(
                          <Checkbox>
                            {/*<IntlMessages id="appModule.iAccept"/>*/}
                            Nhớ đăng nhập?
                          </Checkbox>
                      )}
                    </Form.Item>
                    <Form.Item>
                      <Button type="primary" className="gx-mb-0" htmlType="submit">
                        {/*<IntlMessages id="app.userAuth.signIn"/>*/}
                        Đăng nhập
                      </Button>
                      <span>
                    {/*<IntlMessages id="app.userAuth.or"/>*/}
                        <span>hoặc </span>
                  </span>
                      {/*<Link href="/signup" as="/signup">*/}
                      {/*  /!*<IntlMessages id="app.userAuth.signUp"/>*!/*/}
                      {/*  Sign Up*/}
                      {/*</Link>*/}
                      <span className="gx-link">
                        <Link href={process.env.APP_SUB_DOMAIN + '/signup'}>
                          Đăng ký
                        </Link>
                      </span>
                    </Form.Item>

                    <div className="gx-flex-row gx-justify-content-between">
                      <span></span>
                      <ul className="gx-social-link">
                        <li>
                          {/*<GoogleOutlined/>*/}
                          <Icon type="google" onClick={() => {
                            // this.props.showAuthLoader();
                            // this.props.userGoogleSignIn();
                          }}/>
                        </li>
                        <li>
                          {/*<FacebookOutlined/>*/}
                          <Icon type="facebook" onClick={() => {
                            // this.props.showAuthLoader();
                            // this.props.userFacebookSignIn();
                          }}/>
                        </li>
                        <li>
                          {/*<GithubOutlined/>*/}
                          <Icon type="github" onClick={() => {
                            // this.props.showAuthLoader();
                            // this.props.userGithubSignIn();
                          }}/>
                        </li>
                      </ul>
                    </div>

                    {/*<span*/}
                    {/*  className="gx-text-light gx-fs-sm"> demo user email: 'demo@example.com' and password: 'demo#123'</span>*/}
                  </Form>
                </div>

                <InfoView/>

              </div>
            </div>
          </div>
      );
    }
;

const WrappedNormalLoginForm = Form.create<IProps>()(SignIn);
export default WrappedNormalLoginForm;
