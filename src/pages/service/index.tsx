import {NextPage} from "next"
import {useDispatch, useSelector} from "react-redux";
import MainLayout from "../../layout/main/MainLayout";
import {AppState} from "../../redux";
import React, {useEffect, useState} from "react";
import _ from 'lodash';
import {Authorities} from "../../constant/Authorities";
import Error403 from "../../component/ErrorPages/Error403";
import {Breadcrumb, Button, Card} from "antd";
import {IService, IServiceReq} from "../../model/IService";
import ModalFormService from "../../component/CustomForPage/services/ModalFormService";
import {serviceActions} from "../../redux/slice/serviceSlice";
import TableOption from "../../component/Table/TableOption";
import {ColumnService} from "../../component/CustomForPage/services/ColumnService";

interface IProps {
}

const Index: NextPage<IProps> = () => {
  const dispatch = useDispatch();

  const {authorities}: { authorities: Array<string> } = useSelector((state: AppState) => state.auth);
  const [showModal, setShowModal] = useState<boolean>(false);
  const {services, isLoading, isLoadingCreate}:
      { services: Array<IService>, isLoading: boolean, isLoadingCreate: boolean } = useSelector((state: AppState) => state.service);

  /**
   * This is private page, must validate role before access
   **/
  const privilege: boolean = _.includes(authorities, Authorities.ROLE_USER);

  const fetchAllServices = async () => {
    await dispatch(serviceActions.getAllServices());
  };

  /**
   * Hook useEffect call at top-level, before return
   **/
  useEffect(() => {
    if (privilege) {
      fetchAllServices()
    }
  }, []);

  /**
   * This is private page, must validate role before access
   **/
  if (!privilege)
    return <Error403/>;

  /**
   * Business logic code
   **/
  const defaultCheckedList = ["id", "name", "cost", "price", "description", "action"];

  const insertBox = () => {
    return (
        <Button onClick={() => {
          setShowModal(true)
        }}>
          Create Service
        </Button>
    );
  };

  const handleOk = async (service: IServiceReq): Promise<VoidFunction> => {
    setShowModal(false);
    await dispatch(serviceActions.create(service));
    await fetchAllServices();
    return;
  };

  const handleCancel = () => {
    setShowModal(false);
  };

  return (
      <MainLayout>
        <Card className="gx-card">
          <Breadcrumb>
            <Breadcrumb.Item>Home</Breadcrumb.Item>
            <Breadcrumb.Item><span className="gx-link">Service</span></Breadcrumb.Item>
          </Breadcrumb>
        </Card>

        <Card className="gx-card">

          <TableOption isLoading={isLoading}
                       columns={ColumnService.columns}
                       onReload={async () => {
                         await fetchAllServices();
                       }}
                       data={services}
                       insertFunction={insertBox()}
                       defaultCheckedList={defaultCheckedList}
          />

          <ModalFormService
              modalTitle="Create New Service"
              showModal={showModal}
              handleOk={handleOk}
              isLoadingOk={isLoadingCreate}
              handleCancel={handleCancel}
          />

        </Card>
      </MainLayout>
  );
};

export default Index;