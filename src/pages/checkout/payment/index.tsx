import React, {useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {NextPage} from "next";
import {Button, Card, Col, Form, Input, Modal, Radio, Row} from "antd";
import NumberFormat from "react-number-format";
import {AppState} from "../../../redux";
import {ICart} from "../../../model/state/ICart";
import {useRouter} from 'next/router'
import PaymentLayout from "../../../layout/travelmart/checkout/PaymentLayout";
import {CommonHelper} from "../../../helper/CommonHelper";
import RadioCard from "../../../component/DataEntry/Radio/RadioCard";
import {RadioChangeEvent} from "antd/lib/radio/interface";
import Link from "next/link";
import {OrderActions} from "../../../redux/slice/orderSlice";
import {CartActions} from "../../../redux/slice/cartSlice";
import {IUser} from "../../../model/entity/IUser";
import {SigninPage} from "../../../constant/Commons";
import Error404 from "../../../component/ErrorPages/Error404";
import {IAddress} from "../../../model/entity/IAddress";

const residences = [{
  value: 'Tp Hồ Chí Minh',
  label: 'Tp Hồ Chí Minh',
  children: [{
    value: 'Quận 7',
    label: 'Quận 7',
    children: [{
      value: 'Phường Bình Thuận',
      label: 'Phường Bình Thuận',
    }, {
      value: 'Phường Tân Hưng',
      label: 'Phường Tân Hưng',
    }],
  }],
}, {
  value: 'Đà Lạt',
  label: 'Đà Lạt',
  children: [{
    value: 'Xã An Bình',
    label: 'Xã An Bình',
    children: [{
      value: 'Phường Bà Triệu',
      label: 'Phường Bà Triệu',
    }],
  }],
}];


const FormItem = Form.Item;

const Index: NextPage = () => {
  const dispatch = useDispatch();
  const router = useRouter();

  const {user}: { user: IUser } = useSelector((state: AppState) => state.me);
  const {address}: { address: IAddress } = useSelector((state: AppState) => state.address);
  const cart: ICart = useSelector((state: AppState) => state.cart);
  const {stores, items, products} = cart;

  const [methodShip, setMethodShip] = useState<string>("SHIPPING");
  const [methodPayment, setMethodPayment] = useState<string>("COD");

  const [receiverName, setReceiverName] = useState<string>("");
  const [email, setEmail] = useState<string>("");
  const [phone, setPhone] = useState<string>(address.phone);
  const [houseNumberStreet, setHouseNumberStreet] = useState<string>(address.houseNumberStreet);
  const [addressData, setAddressData] = useState<string>(address.address);

  /**
   * Hook useEffect call at top-level, before return
   **/
  useEffect(() => {
  }, [])

  const formItemLayout = {
    wrapperCol: {
      xs: {span: 24},
      sm: {span: 24},
    },
  };

  const radioStyle = {
    display: "block",
    borderRadius: "6px"
  };

  const RadioGroup = Radio.Group;

  const onChange = (e: RadioChangeEvent) => {
    setMethodShip(e.target.value);
  }

  const onChangePayment = (e: RadioChangeEvent) => {
    setMethodPayment(e.target.value);
  }

  const changeReceiverName = (e) => {
    setReceiverName(e.target.value);
  }

  const changeEmail = (e) => {
    setEmail(e.target.value);
  }

  const changePhone = (e) => {
    setPhone(e.target.value);
  }

  const changeHouseNumberStreet = (e) => {
    setHouseNumberStreet(e.target.value);
  }

  const changeAddress = (e) => {
    setAddressData(e.target.value);
  }

  const createOrder = () => {

    // Check login
    if (user === undefined || user.id == 0) {

      Modal.confirm({
        title: 'Vui lòng đặng nhập trước khi mua hàng!',
        // content: 'Some descriptions',
        okText: "Đăng nhập",
        cancelText: "Chưa muốn",
        onOk() {
          router.push(SigninPage(router))
        },
        onCancel() {
        },
      });

      return;
    }

    if (receiverName == '' || phone == '' || houseNumberStreet == '' || addressData == '' || !/^\d+$/.test(phone)) {
      Modal.warning({
        title: 'Vui lòng cung cấp đúng thông tin nhận hàng'
      })
      return;
    }

    const orderRequest = CommonHelper.buildOrderRequest(user.id, receiverName, email, phone, houseNumberStreet, addressData,
        methodShip, methodPayment, stores, items, products);

    dispatch(OrderActions.createOrder(orderRequest,
        (orderId) => {
          router.push(`/checkout/complete?oid=${orderId}&status=success`).then(() => dispatch(CartActions.cleanCart()));
        }
        ,
        () => {
          router.push(`/checkout/complete?status=fail`);
        }
    ));
  }

  return (
      (cart.items.length == 0 ?
          <Error404/>
          :
          <PaymentLayout>

            <Row type="flex" justify="center" className="gx-mb-5">
              <Col xl={12} lg={12} md={12} sm={24} xs={24}>
                <Card>

                  <div className="gx-mb-4">
                    <h2 className="gx-pb-2 gx-checkout-price">Thông tin giao hàng</h2>
                    <p>Bạn đã có tài khoản? Đăng nhập</p>

                    <Form
                        // onSubmit={this.handleSubmit}
                    >

                      <FormItem
                          {...formItemLayout}
                          // label="E-mail"
                      >
                        {/*{getFieldDecorator('email', {*/}
                        {/*  rules: [{*/}
                        {/*    type: 'email', message: 'The input is not valid E-mail!',*/}
                        {/*  }, {*/}
                        {/*    required: true, message: 'Please input your E-mail!',*/}
                        {/*  }],*/}
                        {/*})(*/}
                        <Input onChange={changeReceiverName} value={receiverName} placeholder="Họ và tên"/>
                        {/*)}*/}
                      </FormItem>

                      <FormItem
                          {...formItemLayout}
                          // label="Captcha"
                          // extra="We must make sure that your are a human."
                      >
                        <Row>
                          <Col span={24} sm={12}>
                            {/*{getFieldDecorator('captcha', {*/}
                            {/*  rules: [{required: true, message: 'Please input the captcha you got!'}],*/}
                            {/*})(*/}
                            <Input onChange={changeEmail} value={email} type="email" placeholder="Email"/>
                            {/*)}*/}
                          </Col>
                          <Col span={24} sm={12}>
                            <Input onChange={changePhone} value={phone} placeholder="Số điện thoại"/>
                          </Col>
                        </Row>
                      </FormItem>

                      <FormItem
                          {...formItemLayout}
                      >
                        {/*{getFieldDecorator('nickname', {*/}
                        {/*  rules: [{required: true, message: 'Please input your nickname!', whitespace: true}],*/}
                        {/*})(*/}
                        <Input onChange={changeHouseNumberStreet} value={houseNumberStreet} placeholder="Số nhà, tên đường"/>
                        {/*)}*/}
                      </FormItem>
                      <FormItem
                          {...formItemLayout}
                          // label="Habitual Residence"
                      >
                        {/*{getFieldDecorator('residence', {*/}
                        {/*  initialValue: ['zhejiang', 'hangzhou', 'xihu'],*/}
                        {/*  rules: [{type: 'array', required: true, message: 'Please select your habitual residence!'}],*/}
                        {/*})(*/}
                        <Input onChange={changeAddress} value={addressData} placeholder="Địa chỉ"/>
                        {/*)}*/}
                      </FormItem>

                    </Form>
                  </div>

                  <div className="gx-mb-4">
                    <h2 className="gx-pb-2 gx-checkout-price">Phương thức vận chuyển</h2>

                    <RadioGroup
                        onChange={onChange}
                        style={{width: "100%"}}
                        // value={this.state.value}
                    >
                      <Radio.Button style={radioStyle} value="RECEIVE_AT_STORE">
                        <RadioCard checked={methodShip === "RECEIVE_AT_STORE"} text={"Nhận tại cửa hàng"}/>
                      </Radio.Button>
                      <Radio.Button style={radioStyle} value="SHIPPING">
                        <RadioCard checked={methodShip === "SHIPPING"} text={"Dịch vụ giao hàng"}/>
                      </Radio.Button>
                    </RadioGroup>

                  </div>

                  <div className="gx-mb-4">
                    <h2 className="gx-pb-2 gx-checkout-price">Phương thức thanh toán</h2>

                    <RadioGroup
                        onChange={onChangePayment}
                        style={{width: "100%"}}
                    >
                      <Radio.Button style={radioStyle} value="COD">
                        <RadioCard checked={methodPayment === "COD"}
                                   image={process.env.APP_SUB_DOMAIN + '/assets/images/icon-payment-method-cod.svg'}
                                   text={"Thanh toán tiền mặt khi nhận hàng"}/>
                      </Radio.Button>
                      <Radio.Button style={radioStyle} value="ZALOPAY">
                        <RadioCard checked={methodPayment === "ZALOPAY"}
                                   image={process.env.APP_SUB_DOMAIN + '/assets/images/icon-payment-method-zalo-pay.svg'}
                                   text={"Thanh toán bằng ví ZaloPay"}/>
                      </Radio.Button>
                      <Radio.Button style={radioStyle} value="MOMO">
                        <RadioCard checked={methodPayment === "MOMO"}
                                   image={process.env.APP_SUB_DOMAIN + '/assets/images/icon-payment-method-momo.svg'}
                                   text={"Thanh toán bằng ví MoMo"}/>
                      </Radio.Button>
                    </RadioGroup>
                  </div>

                  <Row>
                    <Col lg={14} md={14} sm={24} xs={24}>
                      <Link href="/checkout/cart">
                        <a href="/checkout/cart">Giỏ hàng</a>
                      </Link>
                    </Col>

                    <Col lg={10} md={10} sm={24} xs={24}>
                      <Button
                          onClick={createOrder}
                          className="gx-btn-secondary gx-mb-3 ant-btn-lg ant-btn-block">
                        ĐẶT MUA</Button>
                    </Col>
                  </Row>

                </Card>

              </Col>

              <Col xl={6} lg={6} md={6} sm={24} xs={24}>
                <Card>
                  <div className="ant-row-flex gx-mb-0">
                    <h2 className="gx-pb-3 gx-checkout-price">Đơn hàng</h2>
                  </div>

                  <div className="ant-row-flex gx-mb-4 gx-mb-md-4 gx-checkout-summary-price">
                    <div
                        className="gx-font-weight-normal gx-fs-lg gx-checkout-summary-price-title">
                      Thành tiền:
                    </div>
                    <div className="gx-fs-xxl gx-font-weight-medium gx-ml-auto gx-text-primary">
                      <NumberFormat
                          value={CommonHelper.calcTotalPrice(items, products)}
                          displayType={"text"}
                          thousandSeparator={true}
                          suffix={""}
                      />₫
                    </div>
                  </div>

                  <ul className="gx-checkout-notes gx-fs-sm gx-pb-1 gx-pb-sm-0 gx-mb-4">
                    <li>Phí vận chuyển sẽ được tính dựa vào vị trí của bạn.</li>
                    <li>Bạn cũng có thể dùng mã giảm giá trực tiếp.</li>
                  </ul>

                  <Row>
                    <Col lg={24} md={24} sm={24} xs={24}>
                    </Col>
                  </Row>

                </Card>
              </Col>

            </Row>

          </PaymentLayout>)
  )
}
export default Index