import React, {useEffect, useState} from "react";
import {NextPage} from "next";
import {Button, Card, Col, Row} from "antd";
import {useRouter} from 'next/router'
import PaymentLayout from "../../../layout/travelmart/checkout/PaymentLayout";
import Link from "next/link";

const Index: NextPage = () => {
  const router = useRouter();
  const {oid, status} = router.query;

  /**
   * Hook useEffect call at top-level, before return
   **/
  useEffect(() => {
  }, [])

  return (
      <PaymentLayout>

        <Row type="flex" justify="center" className="gx-mb-5">
          <Col xl={16} lg={16} md={16} sm={24} xs={24}>
            <Card>
              <div className="gx-mb-4">
                <div className="gx-pb-5">
                  <span className="gx-checkout-price">Trạng thái đặt hàng:</span>

                  <span className="gx-fs-xl">
                  {status === "success" ?
                      <span> Thành công, mã đơn hàng&ensp;
                        <span className="gx-link">#{oid}</span>
                        {/*  <Link href="">*/}
                        {/*  <a href="">*/}
                        {/*     #{oid}*/}
                        {/*  </a>*/}
                        {/*</Link>*/}
                      </span> :
                      <span>&ensp;Không thành công, vui lòng thử lại sau.</span>}
                </span>
                </div>

                <Button
                    onClick={() => {
                      router.push("/")
                    }}
                    className="gx-btn-secondary gx-mb-3 ant-btn-lg ant-btn-block">
                  Tiếp tục mua hàng</Button>
              </div>

            </Card>
          </Col>
        </Row>

      </PaymentLayout>
  )
}
export default Index