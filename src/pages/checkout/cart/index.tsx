import React from "react";
import {NextPage} from "next";
import {Button, Card, Col, Row} from "antd";
import MainLayout from "../../../layout/travelmart/main/MainLayout";
import ContainerHeader from "../../../component/ContainerHeader";
import ProductItemCheckout from "../../../component/ECommerce/ProductItemCheckout";
import Link from "next/link";
import {ICart} from "../../../model/state/ICart";
import {AppState} from "../../../redux";
import {CommonHelper} from "../../../helper/CommonHelper";
import NumberFormat from "react-number-format";
import {useDispatch, useSelector} from "react-redux";
import {useRouter} from "next/router";
import {CartActions} from "../../../redux/slice/cartSlice";

function getTileCheckout(vendor: string) {
  return (
      <>
        <i className="icon icon-home gx-mr-1"/>
        <Link href="#"><a><span
            className="gx-mr-1 gx-fs-lg gx-text-black">{vendor}</span></a></Link>
        <i className="icon icon-chevron-right"/>
      </>
  );
}

const Index: NextPage = () => {

  const cart: ICart = useSelector((state: AppState) => state.cart);
  const dispatch = useDispatch();
  const {stores, items, products} = cart;
  const router = useRouter();

  return (
      (cart.items.length == 0 ?
          <MainLayout>
            <div className="align-center">
              <div>
                <h2>Không có sản phẩm nào trong giỏ hàng!</h2>
              </div>
              <Link href={process.env.APP_SUB_DOMAIN + '/'}>
                <a>
                  <span style={{fontSize: "25px"}}>
                    Tiếp tục mua sắm!
                  </span>
                </a>
              </Link>
            </div>
          </MainLayout>
          :
          <MainLayout>
            <Row className="gx-mb-5">
              <Col span={24}>
                <ContainerHeader title="GIỎ HÀNG"/>
              </Col>

              {/* product image */}
              <Col xl={16} lg={16} md={16} sm={24} xs={24}>

                {
                  stores.map((store, indexStore) => (
                      <Card title={getTileCheckout(store.name)} key={indexStore}
                            className="gx-card gx-mb-3">
                        {CommonHelper.getProducts(items, products, store.id).map((product, index) => (
                            <ProductItemCheckout key={index} product={product} storeId={store.id}/>
                        ))}
                      </Card>
                  ))
                }

              </Col>

              {/* product detail */}
              <Col xl={8} lg={8} md={8} sm={24} xs={24}>
                <Row>

                  <Col lg={24} md={24} sm={24} xs={24}>
                    <Card className="gx-card">
                      <div className="ant-row-flex gx-mb-0">
                        <h2 className="gx-pb-3 gx-checkout-price">Thông tin đơn hàng</h2>
                      </div>

                      <div className="ant-row-flex gx-mb-4 gx-mb-md-4 gx-checkout-summary-price">
                        <div
                            className="gx-font-weight-normal gx-fs-xl gx-checkout-summary-price-title">
                          Tổng tiền:
                        </div>
                        <div className="gx-fs-xxxl gx-font-weight-medium gx-ml-auto gx-text-primary">
                          <NumberFormat
                              value={CommonHelper.calcTotalPrice(items, products)}
                              displayType={"text"}
                              thousandSeparator={true}
                              suffix={""}
                          />₫
                        </div>
                      </div>

                      <ul className="gx-checkout-notes gx-fs-sm gx-pb-1 gx-pb-sm-0 gx-mb-4">
                        <li>Phí vận chuyển sẽ được tính ở trang thanh toán.</li>
                        <li>Bạn cũng có thể nhập mã giảm giá ở trang thanh toán.</li>
                      </ul>

                      <Row>
                        <Col lg={24} md={24} sm={24} xs={24}>
                          <Button onClick={() => {
                            dispatch(CartActions.cleanCart());
                          }}
                                  className="gx-btn-outline-white gx-mb-3 ant-btn-lg ant-btn-block">
                            XÓA GIỎ HÀNG</Button>
                        </Col>
                      </Row>

                      <Row>
                        <Col lg={24} md={24} sm={24} xs={24}>
                          <Button onClick={() => {
                            router.push("/checkout/payment")
                          }}
                                  className="gx-btn-secondary gx-mb-3 ant-btn-lg ant-btn-block">
                            XÁC NHẬN ĐẶT HÀNG VÀ THANH TOÁN</Button>
                        </Col>
                      </Row>

                    </Card>
                  </Col>

                </Row>

              </Col>
            </Row>

          </MainLayout>)
  )
}
export default Index