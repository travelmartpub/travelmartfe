import React from "react";
import Link from "next/link";
import {IProduct} from "../../model/entity/IProduct";
import NumberFormat from "react-number-format";

interface IProps {
  product: IProduct,
  grid?: true
}

const ProductItemSimple = ({product, grid}: IProps) => {
  const {id, storeId, images, name, price} = product;
  return (
    <Link href={`/product/${storeId}/${id}`}>
      <a href={`/product/${storeId}/${id}`}>
        <div className="gx-product-item gx-product-vertical">

          <div className="gx-product-image">
            <div className="gx-grid-thumb-equal">
                <span className="gx-link gx-grid-thumb-cover">
                  <img src={images != undefined ? images.split(',')[0] :
                    "https://product.hstatic.net/200000360759/product/2290201002073_b1edb636ac6f41dca717e7e6cb8d89bb_master.jpg"}/>
                </span>
            </div>
          </div>

          <div className="gx-product-body">
            <h3 className="gx-product-title">{name}</h3>
            <h4>
              <NumberFormat
                value={price}
                displayType={"text"}
                thousandSeparator={true}
                suffix={""}
              />₫
            </h4>
          </div>

        </div>
      </a>
    </Link>
  )
};

export default ProductItemSimple;

