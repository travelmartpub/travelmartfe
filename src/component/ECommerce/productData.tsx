const productData = [
    {
        thumb: 'https://res.cloudinary.com/hilnmyskv/image/fetch/h_300,q_100,f_auto/http://www.ikea.com/us/en/images/products/legitim-chopping-board-white__0119427_PE275744_S4.JPG',
        name: 'Alarm Clock',
        variant: 'Gold ',
        mrp: '$990 ',
        price: '116,000₫',
        offer: '29 %',
        reviews: [
            {
                rating: 5,
                count: 3
            },
            {
                rating: 4,
                count: 5
            },
            {
                rating: 3,
                count: 5
            },
            {
                rating: 2,
                count: 0
            },
            {
                rating: 1,
                count: 3
            },
        ],
        rating: 5,
        description: "Horo is a home grown brand with utmost emphasis on quality goods to users... ",
    }, {
        thumb: 'https://res.cloudinary.com/hilnmyskv/image/fetch/h_300,q_100,f_auto/http://www.ikea.com/us/en/images/products/benzy-land-cushion-beige__0130826_PE285249_S4.JPG',
        name: 'Bizinto 1 Three Pin',
        variant: 'White',
        mrp: '$490 ',
        price: '316,000₫',
        offer: '1,000₫',
        reviews: [
            {
                rating: 5,
                count: 3
            },
            {
                rating: 4,
                count: 5
            },
            {
                rating: 3,
                count: 5
            },
            {
                rating: 2,
                count: 0
            },
            {
                rating: 1,
                count: 3
            },
        ],
        rating: 4,
        description: "Bizinto is an indirectly manufacture of Power strip in Delhi and supplying...",
    }, {
        thumb: 'https://res.cloudinary.com/hilnmyskv/image/fetch/h_300,q_100,f_auto/http://www.ikea.com/us/en/images/products/ikea-ps-pendant-lamp-white__0238901_PE378511_S4.JPG',
        name: 'Samons Flameless',
        variant: 'Black',
        mrp: '$49 ',
        price: '121,000₫',
        offer: '30 %',
        reviews: [
            {
                rating: 5,
                count: 3
            },
            {
                rating: 4,
                count: 5
            },
            {
                rating: 3,
                count: 5
            },
            {
                rating: 2,
                count: 0
            },
            {
                rating: 1,
                count: 3
            },
        ],
        rating: 3.3,
        description: "Now light your cigarette buds with ease by using this USB Rechargeable...",
    }, {
        thumb: 'https://res.cloudinary.com/hilnmyskv/image/fetch/h_300,q_100,f_auto/http://www.ikea.com/us/en/images/products/ikea-ps-maskros-pendant-lamp__0091262_PE226703_S4.JPG',
        name: 'Sony MDR-ZX110',
        variant: 'White',
        mrp: '$29 ',
        price: '223,000₫',
        offer: '49 %',
        reviews: [
            {
                rating: 5,
                count: 3
            },
            {
                rating: 4,
                count: 5
            },
            {
                rating: 3,
                count: 5
            },
            {
                rating: 2,
                count: 0
            },
            {
                rating: 1,
                count: 3
            },
        ],
        rating: 3.5,
        description: "Experience great sound quality with weight and foldable headphones...",
    }, {
        thumb: 'https://res.cloudinary.com/hilnmyskv/image/fetch/h_300,q_100,f_auto/http://www.ikea.com/us/en/images/products/duktig-piece-salmon-set__0135119_PE291851_S4.JPG',
        name: 'iPhone 7',
        variant: 'Black,500Htz',
        mrp: '$400 ',
        price: '235,000₫',
        offer: '49 %',
        reviews: [
            {
                rating: 5,
                count: 3
            },
            {
                rating: 4,
                count: 5
            },
            {
                rating: 3,
                count: 5
            },
            {
                rating: 2,
                count: 0
            },
            {
                rating: 1,
                count: 3
            },
        ],
        rating: 4.2,
        description: "Bluetooth speaker, Karaoke singing, Car Stereo, instrument recording etc... •",
    }, {
        thumb: 'https://res.cloudinary.com/hilnmyskv/image/fetch/h_300,q_100,f_auto/http://www.ikea.com/us/en/images/products/tarta-chokladkrokant-almond-cake-chocolate-butterscotch__0285556_PE422522_S4.JPG',
        name: 'Stonx v2.1',
        variant: 'Black',
        mrp: '$29 ',
        price: '135,000₫ ',
        offer: '49 %',
        reviews: [
            {
                rating: 5,
                count: 3
            },
            {
                rating: 4,
                count: 5
            },
            {
                rating: 3,
                count: 5
            },
            {
                rating: 2,
                count: 0
            },
            {
                rating: 1,
                count: 3
            },
        ],
        rating: 3.1,
        description: "1 Bluetooth Dongle, 1 Aux Cable, 1 Usb Cable, 1 Manual...",
    }, {
        thumb: 'https://res.cloudinary.com/hilnmyskv/image/fetch/h_300,q_100,f_auto/http://www.ikea.com/us/en/images/products/raskog-utility-cart-turquoise__0144044_PE304208_S4.JPG',
        name: 'T-Shirts',
        variant: 'White',
        mrp: '$10 ',
        price: '5,000₫ ',
        offer: '50 %',
        reviews: [
            {
                rating: 5,
                count: 3
            },
            {
                rating: 4,
                count: 5
            },
            {
                rating: 3,
                count: 5
            },
            {
                rating: 2,
                count: 0
            },
            {
                rating: 1,
                count: 3
            },
        ],
        rating: 3.1,
        description: "1 Bluetooth Dongle, 1 Aux Cable, 1 Usb Cable, 1 Manual...",
    }, {
        thumb: 'https://res.cloudinary.com/hilnmyskv/image/fetch/h_300,q_100,f_auto/http://www.ikea.com/us/en/images/products/vilshult-picture__0090396_PE224180_S4.JPG',
        name: 'Led',
        variant: 'Gold ',
        mrp: '$10 ',
        price: '533,000₫ ',
        offer: '50%',
        reviews: [
            {
                rating: 5,
                count: 3
            },
            {
                rating: 4,
                count: 5
            },
            {
                rating: 3,
                count: 5
            },
            {
                rating: 2,
                count: 0
            },
            {
                rating: 1,
                count: 3
            },
        ],
        rating: 5,
        description: "Horo is a home grown brand with emphasis on quality goods to our users... ",
    }, {
        thumb: 'https://res.cloudinary.com/hilnmyskv/image/fetch/h_300,q_100,f_auto/http://www.ikea.com/us/en/images/products/stefan-chair-black__0122106_PE278491_S4.JPG',
        name: 'Football Check',
        variant: 'Black',
        mrp: '$490 ',
        price: '512,000₫ ',
        offer: '29 %',
        reviews: [
            {
                rating: 5,
                count: 3
            },
            {
                rating: 4,
                count: 5
            },
            {
                rating: 3,
                count: 5
            },
            {
                rating: 2,
                count: 0
            },
            {
                rating: 1,
                count: 3
            },
        ],
        rating: 4,
        description: "Bizinto is an indirectly manufacture of Power strip in Delhi and supplying in all over india...",
    }, {
        thumb: 'https://res.cloudinary.com/hilnmyskv/image/fetch/h_300,q_100,f_auto/http://www.ikea.com/us/en/images/products/groland-kitchen-island__23030_PE107804_S4.JPG',
        name: 'Watch House',
        variant: 'Black',
        mrp: '$49 ',
        price: '552,000₫ ',
        offer: '30 %',
        reviews: [
            {
                rating: 5,
                count: 3
            },
            {
                rating: 4,
                count: 5
            },
            {
                rating: 3,
                count: 5
            },
            {
                rating: 2,
                count: 0
            },
            {
                rating: 1,
                count: 3
            },
        ],
        rating: 3.3,
        description: "Now light your cigarette buds with ease by using this USB Rechargeable Electronic Flameless Lighter.",
    }, {
        thumb: 'https://res.cloudinary.com/hilnmyskv/image/fetch/h_300,q_100,f_auto/http://www.ikea.com/us/en/images/products/bekvam-step-stool__0108612_PE258294_S4.JPG',
        name: 'Fan Electrics',
        variant: 'White,full speed',
        mrp: '$29 ',
        price: '599,000₫  ',
        offer: '49 %',
        reviews: [
            {
                rating: 5,
                count: 3
            },
            {
                rating: 4,
                count: 5
            },
            {
                rating: 3,
                count: 5
            },
            {
                rating: 2,
                count: 0
            },
            {
                rating: 1,
                count: 3
            },
        ],
        rating: 3.5,
        description: "Experience great sound quality with these light weight and fordable headphones.",
    }, {
        thumb: 'https://res.cloudinary.com/hilnmyskv/image/fetch/h_300,q_100,f_auto/http://www.ikea.com/us/en/images/products/lack-tv-unit__59637_PE165525_S4.JPG',
        name: 'Padraig Q7 Handheld',
        variant: 'Black,500Htz',
        mrp: '$56 ',
        price: '53,000₫  ',
        offer: '49 %',
        reviews: [
            {
                rating: 5,
                count: 3
            },
            {
                rating: 4,
                count: 5
            },
            {
                rating: 3,
                count: 5
            },
            {
                rating: 2,
                count: 0
            },
            {
                rating: 1,
                count: 3
            },
        ],
        rating: 4.2,
        description: "Bluetooth speaker, Karaoke singing, Car Stereo, instrument recording, interviews, podcasting, etc. •",
    }
];

export default productData;
