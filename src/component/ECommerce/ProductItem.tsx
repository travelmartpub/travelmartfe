import React from "react";
import {Button} from "antd";
import StarRatingComponent from "react-star-rating-component";
import Link from "next/link";

interface IProduct {
    thumb: string,
    name: string,
    price: string,
    mrp: string,
    offer: string,
    variant: string,
    rating: number,
    description: string
}

interface IProps {
    product: IProduct,
    grid?: true
}

const ProductItem = ({product, grid}: IProps) => {
    const {thumb, name, price, mrp, offer, variant, rating, description} = product;
    return (
        <div className={`gx-product-item  ${grid ? 'gx-product-vertical' : 'gx-product-horizontal'}`}>
          <Link href="/product">
            <a href="/product">

          <div className="gx-product-image">
                <div className="gx-grid-thumb-equal">
          <span className="gx-link gx-grid-thumb-cover">
            <img alt="Remy Sharp" src={thumb}/>
          </span>
                </div>
            </div>
            </a>
          </Link>

            <div className="gx-product-body">
      <Link href="/product">
        <a href="/product">

                <h3 className="gx-product-title">{name}
                    <small className="gx-text-grey">{", " + variant}</small>
                </h3>
                <div className="ant-row-flex">
                    <h4>{price} </h4>
                    <h5 className="gx-text-muted gx-px-2">
                        <del>{mrp}</del>
                    </h5>
                    <h5 className="gx-text-success">{offer} off</h5>
                </div>

        </a>
      </Link>

                <div className="ant-row-flex gx-mb-1">
                    <StarRatingComponent
                        name=""
                        value={rating}
                        starCount={5}
                        editing={false}/>
                    <strong className="gx-d-inline-block gx-ml-2">{rating}</strong>
                </div>
                <p>{description}</p>
            </div>

            <div className="gx-product-footer">
                <Button>
                    Add To Cart
                </Button>

                <Button type="primary">
                    Read More
                </Button>
            </div>
        </div>
    )
};

export default ProductItem;

