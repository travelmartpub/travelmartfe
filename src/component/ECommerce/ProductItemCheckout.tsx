import React from "react";
import {InputNumber} from "antd";
import {IProduct} from "../../model/entity/IProduct";
import {useDispatch} from "react-redux";
import {CartActions} from "../../redux/slice/cartSlice";
import {ICartItem} from "../../model/state/ICart";
import NumberFormat from "react-number-format";

interface IProps {
  storeId: number,
  product: IProduct,
}

const ProductItemCheckout = ({product, storeId}: IProps) => {
  const dispatch = useDispatch();

  const updateQuantity = async (value: number) => {
    const item: ICartItem = {
      productId: product.id,
      quantity: value,
      storeId: storeId
    }
    await dispatch(CartActions.updateQuantity(item));
  }

  const {images, name, price, quantity} = product;
  return (
      <div className='gx-user-list gx-card-strip gx-card-checkout'>
        <img
            alt='avatar'
            src={images !== undefined ? images.split(",")[0] : "https://product.hstatic.net/200000360759/product/2290201002073_b1edb636ac6f41dca717e7e6cb8d89bb_master.jpg"}
            className="gx-avatar-img gx-avatar-img-lg gx-border-0"
        />
        <div className="gx-description gx-checkout-description">
          <h3>{name}</h3>
          <p className="gx-mb-2">
            <NumberFormat
                value={price}
                displayType={"text"}
                thousandSeparator={true}
                suffix={""}
            />₫
          </p>

        </div>

        <div className="gx-card-list-footer gx-text-right">
          <p className="gx-mb-3 gx-ml-auto gx-font-weight-bold gx-fs-lg">
            <NumberFormat
                value={price * quantity}
                displayType={"text"}
                thousandSeparator={true}
                suffix={""}
            />₫
          </p>
          <InputNumber size="default" className="gx-mb-1" min={1}
                       defaultValue={1}
                       value={quantity}
                       onChange={updateQuantity}
          />
        </div>

      </div>
  )
};

export default ProductItemCheckout;

