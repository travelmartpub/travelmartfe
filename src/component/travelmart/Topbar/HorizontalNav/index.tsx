import React from "react";
import {Menu} from "antd";
import {useRouter} from "next/router";

const SubMenu = Menu.SubMenu;

const HorizontalNav = () => {
  const router = useRouter();

  return (

      <Menu
          // defaultOpenKeys={[defaultOpenKeys]}
          // selectedKeys={[selectedKeys]}
          mode="horizontal">

        <SubMenu className="gx-menu-horizontal"
                 key="home"
                 title="Trang chủ"
                 onTitleClick={async () => await router.push('/')}>
        </SubMenu>

        <SubMenu className="gx-menu-horizontal"
                 key="main7"
                 title="Ẩm thực">
        </SubMenu>

        <SubMenu className="gx-menu-horizontal"
                 key="in-built-apps6"
                 title="Quà lưu niệm">
        </SubMenu>

        <SubMenu className="gx-menu-horizontal"
                 key="components5"
                 title="Thời trang">
        </SubMenu>

        <SubMenu className="gx-menu-horizontal"
                 key="extraComponents4"
                 title="Hải sản">
        </SubMenu>

        <SubMenu className="gx-menu-horizontal"
                 key="extraComponents3"
                 title="Nông sản">
        </SubMenu>

        <SubMenu className="gx-menu-horizontal"
                 key="extraComponents2"
                 title="Bánh kẹo">
        </SubMenu>

      </Menu>
  );
};

export default HorizontalNav;