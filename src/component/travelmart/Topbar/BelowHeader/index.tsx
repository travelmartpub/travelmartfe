import React from "react";
import {Button, Dropdown, Icon, Layout, Menu, Popover, Select} from "antd";
import {useSelector} from "react-redux";

import {AppState} from "../../../../redux";
import SearchBox from "../../../SearchBox";
import HorizontalNav from "../HorizontalNav";
import Cart from "../../../Cart";
import {ICart} from "../../../../model/state/ICart";
import Link from "next/link";
import UserInfo from "../../../UserInfo";
import {IUser} from "../../../../model/entity/IUser";

const {Header} = Layout;

const Option = Select.Option;

const menu = (
    <Menu
        // onClick={handleMenuClick}
    >
      <Menu.Item key="1">Products</Menu.Item>
      <Menu.Item key="2">Apps</Menu.Item>
      <Menu.Item key="3">Blogs</Menu.Item>
    </Menu>
);

const BelowHeader = () => {

  const cart: ICart = useSelector((state: AppState) => state.cart);
  const {user}: { user: IUser } = useSelector((state: AppState) => state.me);

  return (
      <div className="gx-header-horizontal gx-header-horizontal-dark gx-below-header-horizontal">
        <div className="gx-header-horizontal-top">
          <div className="gx-container">
            <div className="gx-header-horizontal-top-flex">
              {/*<div className="gx-header-horizontal-top-left">*/}
              {/*  <i className="icon icon-alert gx-mr-3"/>*/}
              {/*  <p className="gx-mb-0 gx-text-truncate">*/}
              {/*    /!*<IntlMessages id="app.announced"/>*!/*/}
              {/*  </p>*/}
              {/*</div>*/}
              {(user === undefined || user.id == 0) &&
                  <ul className="gx-login-list">
                    <li>
                      <Link href={process.env.APP_SUB_DOMAIN + '/signin'}>Đăng nhập</Link>
                    </li>
                    <li>
                      <Link href={process.env.APP_SUB_DOMAIN + '/signup'}>Đăng ký</Link>
                    </li>
                  </ul>}
            </div>
          </div>
        </div>


        <Header
            className="gx-header-horizontal-main">
          <div className="gx-container">
            <div className="gx-header-horizontal-main-flex">

              <div className="gx-d-block gx-d-lg-none gx-linebar gx-mr-xs-3">
                <i className="gx-icon-btn icon icon-menu"
                    // onClick={() => {
                    //   this.props.toggleCollapsedSideNav(!navCollapsed);
                    // }}
                />
              </div>
              {/*<Link to="/"*/}
              {/*      className="gx-d-block gx-d-lg-none gx-pointer gx-mr-xs-3 gx-pt-xs-1 gx-w-logo">*/}
              {/*<img alt="" src={require("assets/images/w-logo.png")}/>*/}
              {/*<img src={process.env.APP_SUB_DOMAIN + '/assets/images/w-logo.png'} alt="loader"/>*/}

              {/*</Link>*/}
              {/*<Link to="/" className="gx-d-none gx-d-lg-block gx-pointer gx-mr-xs-5 gx-logo">*/}
              {/*<img alt="" src={require("assets/images/logo.png")}/>*/}
              <Link href="/">
                <a href="/">
                  <img
                      src="https://firebasestorage.googleapis.com/v0/b/halogram-9c314.appspot.com/o/TravelMart-logos_white.png?alt=media&token=7d74f72c-0c5b-4b40-ab56-ed4931c0d723"
                      alt="loader"/>
                </a>
              </Link>
              {/*</Link>*/}
              <div className="gx-header-search gx-d-none gx-d-lg-flex">
                <SearchBox styleName="gx-lt-icon-search-bar-lg"
                           placeholder="Tìm kiếm..."
                           onChange={undefined}
                           value={undefined}
                    // onChange={this.updateSearchChatUser.bind(this)}
                    // value={this.state.searchText}
                />

                <Select defaultValue="products" style={{width: 120}}
                    // onChange={handleChange}
                >
                  <Option value="products">Sản phẩm</Option>
                  <Option value="stores">Cửa hàng</Option>
                  <Option value="locations">Địa điểm</Option>
                </Select>
              </div>

              <ul className="gx-header-notifications gx-ml-auto">
                <li className="gx-notify gx-notify-search gx-d-inline-block gx-d-lg-none">
                  <Popover overlayClassName="gx-popover-horizantal" placement="bottomRight"
                           content={
                             <div className="gx-d-flex"><Dropdown overlay={menu}>
                               <Button>
                                 Category <Icon type="down"/>
                               </Button>
                             </Dropdown>
                               <SearchBox styleName="gx-popover-search-bar"
                                          placeholder="Search in app..." onChange={undefined}
                                          value={undefined}
                                   // onChange={this.updateSearchChatUser.bind(this)}
                                   // value={this.state.searchText}
                               /></div>
                           } trigger="click">
                    <span className="gx-pointer gx-d-block"><i
                        className="icon icon-search-new"/></span>
                  </Popover>
                </li>

                {/*<li className="gx-notify">*/}
                {/*  <Popover overlayClassName="gx-popover-horizantal" placement="bottomRight"*/}
                {/*           content={<AppNotification/>}*/}
                {/*           trigger="click">*/}
                {/*    <span className="gx-pointer gx-d-block"><i className="icon icon-notification"/></span>*/}
                {/*  </Popover>*/}
                {/*</li>*/}

                {/*<li className="gx-msg">*/}
                {/*  <Popover overlayClassName="gx-popover-horizantal" placement="bottomRight"*/}
                {/*           content={<MailNotification/>} trigger="click">*/}
                {/*<span className="gx-pointer gx-status-pos gx-d-block">*/}
                {/*<i className="icon icon-chat-new"/>*/}
                {/*<span className="gx-status gx-status-rtl gx-small gx-orange"/>*/}
                {/*</span>*/}
                {/*  </Popover>*/}
                {/*</li>*/}
                {/*<li className="gx-language">*/}
                {/*  <Popover overlayClassName="gx-popover-horizantal" placement="bottomRight"*/}
                {/*           trigger="click">*/}
                {/*    <span className="gx-pointer gx-flex-row gx-align-items-center"><i*/}
                {/*        className={`flag flag-24 flag-${locale.icon}`}/>*/}
                {/*    </span>*/}
                {/*  </Popover>*/}
                {/*</li>*/}
                {/*<li className="gx-user-nav"><UserInfo/></li>*/}
                <li className="gx-notify">
                  <Cart cart={cart}/>
                </li>
                {(user !== undefined && user.id != 0) &&
                    <li><UserInfo/></li>}
              </ul>
            </div>
          </div>
        </Header>

        {/* Menu */}

        <div
            className="gx-header-horizontal-nav gx-header-horizontal-nav-curve gx-d-none gx-d-lg-block">
          <div className="gx-container">
            <div className="gx-header-horizontal-nav-flex">
              <HorizontalNav/>
              {/*<ul className="gx-header-notifications gx-ml-auto">*/}
              {/*  <li><span className="gx-pointer gx-d-block"><i*/}
              {/*      className="icon icon-menu-lines"/></span></li>*/}
              {/*  <li><span className="gx-pointer gx-d-block"><i*/}
              {/*      className="icon icon-setting"/></span></li>*/}
              {/*  <li><span className="gx-pointer gx-d-block"><i*/}
              {/*      className="icon icon-apps-new"/></span></li>*/}
              {/*</ul>*/}
            </div>
          </div>
        </div>
      </div>
  );
};

export default BelowHeader;