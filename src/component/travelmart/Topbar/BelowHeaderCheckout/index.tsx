import React from "react";
import {Layout, Menu, Select} from "antd";
import {useSelector} from "react-redux";

import {AppState} from "../../../../redux";
import {ICart} from "../../../../model/state/ICart";
import Link from "next/link";

const {Header} = Layout;

const BelowHeaderCheckout = () => {

  return (
      <div className="gx-header-horizontal gx-header-horizontal-dark gx-below-header-horizontal">

        <Header
            className="gx-header-horizontal-main">
          <div className="gx-container">
            <div className="gx-header-horizontal-main-flex">

              <Link href="/">
                <a href="/">
                  <img src={process.env.APP_SUB_DOMAIN + '/assets/images/logo.png'} alt="loader"/>
                </a>
              </Link>

            </div>
          </div>
        </Header>


      </div>
  );
};

export default BelowHeaderCheckout;