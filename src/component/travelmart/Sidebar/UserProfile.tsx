import {Avatar, Popover} from "antd";
import {useRouter} from "next/router";
import {authActions} from "../../../redux/slice/authSlice";
import {useDispatch} from 'react-redux';

const UserProfile = () => {

  const dispatch = useDispatch();
  const router = useRouter();

  const handleLogout = async () => {
    await router.push(process.env.APP_SUB_DOMAIN + '/');
    await dispatch(authActions.logout(async () => {
      return
    }));
  };

  // console.log("authUser", authUser);

  // JUST FOR MOCK: authUser,
  // TODO: get authentication from redux slice

  const authUser = {
    name: "travelmart user"
  };

  const userSignOut = () => {
    handleLogout().then()
  };

  const userMenuOptions = (
      <ul className="gx-user-popover">
        <li>My Account</li>
        <li>Connections</li>
        <li onClick={() => userSignOut()}>Logout
        </li>
      </ul>
  );

  return (

      <div className="gx-flex-row gx-align-items-center gx-mb-4 gx-avatar-row">
        <Popover placement="bottomRight" content={userMenuOptions} trigger="click">
          <Avatar src='https://via.placeholder.com/150x150'
                  className="gx-size-40 gx-pointer gx-mr-3" alt=""/>
          <span className="gx-avatar-name">{authUser ? authUser.name : "Loading"}<i
              className="icon icon-chevron-down gx-fs-xxs gx-ml-2"/></span>
        </Popover>
      </div>

  )
};

export default UserProfile;