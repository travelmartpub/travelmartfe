import {AppState} from "../../../redux";
import {useSelector} from "react-redux";
import CustomScrollbars from "./CustomScrollbars";
import {Menu} from "antd";
import {
  NAV_STYLE_NO_HEADER_EXPANDED_SIDEBAR,
  NAV_STYLE_NO_HEADER_MINI_SIDEBAR,
  THEME_TYPE_LITE
} from "../../../constant/ThemeSetting";
import Link from "next/link";
import UserProfile from "./UserProfile";
import SidebarLogo from "./SidebarLogo";
import React from "react";
import AppsNavigation from "./AppsNavigation";

const MenuItemGroup = Menu.ItemGroup;

const SidebarContent = () => {

  const {navStyle, themeType, pathname} = useSelector((state: AppState) => state.settings);

  const selectedKeys = pathname.substr(1);
  const defaultOpenKeys = selectedKeys.split('/')[1];

  const getNoHeaderClass = (navStyle: string): string => {
    if (navStyle === NAV_STYLE_NO_HEADER_MINI_SIDEBAR || navStyle === NAV_STYLE_NO_HEADER_EXPANDED_SIDEBAR) {
      return "gx-no-header-notifications";
    }
    return "";
  };

  const getNavStyleSubMenuClass = (navStyle: string): string => {
    if (navStyle === NAV_STYLE_NO_HEADER_MINI_SIDEBAR) {
      return "gx-no-header-submenu-popup";
    }
    return "";
  };

  return (
      <React.Fragment>

        <SidebarLogo/>
        <div className="gx-sidebar-content">
          <div className={`gx-sidebar-notifications ${getNoHeaderClass(navStyle)}`}>
            <UserProfile/>
            <AppsNavigation/>
          </div>
          {/*<CustomScrollbars className="gx-layout-sider-scrollbar">*/}
          <Menu
              defaultOpenKeys={[defaultOpenKeys]}
              selectedKeys={[selectedKeys]}
              theme={themeType === THEME_TYPE_LITE ? 'light' : 'dark'}
              mode="inline">

            {/*<Menu.Item key="sample">*/}
            {/*  <Link to="/sample"><i className="icon icon-widgets"/>*/}
            {/*    <IntlMessages id="sidebar.samplePage"/></Link>*/}
            {/*</Menu.Item>*/}

            <MenuItemGroup key="in-built-apps"
                           className="gx-menu-group"
                           title={
                             /*<IntlMessages id="sidebar.execution"/>*/
                             "Main"
                           }>
              <Menu.Item key="service">
                <Link href="/service">
                  <a href="/service">
                    <i className="icon icon-email"/>
                    {/*<IntlMessages id="sidebar.runBenchmark"/>*/}
                    Services
                  </a>
                </Link>
              </Menu.Item>

              <Menu.Item key="product">
                <Link href="/product">
                  <a href="/product">
                    <i className="icon icon-widgets"/>
                    {/*<IntlMessages id="sidebar.samplePage"/>*/}
                    Products
                  </a>
                </Link>
              </Menu.Item>

              <Menu.Item key="benchmark">
                <Link href="/order">
                  <a href="/order">
                    <i className="icon icon-shopping-cart"/>
                    {/*<IntlMessages id="sidebar.runBenchmark"/>*/}
                    Orders
                  </a>
                </Link>
              </Menu.Item>

              {/*<Menu.Item key="reports">*/}
              {/*  <Link href="/reports">*/}
              {/*    <a href="/reports">*/}
              {/*      <i className="icon icon-all-contacts"/>*/}
              {/*      /!*<IntlMessages id="sidebar.viewReport"/>*!/*/}
              {/*      Reports*/}
              {/*    </a>*/}
              {/*  </Link>*/}
              {/*</Menu.Item>*/}

              {/*<Menu.Item key="benchmark">*/}
              {/*  <Link href="/benchmark">*/}
              {/*    <a href="/benchmark">*/}
              {/*      <i className="icon icon-email"/>*/}
              {/*      /!*<IntlMessages id="sidebar.runBenchmark"/>*!/*/}
              {/*      Benchmark*/}
              {/*    </a>*/}
              {/*  </Link>*/}
              {/*</Menu.Item>*/}

              {/*<Menu.Item key="category-item">*/}
              {/*  <Link href="/category-item">*/}
              {/*    <a href="/category-item">*/}
              {/*      <i className="icon icon-email"/>*/}
              {/*      /!*<IntlMessages id="sidebar.runBenchmark"/>*!/*/}
              {/*      Category*/}
              {/*    </a>*/}
              {/*  </Link>*/}
              {/*</Menu.Item>*/}

              {/*<Menu.Item key="sample">*/}
              {/*  <Link href="/item">*/}
              {/*    <a href="/item">*/}
              {/*      <i className="icon icon-widgets"/>*/}
              {/*      /!*<IntlMessages id="sidebar.samplePage"/>*!/*/}
              {/*      Item*/}
              {/*    </a>*/}
              {/*  </Link>*/}
              {/*</Menu.Item>*/}

              {/*<Menu.Item key="benchmark">*/}
              {/*  <Link href="/order">*/}
              {/*    <a href="/order">*/}
              {/*      <i className="icon icon-shopping-cart"/>*/}
              {/*      /!*<IntlMessages id="sidebar.runBenchmark"/>*!/*/}
              {/*      Order*/}
              {/*    </a>*/}
              {/*  </Link>*/}
              {/*</Menu.Item>*/}

              {/*<Menu.Item key="in-built-apps/contacts">*/}
              {/*  <Link to="/in-built-apps/contacts"><i className="icon icon-contacts"/><IntlMessages*/}
              {/*      id="sidebar.contactsApp"/></Link>*/}
              {/*</Menu.Item>*/}

              {/*<Menu.Item key="in-built-apps/chat">*/}
              {/*  <Link to="/in-built-apps/chat"><i*/}
              {/*      className="icon icon-chat-bubble -flex-column-reverse"/><IntlMessages*/}
              {/*      id="sidebar.chatApp"/></Link>*/}
              {/*</Menu.Item>*/}

              {/*<Menu.Item key="main/notes">*/}
              {/*  <Link to="/in-built-apps/notes"><i className="icon icon-copy"/>*/}
              {/*    <IntlMessages id="sidebar.notes"/></Link>*/}
              {/*</Menu.Item>*/}

              {/*<Menu.Item key="main/algolia">*/}
              {/*  <Link to="/main/algolia"><i className="icon icon-shopping-cart "/>*/}
              {/*    <IntlMessages id="sidebar.algolia"/></Link>*/}
              {/*</Menu.Item>*/}

              {/*<Menu.Item key="in-built-apps/firebase-crud">*/}
              {/*  <Link to="/in-built-apps/firebase-crud"><i*/}
              {/*      className="icon icon-icon"/><IntlMessages*/}
              {/*      id="sidebar.crud"/></Link>*/}
              {/*</Menu.Item>*/}
            </MenuItemGroup>

          </Menu>
          {/*</CustomScrollbars>*/}
        </div>

      </React.Fragment>
  );
};

export default SidebarContent;