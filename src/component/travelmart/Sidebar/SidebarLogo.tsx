import {AppState} from "../../../redux";
import {useSelector} from "react-redux";
import {
  NAV_STYLE_DRAWER,
  NAV_STYLE_FIXED,
  NAV_STYLE_MINI_SIDEBAR, NAV_STYLE_NO_HEADER_MINI_SIDEBAR,
  TAB_SIZE, THEME_TYPE_LITE
} from "../../../constant/ThemeSetting";
import Link from "next/link";

const SidebarLogo = () => {

  const {navStyle, themeType, width} = useSelector((state: AppState) => state.settings);

  if (width < TAB_SIZE && navStyle === NAV_STYLE_FIXED) {
    // navStyle = NAV_STYLE_DRAWER;  // TODO: redux
  }

  return (
      <div className="gx-layout-sider-header">

        {(navStyle === NAV_STYLE_FIXED || navStyle === NAV_STYLE_MINI_SIDEBAR) ?
            <div className="gx-linebar">

              <i className={`gx-icon-btn icon icon-${navStyle === NAV_STYLE_MINI_SIDEBAR ? 'menu-unfold' : 'menu-fold'}
                   ${themeType !== THEME_TYPE_LITE ? 'gx-text-white' : ''}`}

                 onClick={() => {
                   if (navStyle === NAV_STYLE_DRAWER) {
                     // this.props.toggleCollapsedSideNav(!navCollapsed); // TODO: redux
                   } else if (navStyle === NAV_STYLE_FIXED) {
                     // this.props.onNavStyleChange(NAV_STYLE_MINI_SIDEBAR)  // TODO: redux
                   } else if (navStyle === NAV_STYLE_NO_HEADER_MINI_SIDEBAR) {
                     // this.props.toggleCollapsedSideNav(!navCollapsed);  // TODO: redux
                   } else {
                     // this.props.onNavStyleChange(NAV_STYLE_FIXED) //  // TODO: redux
                   }
                 }}
              />
            </div> : null}

        <Link href={"/"}>
          <a href="/" className="gx-site-logo">
            {/*{navStyle === NAV_STYLE_NO_HEADER_MINI_SIDEBAR && width >= TAB_SIZE ?*/}
            {/*  <img alt="" src={require("assets/images/w-logo.png")}/> :*/}
            {/*  themeType === THEME_TYPE_LITE ?*/}
            {/*    <img alt="" src={require("assets/images/logo-white.png")}/> :*/}
            {/*    <img alt="" src={require("assets/images/logo.png")}/>}*/}

            <span style={{
              color: '#006eb8',
              marginLeft: 10
            }}>
              <b>TRAVELMART BETA</b>
            </span>
          </a>
        </Link>

      </div>
  );
};

export default SidebarLogo;