import React, {useEffect} from "react";
import {message} from 'antd';
import {useSelector} from "react-redux";
import {AppState} from "../../redux";
import CircularProgress from "../CircularProgress";

interface IProps {
}

const InfoView: React.FC<IProps> = (props: IProps) => {

  const { isLoading } = useSelector((state: AppState) => state.common);
  const common = useSelector((state: AppState) => state.common);

  useEffect(() => {
    const {error} = common;
    const displayMessage = common.message;

    handleMessage(displayMessage, error);
  }, [common]);


  /**
   * Display message when requiring from user
   * @param displayMessage: message info
   * @param error: message error
   */
  const handleMessage = (displayMessage: string, error: string) => {
    if (displayMessage) {
      message.info(<span id="message-id">{displayMessage}</span>);
    }

    if (error) {
      message.error(<span id="message-id">{error}</span>);
    }
  };

  return (
      <>
        {
          isLoading &&
          <div className="gx-loader-view gx-loader-position">
            <CircularProgress/>
            {/*<CircularProgress className="gx-loader-400"/>*/}
          </div>
        }
      </>
  );
};

export default InfoView;
