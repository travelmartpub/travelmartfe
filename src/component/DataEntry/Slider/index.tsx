import {Col, InputNumber, Row, Slider} from "antd";

interface IMarks {
  [index: number]: number
}

interface IProps {
  marks: IMarks,
  onChange: (value: number | undefined) => void,
  inputValue: number,
  min: number,
  max: number
}

const SliderWithNumberIndex = (props: IProps) => {

  const {marks, onChange, inputValue, min, max} = props;

  return (
      <Row>
        <Col sm={18} xs={24}>
          <Slider min={min} max={max}
                  onChange={onChange}
                  value={inputValue}
                  marks={marks}
                  defaultValue={64}/>
        </Col>

        <Col sm={6} xs={24}>
          <InputNumber
              min={min}
              max={max}
              value={inputValue}
              onChange={onChange}
          />
        </Col>
      </Row>
  );
};

export default SliderWithNumberIndex;
