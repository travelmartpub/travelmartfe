import {Cascader} from "antd";
import {CascaderOptionType} from "antd/lib/cascader";

const filter = (inputValue, path) => {
  return (path.some(option => (option.label).toLowerCase().indexOf(inputValue.toLowerCase()) > -1));
};

interface IProps {
  options: CascaderOptionType[],
  onChange: (value: string[], selectedOptions?: CascaderOptionType[]) => void
}

const CascaderIndex = (props: IProps) => {
  const {options, onChange} = props;

  return (
      <Cascader
          options={options}
          onChange={onChange}
          placeholder="Please select"
          showSearch={{filter}}
      />
  );
};

export default CascaderIndex;
