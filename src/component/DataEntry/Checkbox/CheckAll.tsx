import React, {useState} from "react";
import {Card, Checkbox} from "antd";
import {CheckboxChangeEvent} from "antd/lib/checkbox";

const CheckboxGroup = Checkbox.Group;

const plainOptions = ['Apple', 'Pear', 'Orange'];
const defaultCheckedList = ['Apple', 'Orange'];

interface IProps {
  plainOptions: Array<string>,
  defaultCheckedList: Array<string>
}

const CheckAll = (props: IProps) => {

  const {defaultCheckedList, plainOptions} = props;

  const [checkedList, setCheckedList] = useState<Array<string>>(defaultCheckedList);
  const [indeterminate, setIndeterminate] = useState<boolean>(true);
  const [checkAll, setCheckAll] = useState<boolean>(false);

  const onChange = (checkedValue: Array<string>): void => {
    setCheckedList(checkedValue);
    setIndeterminate(!!checkedValue.length && (checkedValue.length < plainOptions.length));
    setCheckAll(checkedValue.length === plainOptions.length);
  };

  const onCheckAllChange = (e: CheckboxChangeEvent) => {
    setCheckedList(e.target.checked ? plainOptions : []);
    setIndeterminate(false);
    setCheckAll(e.target.checked);
  };

  return (
      <Card className="gx-card" title="CheckAll">
        <div className="gx-border-bottom gx-pb-3 gx-mb-3">
          <Checkbox
              indeterminate={indeterminate}
              onChange={onCheckAllChange}
              checked={checkAll}
          >
            Check all
          </Checkbox>
        </div>
        <CheckboxGroup options={plainOptions}
                       value={checkedList}
                       onChange={onChange}/>
      </Card>
  );
};

export default CheckAll;







