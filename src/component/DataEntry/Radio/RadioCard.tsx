import React, {useState} from "react";

interface IProps {
  checked: boolean,
  text: string,
  image?: string
}

const RadioCard = (props: IProps) => {
      const {checked, text, image} = props;

      return (
          <div title="CheckAll">
            <span className={`radio-fake ${checked ? 'radio-fake-checked' : ''}`}/>
            {
              image ? (
                  <img className="gx-mr-3" src={image} alt="loader"/>
              ) : ('')
            }
            <span>{text}</span>
          </div>
      );
    }
;

export default RadioCard;







