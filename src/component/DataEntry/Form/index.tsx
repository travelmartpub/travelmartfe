import React, { Component, FormEvent, FormEventHandler, Props } from "react";
import {Button, Card, Form, Input, Select, Modal} from "antd";

const FormItem = Form.Item;
const TextArea = Input.TextArea;

interface IProps{
  form,
  onSubmit: FormEventHandler,
  name: string
}

export const ServiceForm = ( (props: IProps) => {
    const {getFieldDecorator} = props.form;
    if ( props.name === "frmInsert" ) {
      return (
        <Form name={props.name} onSubmit={props.onSubmit}>
          <FormItem 
                label="ID"
                labelCol={{xs: 24, sm: 5}}
                wrapperCol={{xs: 24, sm: 12}}
                style={{ display: 'none' }}
          >
            {getFieldDecorator('id', {
                  rules: [{required: false}],
                })(
                  <Input />
            )}
          </FormItem>
          <FormItem
            label="Service Name"
            labelCol={{xs: 24, sm: 5}}
            wrapperCol={{xs: 24, sm: 12}}
          >
            {getFieldDecorator('name' , {
              rules: [{required: true, message: 'Please input your service name!'}],
            })(
              <Input />
            )}
          </FormItem>
          <FormItem
            label="Description"
            labelCol={{xs: 24, sm: 5}}
            wrapperCol={{xs: 24, sm: 12}}
          >
            {getFieldDecorator('description', {
              getValueProps: "",
              rules: [{required: false}],
            })(
              <TextArea />
            )}
          </FormItem>
          <FormItem
            wrapperCol={{xs: 24, sm: {span: 12, offset: 5}}}
          >
          </FormItem>
          <FormItem
            wrapperCol={{xs: 24, sm: {span: 12, offset: 5}}}
          >
            <Button type="primary" htmlType="submit">
              Submit
            </Button>
          </FormItem>
        </Form>
      ); 
    } else {
      return (
        <Form name={props.name} onSubmit={props.onSubmit}>
        <FormItem 
            label="ID"
            labelCol={{xs: 24, sm: 5}}
            wrapperCol={{xs: 24, sm: 12}}
          >
            {getFieldDecorator('id', {
              rules: [{required: false}],
            })(
              <Input />
            )}
      </FormItem>
        <FormItem
          label="Service Name"
          labelCol={{xs: 24, sm: 5}}
          wrapperCol={{xs: 24, sm: 12}}
        >
          {getFieldDecorator('name' , {
            rules: [{required: true, message: 'Please input your service name!'}],
          })(
            <Input />
          )}
        </FormItem>
        <FormItem
          label="Description"
          labelCol={{xs: 24, sm: 5}}
          wrapperCol={{xs: 24, sm: 12}}
        >
          {getFieldDecorator('description', {
            getValueProps: "",
            rules: [{required: false}],
          })(
            <TextArea />
          )}
        </FormItem>
        <FormItem
          wrapperCol={{xs: 24, sm: {span: 12, offset: 5}}}
        >
        </FormItem>
      </Form>
      ) ;
    }
})