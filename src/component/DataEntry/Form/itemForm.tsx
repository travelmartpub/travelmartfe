import React, { FormEventHandler } from "react";
import {Button, Cascader, Form, Input, Select, Modal} from "antd";

const FormItem = Form.Item;
const TextArea = Input.TextArea;

interface IProps{
  form,
  onSubmit: FormEventHandler,
  name: string,
  option
}

export const ItemForm = ( (props: IProps) => {
    const {getFieldDecorator} = props.form;
    if ( props.name === "frmInsert" ) {
      return (
        <Form name={props.name} onSubmit={props.onSubmit} >
          <FormItem 
                label="ID"
                labelCol={{xs: 24, sm: 5}}
                wrapperCol={{xs: 24, sm: 12}}
                style={{ display: 'none' }}
          >
            {getFieldDecorator('id', {
                  rules: [{required: false}],
                })(
                  <Input />
            )}
          </FormItem>
          <FormItem
            label="Item Name"
            labelCol={{xs: 24, sm: 5}}
            wrapperCol={{xs: 24, sm: 12}}
          >
            {getFieldDecorator('name' , {
              rules: [{required: true, message: 'Please input your item name!'}],
            })(
              <Input />
            )}
          </FormItem>
          <FormItem
            label="Description"
            labelCol={{xs: 24, sm: 5}}
            wrapperCol={{xs: 24, sm: 12}}
          >
            {getFieldDecorator('description', {
              rules: [{required: false}],
            })(
              <TextArea />
            )}
          </FormItem>
          <FormItem
            label="Price"
            labelCol={{xs: 24, sm: 5}}
            wrapperCol={{xs: 24, sm: 12}}
          >
            {getFieldDecorator('price', {
              rules: [{required: false}],
            })(
              <Input />
            )}
          </FormItem>
          <FormItem
            wrapperCol={{xs: 24, sm: {span: 12, offset: 5}}}
          >
          </FormItem>
          <FormItem
          label="Category Name"
          labelCol={{xs: 24, sm: 5}}
          wrapperCol={{xs: 24, sm: 12}}
        >
          {getFieldDecorator('categoryID' , {
            initialValue:[{ value: 2, label: "category" }],
            rules: [{required: true, message: 'Please input your Item name!'}],
          })(
            <Cascader options={props.option} placeholder="Please select" />
          )}
        </FormItem >
          <FormItem 
            wrapperCol={{xs: 24, sm: {span: 12, offset: 5}}}
          >
            <Button>
              Submit
            </Button>
          </FormItem>
        </Form>
      ); 
    } else {
      return (
        <Form name={props.name} onSubmit={props.onSubmit}>
        <FormItem 
            label="ID"
            labelCol={{xs: 24, sm: 5}}
            wrapperCol={{xs: 24, sm: 12}}
          >
            {getFieldDecorator('id', {
              rules: [{required: false}],
            })(
              <Input />
            )}
      </FormItem>
        <FormItem
          label="Item Name"
          labelCol={{xs: 24, sm: 5}}
          wrapperCol={{xs: 24, sm: 12}}
        >
          {getFieldDecorator('name' , {
            rules: [{required: true, message: 'Please input your Item name!'}],
          })(
            <Input />
          )}
        </FormItem>
        <FormItem
          label="Description"
          labelCol={{xs: 24, sm: 5}}
          wrapperCol={{xs: 24, sm: 12}}
        >
          {getFieldDecorator('description', {
            rules: [{required: false}],
          })(
            <TextArea />
          )}
        </FormItem>
        <FormItem
            label="Price"
            labelCol={{xs: 24, sm: 5}}
            wrapperCol={{xs: 24, sm: 12}}
          >
            {getFieldDecorator('price', {
              rules: [{required: false}],
            })(
              <Input />
            )}
          </FormItem>
          <FormItem
            wrapperCol={{xs: 24, sm: {span: 12, offset: 5}}}
          >
          </FormItem>
        <FormItem
          wrapperCol={{xs: 24, sm: {span: 12, offset: 5}}}
        >
        </FormItem>
        <FormItem
          label="Category Name"
          labelCol={{xs: 24, sm: 5}}
          wrapperCol={{xs: 24, sm: 12}}
        >
          {getFieldDecorator('categoryID' , {
            rules: [{required: true, message: 'Please input your Item name!'}],
          })(
            <Cascader options={props.option} placeholder="Please select" />
          )}
        </FormItem >
      </Form>
      ) ;
    }
})