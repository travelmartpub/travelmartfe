import {Card, Button, Input, Icon, Cascader, Form} from "antd";
import { Fragment } from 'react';

interface IProps {
    form,
    handleSubmit: any
    options
}
let id = 0;
const FormItem = Form.Item;
export const OrderForm = (props: IProps) => {
    const { setFieldsValue, getFieldValue, getFieldDecorator} = props.form;

    const add = () => {
        const keys = getFieldValue("keys");
        const nextKeys = keys.concat(id++);
        setFieldsValue({
        keys: nextKeys,
        });
    }

    const remove = (k) => {
        getFieldDecorator('keys', { initialValue: [] });
          if (keys.length === 1) {
              return;
          }
          setFieldsValue({
              keys: keys.filter(key => key !== k),
          });
        }
    getFieldDecorator('keys', { initialValue: [] });
    const keys = getFieldValue("keys");

    return (
        <Form onSubmit={props.handleSubmit} >
            {
            
            keys.map((k, index) => (
        <Fragment key={k}>
          
        <FormItem
          label="Quantity"
              labelCol={{xs: 24, sm: 5}}
              wrapperCol={{xs: 24, sm: 12}}
              key= {"itemQuantity" + k}
        >
          {getFieldDecorator(`itemQuantity[${k}]`, {
            rules: [
              {
                required: true,
                whitespace: true,
                message: "Please input quantity",
              },
            ],
          })(<Input id={"it"}/>)}
          {keys.length > 1 ? (
            <Icon
              className="dynamic-delete-button"
              type="minus-circle-o"
              onClick={() => remove(k)}
            />
          ) : null}
          </FormItem>
          <FormItem
          label="Category Name"
          labelCol={{xs: 24, sm: 5}}
          wrapperCol={{xs: 24, sm: 12}}
          key={"itemId" + k}
        >
        {getFieldDecorator(`itemId[${index}]` , {
          rules: [{required: true, message: 'Please input your Item name!'}],
        })(
          <Cascader options={props.options} placeholder="Please select" />
        )}
        </FormItem >
        </Fragment>
        ))
        }
        <FormItem>
        <Button type="dashed" onClick={add} style={{ width: '100%' }}>
            <Icon type="plus" /> Add field
        </Button>
        </FormItem>
        <FormItem wrapperCol={{xs: 24, sm: {span: 12, offset: 5}}}>
        <Button type="primary" htmlType="submit">
            Submit
        </Button>
        </FormItem>
        </Form>
    );
}