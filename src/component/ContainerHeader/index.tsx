interface IProps {
  title: string
}

const ContainerHeader = (props: IProps) => {
  const {title} = props;

  return (
      <div className="gx-page-heading">
        <h2 className="gx-page-title">{title}</h2>
      </div>
  )
};

export default ContainerHeader;