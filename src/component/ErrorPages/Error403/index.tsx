import React from "react";
import Link from "next/link";

interface IProps {
}

const Error403: React.FC<IProps> = (props: IProps) => {
  return (
      <div className="gx-page-error-container">
        <div className="gx-page-error-content">
          <div className="gx-error-code gx-mb-4">403</div>
          <h2 className="gx-text-center">
            Forbidden!
          </h2>
          <p className="gx-text-center">
            <Link href="/" as="/">
              Go to Home
            </Link>
          </p>
        </div>
      </div>
  )
};

export default Error403;
