import React from "react";
import {Scrollbars} from "react-custom-scrollbars";

interface IProps {
  className: string;
  children: JSX.Element | JSX.Element[];
}

const CustomScrollbars = (props: IProps) =>
    <Scrollbars universal
                {...props}
                autoHide
                renderTrackHorizontal={props =>
                    <div {...props}
                         style={{display: 'none'}}
                         className="track-horizontal"/>
                }/>;

export default CustomScrollbars;
