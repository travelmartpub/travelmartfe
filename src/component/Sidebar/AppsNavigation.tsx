interface IProps {
}

const AppsNavigation = (props: IProps) => {
  return (
      <ul className="gx-app-nav">
        <li><i className="icon icon-search-new"/></li>
        <li><i className="icon icon-notification"/></li>
        <li><i className="icon icon-chat-new"/></li>
      </ul>
  );
};

export default AppsNavigation;
