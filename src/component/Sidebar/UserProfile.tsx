import {Avatar, Popover} from "antd";
import {useRouter} from "next/router";
import {authActions} from "../../redux/slice/authSlice";
import {useDispatch, useSelector} from 'react-redux';
import {IUser} from "../../model/entity/IUser";
import {AppState} from "../../redux";

interface IProps {
}

const UserProfile = (props: IProps) => {

  const dispatch = useDispatch();
  const router = useRouter();

  const {user}: { user: IUser } = useSelector((state: AppState) => state.me);

  const handleLogout = async () => {
    await router.push(process.env.APP_SUB_DOMAIN + '/');
    await dispatch(authActions.logout(async () => {
      return;
    }));
  };

  const userSignOut = () => {
    handleLogout().then()
  };

  const gotoAccount = async () => {
    await router.push(process.env.APP_SUB_DOMAIN + '/account');
  }

  const userMenuOptions = (
      <ul className="gx-user-popover">
        <li onClick={gotoAccount}>Hồ sơ</li>
        <li onClick={userSignOut}>Đăng xuất
        </li>
      </ul>
  );

  return (

      <div className="gx-flex-row gx-align-items-center gx-mb-4 gx-avatar-row">
        <Popover placement="bottomRight" content={userMenuOptions} trigger="click">
          <Avatar src={user != undefined ? user.avatar :
              "https://firebasestorage.googleapis.com/v0/b/travelmart-1d106.appspot.com/o/Avatar%2Fdefault-avatar.png?alt=media"}
                  className="gx-size-40 gx-pointer gx-mr-3" alt=""/>
          <span className="gx-avatar-name">{user ? user.userName : "Loading"}<i
              className="icon icon-chevron-down gx-fs-xxs gx-ml-2"/></span>
        </Popover>
      </div>

  )
};

export default UserProfile;