import React from "react";
import {Badge, Button, Popover} from "antd";
import Link from "next/link";
import {ICart, ICartItem} from "../../model/state/ICart";
import {CommonHelper} from "../../helper/CommonHelper";

interface IProps {
  cart: ICart
}

const content = (
    <div>

      <p>
        <i className="icon icon-check-circle-o gx-fs-md"/>
        <span className="gx-fs-13 gx-ml-sm-1 gx-cart-popover">Thêm vào giỏ hàng thành công!</span>
      </p>
      <Button className="gx-mb-1 gx-btn-red ant-btn-block">
        Xem giỏ hàng và thanh toán
      </Button>
    </div>
);

const Cart = (props: IProps) => {

  const {cart} = props;

  return (
      <Link href="/checkout/cart">
        <a href="/checkout/cart">


          <div className="gx-cart-views">
            <Badge className="gx-cart-badge" count={CommonHelper.countItems(cart.items)}
                   style={{backgroundColor: '#52c41a'}}
                   showZero>
              <i className="icon icon-shopping-cart"/>
            </Badge>

            <Popover placement="bottomRight" content={content}
                     trigger="click" visible={cart.miniCartShow}>
              <span className="gx-cart-text">Giỏ Hàng</span>
            </Popover>
          </div>

        </a>
      </Link>
  )

};

export default Cart;
