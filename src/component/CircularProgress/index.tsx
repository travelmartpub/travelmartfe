interface IProps {
  className?: string
}

const CircularProgress = (props: IProps) => {
  const {className} = props;

  return (
      <div className={`loader ${className}`}>
        <img src={process.env.APP_SUB_DOMAIN + '/assets/images/loader.svg'} alt="loader"/>
      </div>
  )
};

export default CircularProgress;