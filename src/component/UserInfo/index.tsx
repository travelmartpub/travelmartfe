import React from "react";
import {Avatar, Popover} from "antd";
import {useRouter} from "next/router";
import {useDispatch, useSelector} from "react-redux";
import {authActions} from "../../redux/slice/authSlice";
import {IUser} from "../../model/entity/IUser";
import {AppState} from "../../redux";
import _ from 'lodash';
import {CommonHelper} from "../../helper/CommonHelper";
import {Authorities} from "../../constant/Authorities";

interface IProps {
}

const UserInfo = (props: IProps) => {

  const dispatch = useDispatch();
  const router = useRouter();
  const {user}: { user: IUser } = useSelector((state: AppState) => state.me);
  const authorities = CommonHelper.getAuthorities(user);
  const storePrivilege: boolean = _.includes(authorities, Authorities.ROLE_STORE);

  const handleLogout = async () => {
    await router.push(process.env.APP_SUB_DOMAIN + '/');
    await dispatch(authActions.logout(() => {
      return
    }));
  };

  const handleGotoStore = async () => {
    await router.push(process.env.APP_SUB_DOMAIN + '/store');
  }

  async function gotoAccount() {
    await router.push(process.env.APP_SUB_DOMAIN + '/account');
  }

  const userMenuOptions = (
      <ul className="gx-user-popover">
        <li onClick={gotoAccount}>Hồ sơ</li>
        {storePrivilege == true && <li onClick={handleGotoStore}>Cửa hàng</li>}
        <li onClick={() => {
          router.push(process.env.APP_SUB_DOMAIN + '/order');
        }}>Đơn hàng
        </li>
        <li onClick={handleLogout}>
          Đăng xuất
        </li>
      </ul>
  );

  return (
      <Popover overlayClassName="gx-popover-horizantal" placement="bottomRight"
               content={userMenuOptions}
               trigger="click">
        <Avatar
            src={user != undefined ? user.avatar :
                "https://firebasestorage.googleapis.com/v0/b/travelmart-1d106.appspot.com/o/Avatar%2Fdefault-avatar.png?alt=media"}
            className="gx-avatar gx-pointer" alt=""/>
      </Popover>
  )

};

export default UserInfo;
