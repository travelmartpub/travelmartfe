import React, {ReactNode, useState} from "react";
import {Button, Checkbox, Col, Dropdown, Icon, Input, Menu, Row, Select} from "antd";
import {ColumnProps} from "antd/es/table";
import {CheckboxChangeEvent} from "antd/lib/checkbox";
import TableBase from "./index";
import {CommonHelper} from "../../helper/CommonHelper";
import {SelectionSelectFn} from "antd/lib/table";

const {Option} = Select;
const {Search} = Input;

interface IProps<T> {
  isLoading: boolean,
  columns: ColumnProps<T>[],
  border?: false,
  onReload: VoidFunction,
  defaultCheckedList?: Array<string>,
  data: Array<T>,
  insertFunction: JSX.Element | JSX.Element[],
  onSelectRow?: SelectionSelectFn<T>,
  pagination?: unknown,
  expandedRowRender?: (record: T, index: number, indent: number, expanded: boolean) => ReactNode;
}

const initDefaultCheckedList = [];

const TableOption = <T, >(props: IProps<T>) => {

  const {
    onReload,
    defaultCheckedList = initDefaultCheckedList,
    isLoading,
    columns,
    insertFunction,
    onSelectRow,
    pagination,
    expandedRowRender
  } = props;
  const {data}: { data: Array<T> } = props;

  const [checkList, setCheckList] = useState<Array<string>>(defaultCheckedList);
  const [visible, setVisible] = useState<boolean>(false);

  const handleVisibleChange = (flag) => {
    setVisible(flag);
  };

  const onChange = async (event: CheckboxChangeEvent) => {
    if (!event.target.checked) {
      await setCheckList(checkList.filter(value => value !== event.target.name));
      return;
    }

    await setCheckList([...checkList, event.target.name]);
  };

  const menu = (plainOptions: Array<string>, defaultCheckedList: Array<string>) => {
    return (
        <Menu>
          <Menu.Item key="0">
            <Checkbox
                // indeterminate={indeterminate}
                // onChange={onCheckAllChange}
                // checked={checkAll}
            >
              Check all
            </Checkbox>
          </Menu.Item>

          <Menu.Divider/>

          {plainOptions && plainOptions.map((value, index) => {
            const isDefault = defaultCheckedList.includes(value);

            return (
                <Menu.Item key={index + 1}>
                  <Checkbox
                      defaultChecked={isDefault}
                      disabled={isDefault}
                      onChange={onChange}
                      name={value}
                  >{value}</Checkbox>
                </Menu.Item>
            )
          })}
        </Menu>
    )
  };

  return (
      <>
        <Row type="flex">
          <Col sm={5} style={{paddingLeft: 0}}>
            {insertFunction}
          </Col>

          <Col sm={19}
               style={{paddingRight: 0, textAlign: "right"}}
          >
            <Row
                // type="flex" align="middle"
            >
              <Col sm={17} style={{textAlign: "right"}}>
                {/*<Input.Group compact>*/}
                {/*  <Select style={{minWidth: 90}} defaultValue={checkList && checkList[0]}>*/}
                {/*    {*/}
                {/*      checkList.map((value, index) => {*/}
                {/*        return (<Option key={index} value={value}>{value}</Option>)*/}
                {/*      })*/}
                {/*    }*/}
                {/*  </Select>*/}

                {/*  <Search*/}
                {/*      placeholder="Please input text"*/}
                {/*      onSearch={value => console.log(value)}*/}
                {/*      style={{width: 200}}*/}
                {/*  />*/}
                {/*</Input.Group>*/}
              </Col>

              <Col sm={7} style={{textAlign: "right"}}>
                <Button style={{border: 0}}
                        onClick={onReload}>
                  <Icon type="reload"/>
                </Button>

                <Dropdown overlay={menu(CommonHelper.getNameOfColumn(columns), defaultCheckedList)}
                          placement="bottomRight"
                          onVisibleChange={handleVisibleChange}
                          visible={visible}
                          trigger={['click']}
                >
                  <Button style={{border: 0}}>
                    <Icon type="setting"/>
                  </Button>
                </Dropdown>
              </Col>


            </Row>
          </Col>

        </Row>

        <br/>

        <TableBase<T> columns={columns.filter((value => checkList.includes(value.dataIndex)))}
                      data={data}
                      isLoading={isLoading}
                      onSelectRow={onSelectRow}
                      pagination={pagination}
                      expandedRowRender={expandedRowRender}

        />

      </>
  )

};

export default TableOption;
