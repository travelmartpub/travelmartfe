import React, {ReactNode} from "react";
import {Table} from "antd";
import {ColumnProps} from "antd/es/table";
import {SelectionSelectFn} from "antd/lib/table";

interface IProps<T> {
  isLoading: boolean,
  columns: ColumnProps<T>[],
  data: T[],
  border?: false;
  onSelectRow?: SelectionSelectFn<T>,
  pagination?: unknown,
  expandedRowRender?: (record: T, index: number, indent: number, expanded: boolean) => ReactNode;
}

const TableBase = <T, >(props: IProps<T>) => {

  const rowSelection = {
    // columnTitle: (<></>),
    // onChange: (selectedRowKeys, selectedRows) => {
    //   console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
    // },
    onSelect: (record, selected, selectedRows, nativeEvent) => {
      props.onSelectRow(record, selected, selectedRows, nativeEvent);
    },
    // onSelectAll: (selected, selectedRows, changeRows) => {
    //   console.log(selected, selectedRows, changeRows);
    // },
  };

  return (
      <Table className="gx-table-responsive"
             loading={props.isLoading}
             columns={props.columns}
             dataSource={props.data}
             rowKey={(record, index) => String(index)}
             bordered={props.border}
          // scroll={{ y: 390 }}
             size={"middle"}
             rowSelection={props.onSelectRow && rowSelection}
             pagination={props.pagination}
             expandedRowRender={props.expandedRowRender}
      />
  )

};

export default TableBase;
