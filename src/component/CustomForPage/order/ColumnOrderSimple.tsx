import React from "react";
import {Dropdown, Menu,} from "antd";
import {ColumnProps} from "antd/es/table";
import NumberFormat from "react-number-format";
import {IOrderStoreDetail} from "../../../model/entity/IOrder";

const menu = () => (
    <Menu>
      <Menu.Item key="0">
        <a rel="noopener noreferrer" href="#" onClick={() => {
        }}>Edit</a>
      </Menu.Item>

      <Menu.Item key="1">
        <a rel="noopener noreferrer" href="#" onClick={() => {
        }}>Delete</a>
      </Menu.Item>
    </Menu>
);

/**
 *  Column structure for table
 */
const columns: ColumnProps<IOrderStoreDetail>[] = [
  {
    title: 'Order ID',
    dataIndex: 'id',
    key: 'id',
    align: 'left',
    sorter: (a, b) => a.id - b.id,
  },
  {
    title: 'Tổng thành tiền',
    dataIndex: 'totalPrice',
    key: 'totalPrice',
    align: 'left',
    sorter: (a, b) => a.totalPrice - b.totalPrice,
    render: (text, record) => (
        <>
          <NumberFormat
              value={record.totalPrice}
              displayType={"text"}
              thousandSeparator={true}
              suffix={""}
          /> ₫
        </>
    )
  },
  {
    title: 'Trạng thái',
    dataIndex: 'status',
    key: 'status',
    align: 'left',
    ellipsis: true,
  },
  {
    title: 'Hành động',
    key: 'action',
    dataIndex: 'action',
    fixed: 'right',
    render: (text, record) => (
        <Dropdown overlay={menu()} placement="bottomLeft" trigger={['click']}>
            <span className="gx-link ant-dropdown-link">
              <i className="gx-icon-btn icon icon-ellipse-v"/>
            </span>
        </Dropdown>
    ),
  }
];

export const ColumnOrderSimple = {
  columns
};