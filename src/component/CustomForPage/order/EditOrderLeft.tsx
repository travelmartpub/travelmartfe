import React, {ChangeEvent, useEffect, useState} from "react";
// import {
//   Button,
//   Col,
//   Divider,
//   Input,
//   InputNumber,
//   Modal,
//   Row,
//   Select,
//   Tag
// } from "antd";
// import NumberFormat from "react-number-format";
// import CustomScrollbars from "../../Sidebar/CustomScrollbars";
// import moment from "moment";
// import ModalFormProduct from "../product/ModalFormProduct";
// import {ColumnService} from "../services/ColumnService";
// import TableOption from "../../Table/TableOption";
// import {IService} from "../../../model/IService";
// // import {IItem} from "../../../pages/order";
//
// const Option = Select.Option;
//
// const {TextArea} = Input;
//
// interface IProps {
//   items: Array<IItem>,
//   discount?: number,
//   handleRemoveItem?: (item: IItem) => VoidFunction,
//   handleUpdateQuantity?: (item: IItem, quantity: number) => VoidFunction,
//   handleUpdatePrice?: (item: IItem, price: number) => VoidFunction,
//   handleUpdateDiscount?: (discount: number) => VoidFunction,
//   handleUpdateDescription?: (description: string) => VoidFunction
// }
//
// interface IItemProps {
//   item: IItem,
//   id: number,
//   showModal: VoidFunction,
//   handleRemoveItem?: (item: IItem) => VoidFunction,
//   handleUpdateQuantity?: (item: IItem, quantity: number) => VoidFunction,
//   handleUpdatePrice?: (item: IItem, price: number) => VoidFunction
// }
//
// const Item = (props: IItemProps) => {
//   const {item, id, showModal, handleRemoveItem, handleUpdateQuantity, handleUpdatePrice} = props;
//
//   return (
//       <Row
//           className="order-list-item"
//           // className="gx-contact-item"
//           type="flex" align="middle"
//       >
//         <Col md={2}
//             // style={{padding: 0}}
//         >
//           <Button
//               // style={{border: 3, textAlign: "center"}}
//               style={{
//                 margin: 0,
//                 border: 0,
//                 lineHeight: 0,
//                 padding: '5!important',
//                 marginLeft: -2,
//                 // marginTop: 5
//               }}
//               // style={{textAlign: "center", lineHeight:3}}
//               // onClick={()=> {
//               //   console.log(item);
//               // }}
//               onClick={() => {
//                 handleRemoveItem(item)
//               }}
//           >
//             <i className="icon icon-close-circle"/>
//           </Button>
//         </Col>
//
//         {/*<Col md={1} style={{textAlign: 'center'}}>*/}
//         {/*  {id}*/}
//         {/*</Col>*/}
//
//         <Col md={4}>
//           {item.code}
//         </Col>
//         <Col md={6}>
//           {/*<Tooltip placement="topLeft" title="Edit item detail">*/}
//           <div className="order-name-item" onClick={() => {
//             showModal();
//             // console.log("Click ", props.item.itemName);
//
//           }}>{item.name}</div>
//           {/*</Tooltip>*/}
//
//         </Col>
//         <Col md={3} style={{textAlign: 'center', paddingRight: 0}}>
//           <InputNumber className="order-item-input-number"
//                        formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
//                        parser={value => value.replace(/\$\s?|(,*)/g, '')}
//                        onChange={(value: number) => {
//                          handleUpdateQuantity(item, value);
//                        }}
//                        min={1}
//                        value={item.quantity}
//                        defaultValue={item.quantity}
//           />
//         </Col>
//         <Col md={5} style={{textAlign: 'center', paddingRight: 0}}>
//           <InputNumber className="order-item-input-number"
//                        formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
//                        parser={value => value.replace(/\$\s?|(,*)/g, '')}
//                        min={1000}
//                        onChange={(value: number) => {
//                          handleUpdatePrice(item, value);
//                        }}
//                        value={item.price}
//                        defaultValue={item.price}
//           />
//         </Col>
//         <Col md={4} style={{textAlign: 'center'}}>
//           <NumberFormat
//               value={item.price * item.quantity}
//               displayType={"text"}
//               thousandSeparator={true}
//               suffix={""}
//           />
//         </Col>
//       </Row>
//   )
// };
//
//
// const EditOrderLeft = (props: IProps) => {
//
//   const {items, discount, handleRemoveItem, handleUpdateQuantity, handleUpdatePrice, handleUpdateDiscount, handleUpdateDescription} = props;
//
//   // const [items, setItems] = useState<Array<IItem>>(initItems);
//   const [showModal, setShowModal] = useState<boolean>(false);
//
//   const handleCancel = () => {
//     setShowModal(false);
//   };
//
//   const calQuantity = (items: Array<IItem>) => {
//     let count = 0;
//     items.map(item => {
//       count += item.quantity;
//     });
//
//     return count;
//   };
//
//   const calSubTotalPrice = () => {
//     let subTotalPrice = 0;
//     items.map(item => {
//       subTotalPrice += item.price * item.quantity;
//     });
//
//     return subTotalPrice;
//   };
//
//   const onChangeDescription = (e) => {
//     handleUpdateDescription(e.target.value);
//   };
//
//   return (
//       <div className="gx-main-content">
//         <Row
//             className="order-header-customer"
//             type="flex" align="middle"
//         >
//           <Col md={24}>
//             <Row type="flex" align="middle">
//               <Col md={5}>
//                 Staff:
//               </Col>
//
//               <Col md={6}>
//                 <Tag>Admin</Tag>
//               </Col>
//
//               <Col md={13} style={{textAlign: "right"}}>
//                 Time: <Tag
//                   style={{marginRight: 0}}>{moment(new Date()).format("DD/MM/YYYY hh:mm")}</Tag>
//               </Col>
//             </Row>
//
//             <Row type="flex" align="middle">
//               <Col md={5}>
//                 Customer:
//               </Col>
//
//               <Col md={15}>
//                 <Select
//                     showSearch
//                     style={{width: '100%'}}
//                     placeholder="Select a customer"
//                     optionFilterProp="children"
//                 >
//                   <Option value={1}>CUS-001 Hoàng Hải An</Option>
//                   <Option value={2}>CUS-129 Lê Nguyễn Chí</Option>
//                   <Option value={3}>CUS-091 Nguyễn Đình Chi</Option>
//                 </Select>
//               </Col>
//
//               <Col md={4} style={{textAlign: "right"}}>
//                 <Button style={{marginBottom: 0}}><i className="icon icon-user"/></Button>
//               </Col>
//
//             </Row>
//
//           </Col>
//         </Row>
//
//         <Row className="order-header-item"
//              type="flex" align="middle"
//         >
//           <Col className="order-column-item" md={2}>
//             {/*Action*/}
//           </Col>
//           {/*<Col className="order-column-item" md={1} style={{textAlign: 'center'}}>*/}
//           {/*  ID*/}
//           {/*</Col>*/}
//           <Col className="order-column-item" md={4}>
//             Code
//           </Col>
//           <Col className="order-column-item" md={6}>
//             Name
//           </Col>
//           <Col className="order-column-item" md={3} style={{textAlign: 'center', paddingRight: 0}}>
//             Count
//           </Col>
//           <Col className="order-column-item" md={5} style={{textAlign: 'center', paddingRight: 0}}>
//             Price
//           </Col>
//           <Col className="order-column-item" md={4} style={{textAlign: 'center'}}>
//             Total
//           </Col>
//         </Row>
//
//         {/*gx-module-content-scroll*/}
//         <CustomScrollbars className="order-content-scroll">
//           {items.map((item, index) => (
//               <Item key={index}
//                     item={item}
//                     id={index}
//                     showModal={() => {
//                       setShowModal(true)
//                     }}
//                     handleUpdateQuantity={handleUpdateQuantity}
//                     handleUpdatePrice={handleUpdatePrice}
//                     handleRemoveItem={handleRemoveItem}
//               />
//           ))}
//         </CustomScrollbars>
//
//         <Row
//             className="order-footer-item"
//             type="flex" align="middle"
//         >
//           <Col md={24}>
//
//             <Row>
//               <Col md={12}>
//                 Items:
//               </Col>
//
//               <Col md={12}>
//                 <NumberFormat
//                     value={calQuantity(items)}
//                     displayType={"text"}
//                     thousandSeparator={true}
//                     suffix={""}
//                 />
//               </Col>
//             </Row>
//
//             <Row>
//               <Col md={12}>
//                 Subtotal:
//               </Col>
//
//               <Col md={12}>
//                 <NumberFormat
//                     value={calSubTotalPrice()}
//                     displayType={"text"}
//                     thousandSeparator={true}
//                     suffix={""}
//                 /> VND
//               </Col>
//             </Row>
//
//             <Row>
//               <Col md={12}>
//                 Discount:
//               </Col>
//
//               <Col md={12}>
//                 <InputNumber className="order-footer-discount"
//                              formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
//                              parser={value => value.replace(/\$\s?|(,*)/g, '')}
//                              defaultValue={0}
//                              value={discount}
//                              min={0}
//                              onChange={(value: number) => {
//                                handleUpdateDiscount(value);
//                              }}
//                 />
//               </Col>
//             </Row>
//
//             <Row>
//               <Col md={12}>
//                 Description:
//               </Col>
//
//               <Col md={12}>
//                 <TextArea
//                     className="order-footer-description"
//                     onChange={onChangeDescription}
//                     autoSize/>
//               </Col>
//             </Row>
//
//             <Divider/>
//
//             <Row>
//               <Col md={12}>
//                 <span className="order-footer-total">Total:</span>
//               </Col>
//
//               <Col md={12}>
//                 <span className="order-footer-total">
//                   <NumberFormat
//                       value={calSubTotalPrice() - discount}
//                       displayType={"text"}
//                       thousandSeparator={true}
//                       suffix={""}
//                   /> VND
//                 </span>
//               </Col>
//             </Row>
//           </Col>
//         </Row>
//
//         <Modal title="Edit Item Detail"
//                visible={showModal}
//                onCancel={handleCancel}
//             //    width={1000}
//             // footer={[
//             //   <Button key="back" onClick={() => {
//             //     // handleCancel();
//             //     // resetFields();
//             //   }}>
//             //     Return
//             //   </Button>,
//             //   <Button key="submit" type="primary"
//             //       // loading={isLoadingOk}
//             //       // onClick={handleSubmit}
//             //   >
//             //     Update
//             //   </Button>
//             // ]}
//         >
//           Hi
//         </Modal>
//       </div>
//   )
// };
//
// export default EditOrderLeft;