import React from "react";
import {ColumnProps} from "antd/es/table";
import TableOption from "../../Table/TableOption";

interface IProps<T> {
  isLoading: boolean,
  columns: ColumnProps<T>[],
  border?: false,
  onReload: VoidFunction,
  defaultCheckedList?: Array<string>,
  data: Array<T>,
  insertFunction: JSX.Element | JSX.Element[];
}

const initDefaultCheckedList = ["id", "name", "description", "action"];

const TableOrderRight = <T extends unknown>(props: IProps<T>) => {

  const {defaultCheckedList = initDefaultCheckedList, isLoading, columns, insertFunction} = props;
  const {data}: { data: Array<T> } = props;
  
  return (
      <>
        <TableOption isLoading={isLoading}
                     columns={columns}
                     onReload={async () => {
                     }}
                     data={data}
                     insertFunction={insertFunction}
                     defaultCheckedList={defaultCheckedList}
        />
      </>
  )

};

export default TableOrderRight;
