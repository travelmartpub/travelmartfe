import React from "react";
import {
  Dropdown,
  Menu,
} from "antd";
import {ColumnProps} from "antd/es/table";
import NumberFormat from "react-number-format";
import moment from "moment";
import {ICategoryService} from "../../../model/ICategoryService";
import {IUnit} from "../../../model/IUnit";
import {IService} from "../../../model/IService";

const menu = (props: IService) => (
    <Menu>
      <Menu.Item key="0">
        <a rel="noopener noreferrer" href="#" onClick={() => {
        }}>Edit Service</a>
      </Menu.Item>

      <Menu.Item key="1">
        <a rel="noopener noreferrer" href="#" onClick={() => {
        }}>Delete Service</a>
      </Menu.Item>
    </Menu>
);

/**
 *  Column structure for table
 */
const columns: ColumnProps<IService>[] = [
  {
    title: 'ID',
    dataIndex: 'id',
    key: 'id',
    align: 'left',
    sorter: (a, b) => a.id - b.id,
  },
  {
    title: 'Name',
    dataIndex: 'name',
    key: 'name',
    align: 'left',
    sorter: (a, b) => a.name.length - b.name.length
    // render: text => <span className="gx-link">{text.login}</span>
  },
  {
    title: 'Cost',
    dataIndex: 'cost',
    key: 'cost',
    align: 'left',
    sorter: (a, b) => a.cost - b.cost,
    render: (text, record) => (
        <NumberFormat
            value={record.cost}
            displayType={"text"}
            thousandSeparator={true}
            suffix={""}
        />
    )
  },
  {
    title: 'Price',
    dataIndex: 'price',
    key: 'price',
    align: 'left',
    sorter: (a, b) => a.price - b.price,
    render: (text, record) => (
        <NumberFormat
            value={record.cost}
            displayType={"text"}
            thousandSeparator={true}
            suffix={""}
        />
    )
  },
  {
    title: 'Description',
    dataIndex: 'description',
    key: 'description',
    align: 'left',
    ellipsis: true,
  },
  {
    title: 'Warranty',
    dataIndex: 'warrantyPeriod',
    key: 'warrantyPeriod',
    sorter: (a, b) => a.warrantyPeriod - b.warrantyPeriod,
    render: (text: number) => {
      return moment(text).format("DD/MM/YYYY")
    },
  },
  {
    title: 'Category',
    key: 'category',
    dataIndex: 'category',
    align: 'left',
    sorter: (a, b) => a.category.name.length - b.category.name.length,
    render: (text: ICategoryService, record) => (
        text.name
    ),
  },
  {
    title: 'Unit',
    key: 'unit',
    dataIndex: 'unit',
    align: 'left',
    sorter: (a, b) => a.unit.name.length - b.unit.name.length,
    render: (text: IUnit, record) => (
        text.name
    )
  },
  {
    title: 'Action',
    key: 'action',
    dataIndex: 'action',
    render: (text, record) => (
        <Dropdown overlay={menu(record)} placement="bottomRight" trigger={['click']}>
            <span className="gx-link ant-dropdown-link">
              <i className="gx-icon-btn icon icon-ellipse-v"/>
            </span>
        </Dropdown>
    ),
  }
];

export const ColumnService = {
  columns
};