import React, {SyntheticEvent} from "react";
import {
  Button, Col, DatePicker,
  Form,
  Icon, Input,
  InputNumber,
  Modal, Row,
  Select,
  Upload
} from "antd";
import {FormComponentProps} from "antd/lib/form";
import {IService, IServiceReq} from "../../../model/IService";
import {RcFile} from "antd/lib/upload";

interface IProps extends FormComponentProps {
  modalTitle: string,
  showModal: boolean,
  handleOk: (data: IServiceReq) => Promise<VoidFunction>,
  isLoadingOk: boolean,
  handleCancel: VoidFunction
  data?: IService
}

const formItemLayout = {
  labelCol: {xs: 24, sm: 6},
  wrapperCol: {xs: 24, sm: 14},
};

const FormItem = Form.Item;
const Option = Select.Option;
const {TextArea} = Input;

const ModalFormServiceIndex = (props: IProps) => {

  const {getFieldDecorator, validateFields, resetFields} = props.form;
  const {modalTitle, showModal, isLoadingOk, handleOk, handleCancel, data} = props;

  const handleSubmit = (e: SyntheticEvent) => {
    e.preventDefault();
    validateFields(async (err, values: IServiceReq) => {
      if (!err) {
        await handleOk({
          ...values,
          warrantyPeriod: values.warrantyPeriod?.valueOf()
        });
        resetFields();
      }
    });
  };

  const normFile = (e) => {
    if (Array.isArray(e)) {
      return e;
    }
    return e && e.fileList;
  };

  return (
      <Modal title={modalTitle}
             visible={showModal}
          // onOk={handleOk}
          // confirmLoading={confirmLoading}
             onCancel={handleCancel}
             width={1000}
             footer={[
               <Button key="back" onClick={() => {
                 handleCancel();
                 resetFields();
               }}>
                 Return
               </Button>,
               <Button key="submit" type="primary" loading={isLoadingOk} onClick={handleSubmit}>
                 Submit
               </Button>
             ]}
      >
        <Form>
          <FormItem
              {...formItemLayout}
              label="Service ID"
          >
            <span className="ant-form-text">{
              data ? data.id : (<i>{"#auto-generic"}</i>)
            }</span>
          </FormItem>

          <FormItem
              {...formItemLayout}
              label="Service Name"
          >
            {getFieldDecorator('name', {
              rules: [{
                required: true, message: 'The input is not valid name!',
              }],
            })(
                <Row gutter={8}>
                  <Col span={15}>
                    <Input placeholder="Please input service name"/>
                  </Col>
                </Row>
            )}

          </FormItem>

          <FormItem
              {...formItemLayout}
              label="Cost"
          >
            {getFieldDecorator('cost', {
              rules: [{
                required: true, message: 'The input is not valid cost!',
              }],
              initialValue: 100000
            })(
                <InputNumber className="gx-mb-3"
                             formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                             parser={value => value.replace(/\$\s?|(,*)/g, '')}
                             style={{width: 120}}
                />
            )}
            <span className="ant-form-text">VND</span>
          </FormItem>

          <FormItem
              {...formItemLayout}
              label="Price"
          >
            {getFieldDecorator('price', {
              rules: [{
                required: true, message: 'The input is not valid price!',
              }],
              initialValue: 120000
            })(
                <InputNumber className="gx-mb-3"
                             formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                             parser={value => value.replace(/\$\s?|(,*)/g, '')}
                             style={{width: 120}}
                />
            )}
            <span className="ant-form-text">VND</span>
          </FormItem>

          <FormItem
              {...formItemLayout}
              label="Category"
              hasFeedback
          >
            {getFieldDecorator('categoryId', {
              rules: [
                {required: true, message: 'Please select your category!'},
              ],
            })(
                <Select
                    showSearch
                    style={{width: 200}}
                    placeholder="Select a category"
                    optionFilterProp="children"
                >
                  <Option value={1}>Camera</Option>
                  <Option value={2}>Internet</Option>
                  <Option value={3}>Setup</Option>
                </Select>
            )}
          </FormItem>

          <FormItem
              {...formItemLayout}
              label="Unit"
              hasFeedback
          >
            {getFieldDecorator('unitId', {
              rules: [
                {required: true, message: 'Please select your category!'},
              ],
              initialValue: 1
            })(
                <Select
                    showSearch
                    style={{width: 200}}
                    placeholder="Select a unit"
                    optionFilterProp="children"
                >
                  <Option value={1}>Lần</Option>
                  <Option value={2}>Cái</Option>
                </Select>
            )}
          </FormItem>

          <FormItem
              {...formItemLayout}
              label="Warranty"
              hasFeedback
          >
            {getFieldDecorator('warrantyPeriod', {})(
                <DatePicker format="DD/MM/YYYY"/>
            )}
          </FormItem>

          <FormItem
              {...formItemLayout}
              label="Description"
              hasFeedback
          >
            {getFieldDecorator('description', {})(
                <TextArea rows={3}/>
            )}
          </FormItem>

          <FormItem
              {...formItemLayout}
              label="Images"
          >
            {getFieldDecorator('images', {
              valuePropName: 'fileList',
              getValueFromEvent: normFile,
            })(
                <Upload name="image description"
                        beforeUpload={(file: RcFile, FileList: RcFile[]): boolean => {
                          return false;
                        }}
                        listType="picture">
                  <Button>
                    <Icon type="upload"/>Click to upload
                  </Button>
                </Upload>
            )}
          </FormItem>

        </Form>
      </Modal>
  )
};

const ModalFormService = Form.create<IProps>()(ModalFormServiceIndex);
export default ModalFormService;