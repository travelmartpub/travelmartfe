import {Button, Icon, Popover} from "antd";
import React from "react";

const buttonWidth = 70;

const text = <span>Settings</span>;
const content = (
    <div>
      <p>Save Running Fields current state</p>
    </div>
);

/**
 * Custom for Running Field card
 */
export const moreFieldsCard = () => {
  return (
      <>
        <div style={{marginLeft: buttonWidth, clear: 'both', whiteSpace: 'nowrap'}}>
          <Popover placement="bottomRight" title={text} content={content} trigger="click">
            <Button style={{border: 0}}>
              <Icon type="setting" onClick={() => {
              }}/>
            </Button>
          </Popover>
        </div>
      </>
  )
};

/**
 * Custom for Executions card
 */
export const moreExecutionsCard = (handleOnclick: VoidFunction) => {
  return (
      <>
        <div style={{marginLeft: buttonWidth, clear: 'both', whiteSpace: 'nowrap'}}>
          <Button
              style={{border: 0}}
              onClick={() => {
                handleOnclick();
              }}
          >
            <Icon type="reload"/>
          </Button>
        </div>
      </>
  )
};

/**
 *  Marks for slider
 */
export const marksCCU = {
  1: 1,
  32: 32,
  64: 64,
  125: 125
};


export const marksDuration = {
  30: 30,
  300: 300,
  900: 900,
};

/**
 * Just mock for test
 */

export const optionsScripts = [{
  value: 'wwomens',
  label: 'wwomens',
  children: [{
    value: 'wwomens_7_referral.jmx',
    label: 'wwomens_7_referral.jmx',
    children: [{
      value: 'opstion',
      label: 'West Lake',
    }],
  }, {
    value: 'wwomens_1_landding_page.jmx',
    label: 'wwomens_1_landding_page.jmx'
  }]
}, {
  value: 'um',
  label: 'um',
  children: [{
    value: 'registerorlogin.jmx',
    label: 'registerorlogin.jmx',
    // children: [{
    //   value: 'opstion 2',
    //   label: 'react',
    // }],
  }, {
    value: 'GET_BALANCE.jmx',
    label: 'get_balance.jmx'
  }
  ],
}];