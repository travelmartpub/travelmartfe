import React from "react";
import {Dropdown, Menu,} from "antd";
import {ColumnProps} from "antd/es/table";
import NumberFormat from "react-number-format";
import {IProduct} from "../../../model/entity/IProduct";

const menu = (record) => (
    <Menu>
      <Menu.Item key="0">
        <a rel="noopener noreferrer" href="#" onClick={() => {
        }}>Edit</a>
      </Menu.Item>

      <Menu.Item key="1">
        <a rel="noopener noreferrer" href="#" onClick={() => {
        }}>Delete</a>
      </Menu.Item>
    </Menu>
);

/**
 *  Column structure for table
 */

const columns: ColumnProps<IProduct>[] = [
  {
    dataIndex: 'images',
    key: 'images',
    align: 'left',
    width: 90,
    fixed: 'left',
    render: (text, record) => {
      return <div>
        <img className="gx-rounded-circle gx-size-50" src={record.images.split(",")[0]}
             alt={record.name}/>
      </div>
    },
  },
  {
    title: 'ID',
    dataIndex: 'id',
    key: 'id',
    align: 'left',
    width: 130,
    sorter: (a, b) => a.id - b.id,
  },
  {
    title: 'Tên',
    dataIndex: 'name',
    key: 'name',
    align: 'left',
    width: 250,
    ellipsis: true,
    sorter: (a, b) => a.name.length - b.name.length
  },
  {
    title: 'Đơn giá',
    dataIndex: 'price',
    key: 'price',
    align: 'left',
    width: 130,
    sorter: (a, b) => a.price - b.price,
    render: (text, record) => (
        <>
          <NumberFormat
              value={record.price}
              displayType={"text"}
              thousandSeparator={true}
              suffix={""}
          /> ₫
        </>
    )
  },
  {
    title: 'Số lượng',
    dataIndex: 'stock',
    key: 'stock',
    width: 130,
    sorter: (a, b) => a.stock - b.stock
  },
  {
    title: 'Mô tả',
    dataIndex: 'description',
    key: 'description',
    align: 'left',
    ellipsis: true,
  },
  {
    title: 'Trạng thái',
    dataIndex: 'activated',
    key: 'activated',
    align: 'left',
    ellipsis: true,
  },
  {
    title: 'Hành động',
    key: 'action',
    dataIndex: 'action',
    fixed: 'right',
    render: (text, record) => (
        <Dropdown overlay={menu(record)} placement="bottomLeft" trigger={['click']}>
            <span className="gx-link ant-dropdown-link">
              <i className="gx-icon-btn icon icon-ellipse-v"/>
            </span>
        </Dropdown>
    ),
  }
];

export const ColumnProductSimple = {
  columns
};