import React, {SyntheticEvent} from "react";
import {Button, Col, Form, Input, InputNumber, Modal, Row, Select} from "antd";
import {FormComponentProps} from "antd/lib/form";
import {IProductRequest} from "../../../model/IProductRequest";
import {IProduct} from "../../../model/entity/IProduct";
import {IUser} from "../../../model/entity/IUser";
import {AppState} from "../../../redux";
import {useDispatch, useSelector} from "react-redux";
import {ProductActions} from "../../../redux/slice/productSlice";

interface IProps extends FormComponentProps {
  modalTitle: string,
  showModal: boolean,
  handleOk: VoidFunction,
  isLoadingOk: boolean,
  handleCancel: VoidFunction
  data?: IProduct
}

const formItemLayout = {
  labelCol: {xs: 24, sm: 6},
  wrapperCol: {xs: 24, sm: 14},
};

const FormItem = Form.Item;
const Option = Select.Option;
const {TextArea} = Input;

const ModalFormProductIndex = (props: IProps) => {

  const {getFieldDecorator, validateFields, resetFields, setFieldsValue} = props.form;
  const {modalTitle, showModal, isLoadingOk, handleOk, handleCancel, data} = props;
  const {user}: { user: IUser } = useSelector((state: AppState) => state.me);
  const dispatch = useDispatch();

  const handleSubmit = (e: SyntheticEvent) => {
    e.preventDefault();
    validateFields(async (err, values: IProductRequest) => {
      if (values.name == '' || values.price <= 0 || values.stock <= 0) {
        Modal.warning({
          title: 'Cập nhật không thành công!',
          content: 'Vui lòng nhập thông tin sản phẩm hợp lệ'
        })
      }

      if (!err) {
        values.storeId = user.storeId;
        if (data.id != 0) {
          values.id = data.id;
          dispatch(ProductActions.updateProduct(values, () => {
            Modal.success({
              title: 'Cập nhật thành công!',
              onOk() {
                handleOk();
                resetFields();
              }
            })
          }, () => {
            Modal.warning({
              title: 'Cập nhật không thành công!',
              content: 'Vui lòng nhập thông tin sản phẩm hợp lệ'
            })
          }));
        } else {
          dispatch(ProductActions.createProduct(values, () => {
            handleOk();
            resetFields();
          }, () => {
            Modal.warning({
              title: 'Cập nhật không thành công!',
              content: 'Vui lòng nhập thông tin sản phẩm hợp lệ'
            })
          }));
        }
        //resetFields();
      }
    });
  };

  return (
      <Modal title={modalTitle}
             visible={showModal}
          // onOk={handleOk}
          // confirmLoading={confirmLoading}
             onCancel={handleCancel}
             width={1000}
             footer={[
               <Button key="back" onClick={async () => {
                 await handleCancel();
                 resetFields();
               }}>
                 Hủy
               </Button>,
               <Button key="submit" type="primary" loading={isLoadingOk} onClick={handleSubmit}>
                 Xác nhận
               </Button>
             ]}
      >
        <Form>
          <FormItem
              {...formItemLayout}
              label="ID"
          >
            <span className="ant-form-text">{
              data.id != 0 ? data.id : (<i>{"#auto-generic"}</i>)
            }</span>
          </FormItem>

          <FormItem
              {...formItemLayout}
              label="Tên sản phẩm"
          >
            <Row gutter={8}>
              <Col span={15}>
                {getFieldDecorator('name', {
                  rules: [{
                    required: true, message: 'Vui lòng nhập tên sản phẩm',
                  }],
                  initialValue: data.name
                })(
                    <Input name="name" placeholder="Nhập tên sản phẩm"/>)}
              </Col>
            </Row>

          </FormItem>

          <FormItem
              {...formItemLayout}
              label="Giá sản phẩm"
          >
            {getFieldDecorator('price', {
              rules: [{
                required: true, message: 'Vui lòng nhập giá sản phẩm',
              }],
              initialValue: data.price
            })(
                <InputNumber className="gx-mb-3"
                             formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                             parser={value => value.replace(/\$\s?|(,*)/g, '')}
                             style={{width: 120}}
                />)}
            <span className="ant-form-text">VND</span>
          </FormItem>

          <FormItem
              {...formItemLayout}
              label="Số lượng có sẵn"
          >
            {getFieldDecorator('stock', {
              rules: [{
                required: true, message: 'Vui lòng nhập số lượng sản phẩm',
              }],
              initialValue: data.stock
            })(
                <InputNumber className="gx-mb-3"
                             formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                             parser={value => value.replace(/\$\s?|(,*)/g, '')}
                             style={{width: 120}}
                />)}
            {/*<span className="ant-form-text">VND</span>*/}
          </FormItem>

          <FormItem
              {...formItemLayout}
              label="Mô tả sản phẩm"
              hasFeedback
          >
            {getFieldDecorator('description', {
              initialValue: data.description
            })(
                <TextArea rows={4}/>)}
          </FormItem>

          <FormItem
              {...formItemLayout}
              label="Hình ảnh"
              hasFeedback
          >
            {getFieldDecorator('images', {
              initialValue: data.images
            })(
                <TextArea rows={2}/>)}
          </FormItem>

        </Form>
      </Modal>
  )
};

const ModalFormProduct = Form.create<IProps>()(ModalFormProductIndex);
export default ModalFormProduct;