# syntax=docker/dockerfile:experimental

FROM node:12-alpine AS builder

WORKDIR /build
ARG RELEASE=dev

COPY package.json yarn.lock ./
RUN --mount=type=cache,id=yarn,target=/usr/local/share/.cache/yarn \
  yarn install --frozen-lockfile --network-timeout 1200000
RUN mkdir -p /build/node_modules && chown -R node:node /build
COPY --chown=node:node . .

RUN yarn build

# Alpine Linux with NodeJS
FROM node:12-alpine

ARG SERVICE_NAME=webcommerce
ARG SERVICE_PORT=9000
ENV HOME=/home

# Run with serve
RUN apk add --update sudo

WORKDIR $HOME
COPY --from=builder /build/next.config.js ./
COPY --from=builder /build/public ./public
COPY --from=builder /build/.next ./.next
COPY --from=builder /build/package.json ./package.json
COPY --from=builder /build/node_modules ./node_modules

EXPOSE $SERVICE_PORT

CMD ["yarn", "start", "-p", "9000"]